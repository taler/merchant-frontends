#!/usr/bin/env python3

"""
Testing module, very limited: it is NOT able to mock
a wallet (resume Python wallet?)!
"""

import unittest
import logging
from mock import patch, MagicMock
from talerfrontends.donations import donations
from talerfrontends.blog import blog
from talerfrontends.talerconfig import TalerConfig
from datetime import datetime

logger = logging.getLogger(__name__)
dapp = donations.app
bapp = blog.app
tc = TalerConfig.from_env()
CURRENCY = tc["taler"]["currency"].value_string(required=True)

class DonationsTestCase(unittest.TestCase):
    def setUp(self):
        dapp.testing = True
        self.app = dapp.test_client()

    @patch("requests.post")
    @patch("random.randint")
    @patch("datetime.datetime")
    def test_proposal_creation(self, mocked_datetime,
                               mocked_random, mocked_post):
        qs = "/generate-contract?nonce=44&donation_receiver=Tor&donation_amount=1.0"
        mocked_datetime.today.return_value = datetime.today()
        mocked_random.return_value = 333
        order_id = "donation-%s-%X-%s" % \
            ("Tor", mocked_random(), mocked_datetime.today().strftime("%H_%M_%S")) 
        ret_post = MagicMock()
        ret_post.status_code = 200
        ret_post.json.return_value = {}
        mocked_post.return_value = ret_post
        self.app.get(qs)
        mocked_post.assert_called_with(
            "http://backend.test.taler.net/proposal", json={
            "order": {
                "summary": "Donation!",
                "order_id": order_id,
                "nonce": "44",
                "amount": {
                    "value": 1,
                    "fraction": 0,
                    "currency": CURRENCY},
                "max_fee": {
                    "value": 1,
                    "fraction": 0,
                    "currency": CURRENCY},
                "order_id": order_id,
                "products": [{
                    "description": "Donation to Tor",
                    "quantity": 1,
                    "product_id": 0,
                    "price": {
                        "value": 1,
                        "fraction": 0,
                        "currency": CURRENCY}}],
                "fulfillment_url":
                    "http://localhost/fulfillment?order_id=%s" % order_id,
                "pay_url": "http://localhost/pay",
                "merchant": {
                    "instance": "Tor",
                    "address": "nowhere",
                    "name": "Kudos Inc.",
                    "jurisdiction": "none"}
        }})

class BlogTestCase(unittest.TestCase):
    def setUp(self):
        bapp.testing = True
        self.app = bapp.test_client()

    @patch("requests.post")
    def test_proposal_creation(self, mocked_post): 
        ret_post = MagicMock()
        ret_post.status_code = 200
        ret_post.json.return_value = {}
        mocked_post.return_value = ret_post
        self.app.get("/generate-contract?nonce=55&article_name=Check_Me") 
        mocked_post.assert_called_with(
            "http://backend.test.taler.net/proposal",
            json={
                "order": {
                    "summary": "Check Me",
                    "nonce": "55",
                    "amount": {
                        "value": 1,
                        "fraction": 0,
                        "currency": CURRENCY},
                    "max_fee": {
                        "value": 1,
                        "fraction": 0,
                        "currency": CURRENCY},
                    "products": [{
                            "description": "Essay: Check Me",
                            "quantity": 1,
                            "product_id": 0,
                            "price": {
                                "value": 1,
                                "fraction": 0,
                                "currency": CURRENCY}}],
                    "fulfillment_url": "http://localhost/essay/Check_Me",
                    "pay_url": "http://localhost/pay",
                    "merchant": {
                        "instance": tc["blog"]["instance"].value_string(required=True),
                        "address": "nowhere",
                        "name": "Kudos Inc.",
                        "jurisdiction": "none"},
                    "extra": {"article_name": "Check_Me"}}})

if "__main__" == __name__:
    unittest.main()
