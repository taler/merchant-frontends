"use strict";

function handleWalletPresent() {
  document.getElementById("ccfakeform").style.display = "none";
  document.getElementById("talerwait").style.display = "";

  let no_contract = document.querySelectorAll("[name=no_contract]")[0];
  if (Number(no_contract.getAttribute("value"))) {
    let contract_url = document.querySelectorAll("[name=contract_url]")[0];
    taler.offerContractFrom(decodeURIComponent(contract_url.getAttribute("value")));
  }
  else {
    let hc = document.querySelectorAll("[name=hc]")[0];
    let pay_url = document.querySelectorAll("[name=pay_url]")[0];
    let offering_url = document.querySelectorAll("[name=offering_url]")[0];
    taler.executePayment(hc.getAttribute("value"),
                         decodeURIComponent(pay_url.getAttribute("value")),
                         decodeURIComponent(offering_url.getAttribute("value")));
  }
}

function handleWalletAbsent() {
  document.getElementById("talerwait").style.display = "none";
  document.body.style.display = "";
}

taler.onPresent(handleWalletPresent);
taler.onAbsent(handleWalletAbsent);

var data = {
  cb: function(){
    var msg = document.getElementById("talerwait");
    msg.innerHTML = "Payment request timed out!";
    },
  retries: 2
  };
var index = 0;
var dots = ["/",
            "-",
            "\\",
            "|"];
function print_dots() {
  var spot = document.getElementById("action-indicator");
  if (spot)
    spot.innerHTML = dots[index];
  index++;
  index = index % dots.length;
  window.setTimeout(print_dots, 400);
}
print_dots();
