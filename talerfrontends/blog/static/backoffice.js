/*
  @licstart  The following is the entire license notice for the
  JavaScript code in this page.

  Copyright (C) 2015, 2016 INRIA

  The JavaScript code in this page is free software: you can
  redistribute it and/or modify it under the terms of the GNU
  Lesser General Public License (GNU LGPL) as published by the Free Software
  Foundation, either version 2.1 of the License, or (at your option)
  any later version.  The code is distributed WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU LGPL for more details.

  As additional permission under GNU LGPL version 2.1 section 7, you
  may distribute non-source (e.g., minimized or compacted) forms of
  that code without the copy of the GNU LGPL normally required by
  section 4, provided you include this license notice and a URL
  through which recipients can access the Corresponding Source.

  @licend  The above is the entire license notice
  for the JavaScript code in this page.
 
  @author Marcello Stanisci
*/
"use strict";

var FRACTION = 100000000;
var TIMESTAMP = 0;
var START = 0;
var DELTA = 5
var INSTANCE = "FSF";
var LAST = 0;

function amount_to_string(amount){
  var number = Number(amount.value) + (Number(amount.fraction)/FRACTION);
  return `${number.toFixed(2)} ${amount.currency}`;
}

function close_popup(){

  var ctx = document.getElementsByClassName("track-content")[0];
  var tbody = xpath_get("table/tbody", ctx).snapshotItem(0);
  var tbody_children = xpath_get("table/tbody/tr", ctx);

  for(var i=1; i<tbody_children.snapshotLength; i++){
    tbody.removeChild(tbody_children.snapshotItem(i));
  }

  toggle_overlay(true);
}

function amount_sum(a1, a2){
  if(a1.currency != a2.currency)
    throw "Currency mismatch, terminating.";
  var ret = {currency: a2.currency,
             value: 0,
             fraction: 0};
  ret.value = a1.value + a2.value;
  var fraction = a1.fraction + a2.fraction;
  ret.value += Math.floor(fraction / FRACTION);
  ret.fraction = fraction % FRACTION;
  return ret;
}

function parse_date(date){

  var split = date.match(/Date\((.*)\)/);
  var seconds;
  if(isNaN(seconds = Number(split[1]))){
    console.error("Malformed date gotten from backend");
    return;
  }
  var d = new Date(seconds * 1000);
  var months = ["Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"];
  var hours = ("0" + d.getHours()).slice(-2);
  var minutes = ("0" + d.getMinutes()).slice(-2);
  return `${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()}, ${hours}:${minutes}`;
}

function toggle_overlay(force_close){
  var overlay = document.getElementsByClassName("overlay")[0];
  if(overlay.style.visibility == "visible" || force_close){
    overlay.style.visibility = "hidden";
    overlay.style.opacity = 0;
  }
  else
  {
    overlay.style.visibility = "visible";
    overlay.style.opacity = 1;
  }

}

function make_marker(wtid){
  var tr = document.createElement("tr");
  var td = document.createElement("td");
  var p = document.createElement("p");
  td.setAttribute("colspan", 3);
  p.textContent = wtid.substr(0, 20) + "...";
  p.setAttribute("id", "marker");
  td.appendChild(p);
  tr.appendChild(td);
  return tr;
}

function track_transfer(exchange, wtid){
  var loader = document.getElementsByClassName("loader")[0]; 
  loader.style.visibility = "visible";
  var qs = `/track/transfer?exchange=${exchange}&wtid=${wtid}&instance=FSF`;
  var req = new XMLHttpRequest();
  req.open("GET", qs, true);
  req.onload = function(){
    if(4 == req.readyState){
      switch(req.status){
      case 200:
        var tracks = JSON.parse(req.responseText);
        var table = document.getElementById("history");
        var tbody = xpath_get("tbody", table).snapshotItem(0);
        var tbody_children = xpath_get("tbody/*[position() > 1]", table);
        for(var i=0; i<tbody_children.snapshotLength; i++){
          tbody.removeChild(tbody_children.snapshotItem(i));
        }
        close_popup();
        fill_table(tracks.deposits_sums, tracks.execution_time);

        // draw a line @ the bottom, mentioning the WTID.
        var table = document.getElementById("history");
        var tbody = xpath_get("tbody", table).snapshotItem(0);
        var marker = make_marker(wtid);
        tbody.appendChild(marker);
        break;
      case 400:
        console.log("Bad request, check submitted data!");
        break;
      default:
        console.log(`Status: ${req.status}, not handled.`);
      }
    }
  }
  req.send();
}

function track_order(order_id, instance){
  var req = new XMLHttpRequest();
  req.open("GET", `/track/order?order_id=${order_id}&instance=${instance}`, true);
  req.onload = function(){
    if (4 == req.readyState){
      if(200 == req.status){
        var tracks = JSON.parse(req.responseText); 
        if(!tracks)
          console.log("Got invalid JSON");
        if(0 == tracks.length){
          console.log("Got no tracks AND status == 200.  Should not be here.");
          return;
        }
        for(var i=0; i<tracks.length; i++){
          var entry = tracks[i];
          var overlay = document.getElementsByClassName("overlay")[0];
          var track_content = document.getElementsByClassName("track-content")[0];
          var table = document.evaluate("table/tbody",
                                        track_content,
                                        null,
                                        XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
                                        null).snapshotItem(0);
          var row = document.createElement("tr");
          var subject = `${entry.wtid} ${entry.exchange_url}`;
          console.log("Subject", subject);

          row.innerHTML = `<td class="wtid">
                             <a onclick='track_transfer("${entry.exchange}", "${entry.wtid}")'
                                href="#${i}">${subject.substring(0, 20)}...</a>
                           </td> 
                           <td class="amount">${amount_to_string(entry.amount)}</td>
                           <td class="date">${parse_date(entry.execution_time)}</td>`;

          table.appendChild(row);
          toggle_overlay();
        }
      }
      if (202 == req.status){
        console.log("Pending order.");
        var overlay = document.getElementsByClassName("overlay")[0];
        var track_content = document.getElementsByClassName("track-content")[0];
        track_content.innerHTML = "This order is still waiting to be paid back.";
        toggle_overlay();
      } 
      if (404 == req.status)
        alert("Order ID unknown");
    }
  }
  req.send();
}

function xpath_get(xpath, ctx){
  var ret = document.evaluate(xpath,
                              ctx,
                              null,
                              XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
                              null);
  return ret;
}

/**
 * Append results to the table showing results.
 */
function fill_table(history, execution_time){
  var table = document.getElementById("history");
  var tbody = xpath_get("tbody", table).snapshotItem(0);
  
  if(0 == history.length){
    window.setTimeout(remove_loader, 900);
    return;
  }

  /* remove "no records" message */
  var nr = xpath_get("tr[@class='no-records']", tbody).snapshotItem(0)
  if (nr)
    tbody.removeChild(nr);
  /* Make table's header visible */
  xpath_get("tr[@class='headers']", tbody).snapshotItem(0).style.visibility = "";

  for (var i=0; i<history.length; i++){
    var entry = history[i];
    var row = document.createElement("tr");
    row.className = "even";
    var row_summary = document.createElement("tr");
    row_summary.className = "odd";
    var td_order_id = document.createElement("td");
    td_order_id.className = "order-id";
    var td_summary = document.createElement("td");
    td_summary.setAttribute("colspan", 3);
    td_summary.className += "summary";
    var td_amount = document.createElement("td");
    td_amount.className = "amount";
    var td_date = document.createElement("td");
    td_date.className = "date";
    td_order_id.innerHTML = `<a href="#${i}" onclick='track_order("${entry.order_id}", "FSF");'>${entry.order_id}</a>`;
    td_summary.className = "summary";
    td_summary.innerHTML = entry.summary;
    td_amount.innerHTML = amount_to_string(entry.amount || entry.deposit_value);
    td_date.innerHTML = parse_date(entry.timestamp || execution_time);
    row.appendChild(td_order_id);
    row.appendChild(td_summary);
    row.appendChild(td_amount);
    row.appendChild(td_date);
    row_summary.appendChild(td_summary);
    tbody.appendChild(row);
    tbody.appendChild(row_summary);
    }

  function remove_loader(){
    var loader = document.getElementsByClassName("loader")[0]; 
    loader.style.visibility = "hidden";
    table.style.visibility = "";
  }
  window.setTimeout(remove_loader, 900);
}

/**
 * Issue a direrct /track via Web form.
 */
function track_cherry_pick(form){
  var types = xpath_get("input[@type='radio']", form);
  for(var i in [0, 1]){
    if (!types.snapshotItem(i).checked)
      continue;
    var type = types.snapshotItem(i).value;
    if ("order" == type){
      var order_id = xpath_get("input[@class='order']", form).snapshotItem(0);
      track_order(order_id.value, INSTANCE);
    }
    else{
      var data = xpath_get("input[@class='transfer']", form);
      var wtid = data.snapshotItem(0);
      var exchange = data.snapshotItem(1);
      track_transfer(exchange.value, wtid.value);
    }  
  }
}

/**
 * Restore the cherry-pick form to the initial state.
 * In other words, it formats the form so that /track/order
 * can be called.
 */
function cherry_pick_form_order(form){
  var input_order = xpath_get("input[@class='order']", form).snapshotItem(0);
  input_order.style.visibility = "";
  var input_transfer = xpath_get("input[@class='transfer']", form);
  for(var i in [0, 1])
    input_transfer.snapshotItem(i).style.visibility = "hidden";
}

/**
 * Change the cherry-pick form to the format useful for
 * tracking wire transfers.
 */
function cherry_pick_form_transfer(form){
  var input_order = xpath_get("input[@class='order']", form).snapshotItem(0);
  input_order.style.visibility = "hidden";
  var input_transfer = xpath_get("input[@class='transfer']", form);
  for(var i in [0, 1])
    input_transfer.snapshotItem(i).style.visibility = "";
}


/**
 * - scroll if true, the logic tries to retrieve the
 *   "next page" of all the proposals known to the merchant.
 */
function get_history(scroll){
  var loader = document.getElementsByClassName("loader")[0]; 
  loader.style.visibility = "visible";
  var qs = `/history?instance=${INSTANCE}&delta=${DELTA}`;
  if(scroll){
    START = LAST;
    qs += `&start=${START}`;
  }
  var req = new XMLHttpRequest();
  req.open("GET", qs, true);
  req.onload = function(){
    if(4 == req.readyState){
      if(200 == req.status){
        var history = JSON.parse(req.responseText); 
        if(!history){
          console.log("Got invalid JSON");
          return;
        }
        if(0 < history.length){
          console.log(req.respnseText);
          LAST = history[history.length - 1].row_id;
        }
        fill_table(history);
      }
      else{
        console.log("error: status != 200");
      }
    }
  }
  req.send();
}

document.addEventListener("DOMContentLoaded", () => get_history(false));
document.addEventListener("scroll", function(){
  if(window.innerHeight + window.scrollY >= document.body.offsetHeight)
    window.setTimeout(()=>get_history(true), 400);
});
document.onkeydown = function(e) {
  if(!e)
    e = event;
  if(e.keyCode == 27){
    close_popup();
  }
}
