<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Freedom-or-Power_003f">
 </a>
 <h1 class="chapter">
  43. Freedom or Power?
 </h1>
 <p>
  Written by
  <a name="index-Kuhn_002c-Bradley-M_002e">
  </a>
  Bradley M. Kuhn and Richard Stallman.
  <br>
   <em>
    The love of liberty is the love of others; the love of power is the love of ourselves.
   </em>
  </br>
 </p>
 <a name="index-Hazlitt_002c-William">
 </a>
 <p align="right">
  —William Hazlitt
 </p>
 <br>
  <a name="index-proprietary-software_002c-freedom-or-power_003f">
  </a>
  <p>
   In the free software movement, we stand for freedom for the users of
software. We formulated our views by looking at what freedoms are
necessary for a good way of life, and permit useful programs to foster
a community of goodwill, cooperation, and collaboration. Our criteria
for free software specify the freedoms that a program’s users need so
that they can cooperate in a community.
  </p>
  <p>
   We stand for freedom for programmers as well as for other users.
Most of us are programmers, and we want freedom for ourselves as well
as for you. But each of us uses software written by others, and we
want freedom when using that software, not just when using our own
code. We stand for freedom for all users, whether they program often,
occasionally, or not at all.
  </p>
  <p>
   However, one so-called freedom that we do not advocate is the
“freedom to choose any license you want for software you
write.” We reject this because it is really a form of power,
not a freedom.
  </p>
  <p>
   This oft overlooked distinction is crucial. Freedom is being able to make
decisions that affect mainly you; power is being able to make decisions
that affect others more than you. If we confuse power with freedom, we
will fail to uphold real freedom.
  </p>
  <a name="index-developers_002c-copyright-law-favors">
  </a>
  <p>
   Making a program proprietary is an exercise of power. Copyright law
today grants software developers that power, so they and only they
choose the rules to impose on everyone else—a relatively small
number of people make the basic software decisions for all users,
typically by denying their freedom. When users lack the
freedoms that define free software, they can’t tell what the
software is doing, can’t check for back doors, can’t monitor possible
viruses and worms, can’t find out what personal information is being
reported (or stop the reports, even if they do find out). If it breaks,
they can’t fix it; they have to wait for the developer to exercise its
power to do so. If it simply isn’t quite what they need, they are stuck
with it. They can’t help each other improve it.
  </p>
  <a name="index-Microsoft_002c-freedom-or-power_003f">
  </a>
  <p>
   Proprietary software developers are often businesses. We in the free
software movement are not opposed to business, but we have seen what
happens when a software business has the “freedom” to
impose arbitrary rules on the users of software. Microsoft is an
egregious example of how denying users’ freedoms can lead to direct
harm, but it is not the only example. Even when there is no monopoly,
proprietary software harms society. A choice of masters is not
freedom.
  </p>
  <p>
   Discussions of rights and rules for software have often concentrated
on the interests of programmers alone. Few people in the world
program regularly, and fewer still are
   <a name="index-ownership_002c-developers_0027-interests-v_002e-public_0027s-prosperity-and-freedom-1">
   </a>
   owners of proprietary software
businesses. But the entire developed world now needs and uses
software, so software developers now control the way it lives,
does business, communicates, and is entertained. The ethical and
political issues are not addressed by the slogan of “freedom of
choice (for developers only).”
   <a name="index-developers_002c-copyright-law-favors-1">
   </a>
  </p>
  <p>
   If “code is law,”
   <a href="#FOOT53" name="DOCF53">
    (53)
   </a>
   then the real question we face is: who should control the code you
use—you, or an elite few? We believe you are entitled to control the
software you use, and giving you that control is the goal of free
software.
  </p>
  <a name="index-GPL-7">
  </a>
  <p>
   We believe you should decide what to do with the software you use;
however, that is not what today’s law says. Current copyright law
places us in the position of power over users of our code, whether we
like it or not. The ethical response to this situation is to proclaim
freedom for each user, just as the Bill of Rights was supposed to
exercise government power by guaranteeing each citizen’s
freedoms. That is what the GNU General Public License is for: it puts
you in control of your usage of the software while protecting you from
others who would like to take control of your decisions.
  </p>
  <p>
   As more and more users realize that code is law, and come to feel that
they too deserve freedom, they will see the importance of the freedoms
we stand for, just as more and more users have come to appreciate the
practical value of the free software we have developed.
   <a name="index-proprietary-software_002c-freedom-or-power_003f-1">
   </a>
  </p>
  <div class="footnote">
   <hr>
    <h3>
     Footnotes
    </h3>
    <h3>
     <a href="#DOCF53" name="FOOT53">
      (53)
     </a>
    </h3>
    <p>
     William J. Mitchell,
     <em>
      City of Bits: Space, Place, and the
Infobahn
     </em>
     (Cambridge, Mass.: MIT Press, 1995), p. 111, as quoted by
Lawrence Lessig in
     <em>
      Code and Other Laws of Cyberspace, Version
2.0
     </em>
     (New York, NY: Basic Books, 2006), p. 5.
    </p>
   </hr>
  </div>
  <hr size="2"/>
 </br>

