<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="The-Free-Software-Definition">
 </a>
 <h1 class="chapter">
  1. The Free Software Definition
 </h1>
 <a name="index-free-software-_0028see-also-free-software_002c-four-freedoms_002c-citizen-values_002c-selling_002c-and-software_0029">
 </a>
 <p>
  We maintain this free software definition to show clearly what must be
true about a particular software program for it to be considered free
software. From time to time we revise this definition to clarify it.
If you would like to review the changes we’ve made, please see the
History section, following the definition, at
  <a href="http://gnu.org/philosophy/free-sw.html">
   http://gnu.org/philosophy/free-sw.html
  </a>
  .
 </p>
 <p>
  “Free software” is a matter of liberty, not price. To understand
the concept, you should think of “free” as in “free speech,”
not as in “free beer.”
 </p>
 <a name="index-free-software_002c-four-freedoms">
 </a>
 <a name="index-four-freedoms">
 </a>
 <p>
  Free software is a matter of the users’ freedom to run, copy, distribute,
study, change and improve the software. More precisely, it means that the
program’s users have the four essential freedoms:
 </p>
 <ul>
  <li>
   The freedom to run the program, for any purpose (freedom 0).
  </li>
  <li>
   The freedom to study how the program works, and change it to make it
do what you wish (freedom 1). Access to the source code is a
precondition for this.
  </li>
  <li>
   The freedom to redistribute copies so you can help your neighbor
(freedom 2).
  </li>
  <li>
   The freedom to distribute copies of your modified versions to others
(freedom 3). By doing this you can give the whole community a chance
to benefit from your changes. Access to the source code is a
precondition for this.
  </li>
 </ul>
 <p>
  A program is free software if users have all of these freedoms. Thus,
you should be free to redistribute copies, either with or without
modifications, either gratis or charging a fee for distribution, to
anyone anywhere. Being free to do these things means (among other
things) that you do not have to ask or pay for permission to do so.
 </p>
 <p>
  You should also have the freedom to make modifications and use them
privately in your own work or play, without even mentioning that they
exist. If you do publish your changes, you should not be required to
notify anyone in particular, or in any particular way.
 </p>
 <p>
  The freedom to run the program means the freedom for any kind of
person or organization to use it on any kind of computer system, for
any kind of overall job and purpose, without being required to
communicate about it with the developer or any other specific
entity. In this freedom, it is the
  <em>
   user’s
  </em>
  purpose that matters,
not the
  <em>
   developer’s
  </em>
  purpose; you as a user are free to run the
program for your purposes, and if you distribute it to someone else,
she is then free to run it for her purposes, but you are not entitled
to impose your purposes on her.
 </p>
 <p>
  The freedom to redistribute copies must include binary or executable
forms of the program, as well as source code, for both modified and
unmodified versions. (Distributing programs in runnable form is necessary
for conveniently installable free operating systems.) It is OK if there
is no way to produce a binary or executable form for a certain program
(since some languages don’t support that feature), but you must have the
freedom to redistribute such forms should you find or develop a way to
make them.
 </p>
 <p>
  In order for freedoms 1 and 3 (the freedom to make changes
and the freedom to publish improved versions) to be meaningful, you
must have access to the source code of the program. Therefore,
accessibility of source code is a necessary condition for free
software. Obfuscated “source code” is not real source code and does
not count as source code.
 </p>
 <p>
  Freedom 1 includes the freedom to use your changed version in
place of the original. If the program is delivered in a product
designed to run someone else’s modified versions but refuse to run
yours—a practice known as
  <a name="index-tivoization">
  </a>
  “tivoization” or (in its practitioners’
perverse terminology) as
  <a name="index-secure-boot-_0028see-also-tivoization_0029">
  </a>
  “secure boot”—freedom 1 becomes a theoretical fiction rather
than a practical freedom. This is not sufficient. In other words,
these binaries are not free software even if the source code they are
compiled from is free.
 </p>
 <p>
  One important way to modify a program is by merging in available free
subroutines and modules.
  <a name="index-copyright-_0028see-also-both-copyleft-and-DMCA_0029">
  </a>
  If the program’s license says that you cannot merge in a suitably
licensed existing module—for instance, if it requires you to be the
copyright holder of any code you add—then the license is too
restrictive to qualify as free.
 </p>
 <a name="index-copyleft-_0028see-also-copyright_0029-1">
 </a>
 <p>
  Freedom 3 includes the freedom to release your modified versions
as free software. A free license may also permit other ways of
releasing them; in other words, it does not have to be a copyleft
license. However, a license that requires modified versions to be
nonfree does not qualify as a free license.
 </p>
 <p>
  In order for these freedoms to be real, they must be permanent and
irrevocable as long as you do nothing wrong; if the developer of the
software has the power to revoke the license, or retroactively change
its terms, without your doing anything wrong to give cause, the
software is not free.
 </p>
 <p>
  However, certain kinds of rules about the manner of distributing free
software are acceptable, when they don’t conflict with the central
freedoms. For example, copyleft (very simply stated) is the rule that
when redistributing the program, you cannot add restrictions to deny
other people the central freedoms. This rule does not conflict with
the central freedoms; rather it protects them.
 </p>
 <a name="index-commercial-use-and-development">
 </a>
 <a name="index-free-software_002c-to-be-distinguished-from-noncommercial-software">
 </a>
 <p>
  “Free software” does not mean “noncommercial.” A free program must
be available for commercial use, commercial development, and
commercial distribution. Commercial development of free software is no
longer unusual; such free commercial software is very important. You
may have paid money to get copies of free software, or you may have
obtained copies at no charge. But regardless of how you got your
copies, you always have the freedom to copy and change the software,
even to sell copies.
 </p>
 <p>
  Whether a change constitutes an improvement is a subjective matter.
If your modifications are limited, in substance, to changes that
someone else considers an improvement, that is not freedom.
 </p>
 <p>
  However, rules about how to package a modified version are acceptable,
if they don’t substantively limit your freedom to release modified
versions, or your freedom to make and use modified versions privately.
Thus, it is acceptable for the license to require that you change the
name of the modified version, remove a logo, or identify your
modifications as yours.  As long as these requirements are not so
burdensome that they effectively hamper you from releasing your
changes, they are acceptable; you’re already making other changes to
the program, so you won’t have trouble making a few more.
 </p>
 <p>
  Rules that “if you make your version available in this way, you must
make it available in that way also” can be acceptable too, on the
same condition.  An example of such an acceptable rule is one saying
that if you have distributed a modified version and a previous
developer asks for a copy of it, you must send one.  (Note that such a
rule still leaves you the choice of whether to distribute your version
at all.)  Rules that require release of source code to the users for
versions that you put into public use are also acceptable.
 </p>
 <p>
  In the GNU Project, we use copyleft to protect these freedoms legally
for everyone. But
  <a name="index-noncopylefted-free-software-_0028see-also-software_0029">
  </a>
  noncopylefted free software also exists. We believe there are
important reasons why it is better to use copyleft, but if your
program is noncopylefted free software, it is still basically
ethical. (See “Categories of Free and Nonfree Software”
(p. @refx{Categories-pg}{) for a description of how “free software,”
“copylefted software” and other categories of software relate to
each other.)
  <a name="index-copyleft-_0028see-also-copyright_0029-2">
  </a>
 </p>
 <a name="index-free-software_002c-and-export-control-regulations">
 </a>
 <p>
  Sometimes government export control regulations and trade sanctions
can constrain your freedom to distribute copies of programs
internationally. Software developers do not have the power to
eliminate or override these restrictions, but what they can and must
do is refuse to impose them as conditions of use of the program. In
this way, the restrictions will not affect activities and people
outside the jurisdictions of these governments. Thus, free software
licenses must not require obedience to any export regulations as a
condition of any of the essential freedoms.
 </p>
 <a name="index-copyright-_0028see-also-both-copyleft-and-DMCA_0029-1">
 </a>
 <p>
  Most free software licenses are based on copyright, and there are limits
on what kinds of requirements can be imposed through copyright. If a
copyright-based license respects freedom in the ways described above, it
is unlikely to have some other sort of problem that we never anticipated
(though this does happen occasionally). However, some free software
licenses are based on contracts, and contracts can impose a much larger
range of possible restrictions. That means there are many possible ways
such a license could be unacceptably restrictive and nonfree.
 </p>
 <p>
  We can’t possibly list all the ways that might happen. If a
contract-based license restricts the user in an unusual way that
copyright-based licenses cannot, and which isn’t mentioned here as
legitimate, we will have to think about it, and we will probably conclude
it is nonfree.
 </p>
 <a name="index-call-to-action_002c-use-correct-terminology-_0028see-also-terminology_0029">
 </a>
 <a name="index-_0060_0060piracy_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  When talking about free software, it is best to avoid using terms like
“give away” or “for free,” because those terms imply that the
issue is about price, not freedom. Some common terms such as
“piracy” embody opinions we hope you won’t endorse. See “Words to
Avoid (or Use with Care)” (p. @refx{Words to Avoid-pg}{) for a discussion
of these terms. We also have a list of proper translations of “free
software” into various languages (p. @refx{FS Translations-pg}{).
 </p>
 <p>
  Finally, note that criteria such as those stated in this free software
definition require careful thought for their interpretation. To decide
whether a specific software license qualifies as a free software license,
we judge it based on these criteria to determine whether it fits their
spirit as well as the precise words. If a license includes unconscionable
restrictions, we reject it, even if we did not anticipate the issue
in these criteria. Sometimes a license requirement raises an issue
that calls for extensive thought, including discussions with a lawyer,
before we can decide if the requirement is acceptable. When we reach
a conclusion about a new issue, we often update these criteria to make
it easier to see why certain licenses do or don’t qualify.
 </p>
 <p>
  If you are interested in whether a specific license qualifies as a
free software license, see our list of licenses, at
  <a href="http://gnu.org/licenses/license-list.html">
   http://gnu.org/licenses/license-list.html
  </a>
  . If the
license you are concerned with is not listed there, you can ask us
about it by sending us email at
  <a href="mailto:licensing@gnu.org">
   licensing@gnu.org
  </a>
  .
 </p>
 <p>
  If you are contemplating writing a new license, please contact the
Free Software Foundation first by writing to that address. The
proliferation of different free software licenses means increased work
for users in understanding the licenses; we may be able to help you
find an existing free software license that meets your needs.
 </p>
 <p>
  If that isn’t possible, if you really need a new license, with our
help you can ensure that the license really is a free software license
and avoid various practical problems.
 </p>
 <a name="Beyond-Software">
 </a>
 <h3 class="subheading">
  Beyond Software
 </h3>
 <a name="index-manuals-_0028see-also-manuals_002c-FDL_002c-and-documentation_0029">
 </a>
 <p>
  Software manuals must be free, for the same reasons that software
must be free, and because the manuals are in effect part of the
software.
 </p>
 <p>
  The same arguments also make sense for other kinds of works of
practical use—that is to say, works that embody useful knowledge,
such as educational works and reference works.
  <a name="index-Wikipedia">
  </a>
  Wikipedia is the best-known example.
 </p>
 <p>
  Any kind of work
  <em>
   can
  </em>
  be free, and the definition of free software
has been extended to a definition of free cultural works
  <a href="#FOOT1" name="DOCF1">
   (1)
  </a>
  applicable to any kind of works.
  <a name="index-free-software-_0028see-also-free-software_002c-four-freedoms_002c-citizen-values_002c-selling_002c-and-software_0029-1">
  </a>
 </p>
 <div class="footnote">
  <hr>
   <h3>
    Footnotes
   </h3>
   <h3>
    <a href="#DOCF1" name="FOOT1">
     (1)
    </a>
   </h3>
   <p>
    See
    <a href="http://freedomdefined.org">
     http://freedomdefined.org
    </a>
    .
   </p>
  </hr>
 </div>
 <hr size="2"/>

