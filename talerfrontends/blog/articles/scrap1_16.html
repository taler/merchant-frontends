<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Words-to-Avoid-_0028or-Use-with-Care_0029--Because-They-Are-Loaded-or-Confusing">
 </a>
 <h1 class="chapter">
  16. Words to Avoid (or Use with Care)
  <br>
   Because They Are Loaded or Confusing
  </br>
 </h1>
 <a name="index-call-to-action_002c-use-correct-terminology-_0028see-also-terminology_0029-7">
 </a>
 <a name="index-terminology_002c-importance-of-using-correct-7">
 </a>
 <p>
  There are a number of words and phrases that we recommend avoiding, or
avoiding in certain contexts and usages. Some are ambiguous or
misleading; others presuppose a viewpoint that we hope you disagree
with. (See also “Categories of Free and Nonfree Software,” on
p. @refx{Categories-pg}{.)
 </p>
 <a name="BSD_002dStyle">
 </a>
 <h3 class="subheading">
  BSD-Style
 </h3>
 <a name="index-_0060_0060BSD_002dstyle_002c_0027_0027-problematic-term">
 </a>
 <p>
  The expression “BSD-style license” leads to confusion because it
lumps together licenses that have important differences. For instance,
the original
  <a name="index-BSD-licenses-_0028see-also-both-_0060_0060BSD_002dstyle_0027_0027-and-GPL_0029-1">
  </a>
  <a name="index-GPL_002c-BSD-license-and">
  </a>
  BSD license with the advertising clause is incompatible with the GNU
General Public License, but the revised BSD license is compatible with
the GPL.
 </p>
 <p>
  To avoid confusion, it is best to name the specific license in
question and avoid the vague term “BSD-style.”
 </p>
 <a name="Closed">
 </a>
 <h3 class="subheading">
  Closed
 </h3>
 <a name="index-_0060_0060closed_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  Describing nonfree software as “closed” clearly refers to the term
“open source.” In the free software movement, we do not want to be
confused with the open source camp, so we are careful to avoid saying
things that would encourage people to lump us in with them. For
instance, we avoid describing nonfree software as “closed.” We call
it “nonfree” or “proprietary.”
 </p>
 <p>
  @vglue -13pt@null
  <a name="Cloud-Computing">
  </a>
 </p>
 <h3 class="subheading">
  Cloud Computing
 </h3>
 <a name="index-_0060_0060cloud-computing_002c_0027_0027-avoid-use-of-term">
 </a>
 <p>
  The term “cloud computing” is a marketing buzzword with no clear
meaning. It is used for a range of different activities whose only
common characteristic is that they use the Internet for something
beyond transmitting files. Thus, the term is a nexus of confusion. If
you base your thinking on it, your thinking will be vague.
 </p>
 <p>
  When thinking about or responding to a statement someone else has made
using this term, the first step is to clarify the topic. Which kind of
activity is the statement really about, and what is a good, clear term
for that activity? Once the topic is clear, the discussion can head
for a useful conclusion.
 </p>
 <p>
  Curiously,
  <a name="index-Ellison_002c-Larry">
  </a>
  Larry Ellison, a proprietary software
  <a name="index-developers_002c-proprietary-software-2">
  </a>
  developer, also noted the vacuity of the term “cloud
computing.”
  <a href="#FOOT32" name="DOCF32">
   (32)
  </a>
  He decided to use the term anyway
{@parfillskip=0pt@parbecause, as a proprietary software developer, he isn’t motivated by
the same ideals as we are.
 </p>
 <a name="Commercial">
 </a>
 <h3 class="subheading">
  Commercial
 </h3>
 <a name="index-commercial-software-_0028see-also-software_0029-1">
 </a>
 <a name="index-_0060_0060commercial_002c_0027_0027-problematic-use-of-term">
 </a>
 <a name="index-software_002c-commercial-_0028see-also-commercial-software_0029-1">
 </a>
 <p>
  Please don’t use “commercial” as a synonym for “nonfree.” That
confuses two entirely different issues.
 </p>
 <p>
  A program is commercial if it is developed as a business activity. A
commercial program can be free or nonfree, depending on its manner of
distribution. Likewise, a program developed by a school or an
individual can be free or nonfree, depending on its manner of
distribution. The two questions—what sort of entity developed the
program and what freedom its users have—are independent.
 </p>
 <a name="index-universities-1">
 </a>
 <p>
  In the first decade of the free software movement, free software
packages were almost always noncommercial; the components of the
GNU/Linux operating system were developed by individuals or by
nonprofit organizations such as the FSF and universities. Later, in
the 1990s, free commercial software started to appear.
 </p>
 <p>
  Free commercial software is a contribution to our community, so we
should encourage it. But people who think that “commercial” means
“nonfree” will tend to think that the “free commercial”
combination is self-contradictory, and dismiss the possibility. Let’s
be careful not to use the word “commercial” in that way.
 </p>
 <a name="Compensation">
 </a>
 <h3 class="subheading">
  Compensation
 </h3>
 <a name="index-_0060_0060compensation_002c_0027_0027-false-assumptions-connected-to-term">
 </a>
 <a name="index-copyright_002c-false-assumptions-related-to-_0060_0060compensation_0027_0027-for-authors">
 </a>
 <p>
  To speak of “compensation for authors” in connection with copyright
carries the assumptions that (1) copyright exists for the sake of
authors and (2) whenever we read something, we take on a debt to the
author which we must then repay. The first assumption is simply false,
and the second is outrageous.
 </p>
 <a name="Consumer">
 </a>
 <h3 class="subheading">
  Consumer
 </h3>
 <a name="index-_0060_0060consumer_002c_0027_0027-problematic-use-of-term-_0028see-also-_0060_0060open-source_0027_0027_0029">
 </a>
 <p>
  The term “consumer,” when used to refer to computer users, is loaded
with assumptions we should reject. Playing a digital recording, or
running a program, does not consume it.
 </p>
 <p>
  The terms “producer” and “consumer” come from economic theory, and
bring with them its narrow perspective and misguided assumptions. They
tend to warp your thinking.
 </p>
 <p>
  In addition, describing the users of software as “consumers”
presumes a narrow role for them: it regards them as cattle that
passively graze on what others make available to them.
 </p>
 <p>
  This kind of thinking leads to travesties like the
  <a name="index-Consumer-Broadband-and-Digital-Television-Promotion-Act-_0028CBDTPA_0029">
  </a>
  CBDTPA, the “Consumer Broadband and Digital Television Promotion Act,”
which would require copying restriction facilities in every digital
device. If all the users do is “consume,” then why should they mind?
 </p>
 <p>
  The shallow economic conception of users as “consumers” tends to go
hand in hand with the idea that published works are mere “content.”
 </p>
 <p>
  To describe people who are not limited to passive use of works, we
suggest terms such as “individuals” and “citizens.”
 </p>
 <a name="Content">
 </a>
 <h3 class="subheading">
  Content
 </h3>
 <a name="index-_0060_0060content_002c_0027_0027-problematic-use-of-term">
 </a>
 <p>
  If you want to describe a feeling of comfort and satisfaction, by all
means say you are “content,” but using the word as a noun to
describe written and other works of authorship adopts an attitude you
might rather avoid. It regards these works as a commodity whose
purpose is to fill a box and make money. In effect, it disparages the
works themselves.
 </p>
 <p>
  Those who use this term are often the publishers that push for
increased copyright power in the name of the authors (“creators,” as
they say) of the works. The term “content” reveals their real
attitude towards these works and their authors. (See
  <a name="index-Love_002c-Courtney">
  </a>
  Courtney
Love’s open letter to
  <a name="index-Case_002c-Steve">
  </a>
  Steve Case
  <a href="#FOOT33" name="DOCF33">
   (33)
  </a>
  and search for “content provider” in that page. Alas, Ms. Love is
unaware that the term
  <a name="index-_0060_0060intellectual-property_002c_0027_0027-bias-and-fallacy-of-term-_0028see-also-ownership_0029-5">
  </a>
  “intellectual property” is also biased and confusing.)
 </p>
 <p>
  However, as long as other people use the term “content provider,”
political dissidents can well call themselves “malcontent
providers.”
 </p>
 <p>
  The term “content management” takes the prize for vacuity.
“Content” means “some sort of information,” and “management” in
this context means “doing something with it.” So a “content
management system” is a system for doing something to some sort of
information. Nearly all programs fit that description.
 </p>
 <p>
  In most cases, that term really refers to a system for updating pages
on a web site. For that, we recommend the term “web site revision
system” (WRS).
 </p>
 <a name="Creator">
 </a>
 <h3 class="subheading">
  Creator
 </h3>
 <a name="index-copyright_002c-_0060_0060creator_0027_0027">
 </a>
 <a name="index-_0060_0060creator_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  The term “creator” as applied to authors implicitly compares them to
a deity (“the creator”). The term is used by publishers to elevate
authors’ moral standing above that of ordinary people in order to
justify giving them increased copyright power, which the publishers
can then exercise in their name. We recommend saying “author”
instead. However, in many cases “copyright holder” is what you
really mean.
 </p>
 <a name="Digital-Goods">
 </a>
 <h3 class="subheading">
  Digital Goods
 </h3>
 <a name="index-_0060_0060digital-goods_002c_0027_0027-problematic-term">
 </a>
 <p>
  The term “digital goods,” as applied to copies of works of
authorship, erroneously identifies them with physical goods—which
cannot be copied, and which therefore have to be manufactured and
sold.
 </p>
 <a name="Digital-Rights-Management">
 </a>
 <h3 class="subheading">
  Digital Rights Management
 </h3>
 <a name="index-DRM_002c-call-it-_0060_0060Digital-Restrictions-Management_0027_0027">
 </a>
 <a name="index-_0060_0060Digital-Rights-Management_002c_0027_0027-avoid-use-of-term-_0028see-also-DRM_0029">
 </a>
 <p>
  “Digital Rights Management” refers to technical schemes designed to
impose restrictions on computer users. The use of the word “rights”
in this term is propaganda, designed to lead you unawares into seeing
the issue from the viewpoint of the few that impose the restrictions,
and ignoring that of the general public on whom these restrictions are
imposed.
 </p>
 <p>
  Good alternatives include “Digital Restrictions Management,” and
“digital handcuffs.”
 </p>
 <a name="Ecosystem">
 </a>
 <h3 class="subheading">
  Ecosystem
 </h3>
 <a name="index-_0060_0060ecosystem_002c_0027_0027-erroneous-description-of-free-software-community">
 </a>
 <p>
  It is a mistake to describe the free software community, or any human
community, as an “ecosystem,” because that word implies the absence
of ethical judgment.
 </p>
 <p>
  The term “ecosystem” implicitly suggests an attitude of
nonjudgmental observation: don’t ask how what
  <em>
   should
  </em>
  happen,
just study and explain what
  <em>
   does
  </em>
  happen. In an ecosystem, some
organisms consume other organisms. We do not ask whether it is fair
for an owl to eat a mouse or for a mouse to eat a plant, we only
observe that they do so. Species’ populations grow or shrink according
to the conditions; this is neither right nor wrong, merely an
ecological phenomenon.
 </p>
 <p>
  By contrast, beings that adopt an ethical stance towards their
surroundings can decide to preserve things that, on their own, might
vanish—such as civil society, democracy, human rights, peace, public
health, clean air and water, endangered species, traditional
arts…and computer users’ freedom.
 </p>
 <a name="For-Free">
 </a>
 <h3 class="subheading">
  For Free
 </h3>
 <a name="index-_0060_0060for-free_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  If you want to say that a program is free software, please don’t say
that it is available “for free.” That term specifically means “for
zero price.” Free software is a matter of freedom, not price.
 </p>
 <p>
  Free software copies are often available for free—for example, by
downloading via FTP. But free software copies are also available for a
price on CD-ROMs; meanwhile, proprietary software copies are
occasionally available for free in promotions, and some proprietary
packages are normally available at no charge to certain users.
 </p>
 <p>
  To avoid confusion, you can say that the program is available
“as free software.”
 </p>
 <a name="Freely-Available">
 </a>
 <h3 class="subheading">
  Freely Available
 </h3>
 <a name="index-_0060_0060freely-available_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  Don’t use “freely available software” as a synonym for “free
software.” The terms are not equivalent. Software is “freely
available” if anyone can easily get a copy. “Free software” is
defined in terms of the freedom of users that have a copy of it. These
are answers to different questions.
 </p>
 <a name="Freeware-1">
 </a>
 <h3 class="subheading">
  Freeware
 </h3>
 <a name="index-freeware-_0028see-also-software_0029-1">
 </a>
 <p>
  Please don’t use the term “freeware” as a synonym for “free
software.” The term “freeware” was used often in the 1980s for
programs released only as executables, with source code not
available. Today it has no particular agreed-on definition.
 </p>
 <p>
  When using languages other than English, please avoid borrowing
English terms such as “free software” or “freeware.” It is better
to translate the term “free software” into your language. (Please
see p. @refx{FS Translations-pg}{ for a list of recommended unambiguous
translations for the term “free software” into various languages.)
 </p>
 <p>
  By using a word in your own language, you show that you are really
referring to freedom and not just parroting some mysterious foreign
marketing concept. The reference to freedom may at first seem strange
or disturbing to your compatriots, but once they see that it means
exactly what it says, they will really understand what the issue is.
 </p>
 <a name="Give-Away-Software">
 </a>
 <h3 class="subheading">
  Give Away Software
 </h3>
 <a name="index-_0060_0060give-away-software_002c_0027_0027-misleading-use-of-term-1">
 </a>
 <p>
  It’s misleading to use the term “give away” to mean “distribute a
program as free software.” This locution has the same problem as
“for free”: it implies the issue is price, not freedom. One way to
avoid the confusion is to say “release as free software.”
 </p>
 <a name="Hacker">
 </a>
 <h3 class="subheading">
  Hacker
 </h3>
 <a name="index-hackers-7">
 </a>
 <a name="index-_0060_0060hacker_002c_0027_0027-actual-meaning-of-term-_0028see-also-_0060_0060cracker_0027_0027_0029-1">
 </a>
 <a name="index-MIT-5">
 </a>
 <p>
  A hacker is someone who enjoys playful cleverness
  <a href="#FOOT34" name="DOCF34">
   (34)
  </a>
  —not
necessarily with computers. The programmers in the old MIT free
software community of the 60s and 70s referred to themselves as
hackers. Around 1980, journalists who discovered the hacker community
mistakenly took the term to mean “security breaker.”
 </p>
 <p>
  Please don’t spread this mistake. People who break security are
“crackers.”
 </p>
 <a name="Intellectual-Property">
 </a>
 <h3 class="subheading">
  Intellectual Property
 </h3>
 <a name="index-_0060_0060intellectual-property_002c_0027_0027-bias-and-fallacy-of-term-_0028see-also-ownership_0029-6">
 </a>
 <a name="index-trademarks-and_002for-trademark-law-1">
 </a>
 <p>
  Publishers and lawyers like to describe copyright as “intellectual
property”—a term also applied to patents, trademarks, and other
more obscure areas of law. These laws have so little in common, and
differ so much, that it is ill-advised to generalize about them. It is
best to talk specifically about “copyright,” or about “patents,”
or about “trademarks.”
 </p>
 <p>
  The term “intellectual property” carries a hidden assumption—that
the way to think about all these disparate issues is based on an
analogy with physical objects, and our conception of them as physical
property.
 </p>
 <p>
  When it comes to copying, this analogy disregards the crucial
difference between material objects and information: information can
be copied and shared almost effortlessly, while material objects can’t
be.
 </p>
 <p>
  To avoid spreading unnecessary bias and confusion, it is best to adopt
a firm policy not to speak or even think in terms of “intellectual
property.”
 </p>
 <p>
  The hypocrisy of calling these powers “rights” is starting to make
the
  <a name="index-World-_0060_0060Intellectual-Property_0027_0027-Organization-_0028WIPO_0029-_0028see-also-_0060_0060intellectual-property_0027_0027_0029-2">
  </a>
  World “Intellectual Property” Organization embarrassed.
 </p>
 <a name="LAMP-System">
 </a>
 <h3 class="subheading">
  LAMP System
 </h3>
 <a name="index-_0060_0060LAMP-system_002c_0027_0027-problematic-term-_0028see-also-GLAMP_0029">
 </a>
 <p>
  “LAMP” stands for “Linux, Apache, MySQL and PHP”—a common
combination of software to use on a web server, except that “Linux”
in this context really refers to the GNU/Linux system. So instead of
“LAMP” it should be
  <a name="index-GLAMP-_0028GNU_002c-Linux_002c-Apache_002c-MySQL-and-PHP_0029-system">
  </a>
  <a name="index-GNU_002c-GLAMP-_0028GNU_002c-Linux_002c-Apache_002c-MySQL-and-PHP_0029-system">
  </a>
  “GLAMP”: “GNU, Linux, Apache, MySQL and PHP.”
 </p>
 <a name="Linux-System">
 </a>
 <h3 class="subheading">
  Linux System
 </h3>
 <a name="index-_0060_0060Linux-system_002c_0027_0027-avoid-use-of-term">
 </a>
 <a name="index-Torvalds_002c-Linus-1">
 </a>
 <a name="index-GNU_002c-GNU-Project-7">
 </a>
 <a name="index-kernel_002c-Linux-1">
 </a>
 <a name="index-Linux-kernel-1">
 </a>
 <a name="index-_0060_0060Linux_002c_0027_0027-erroneous-use-of-term-_0028see-also-open-source_0029-3">
 </a>
 <p>
  Linux is the name of the kernel that Linus Torvalds developed starting
in 1991. The operating system in which Linux is used is basically GNU
with Linux added. To call the whole system “Linux” is both unfair
and confusing. Please call the complete system GNU/Linux, both to give
the GNU Project credit and to distinguish the whole system from the
kernel alone.
 </p>
 <a name="Market">
 </a>
 <h3 class="subheading">
  Market
 </h3>
 <a name="index-_0060_0060market_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  It is misleading to describe the users of free software, or the
software users in general, as a “market.”
 </p>
 <p>
  This is not to say there is no room for markets in the free software
community. If you have a free software support business, then you
have clients, and you trade with them in a market. As long as you
respect their freedom, we wish you success in your market.
 </p>
 <p>
  But the free software movement is a social movement, not a business,
and the success it aims for is not a market success. We are trying to
serve the public by giving it freedom—not competing to draw business
away from a rival. To equate this campaign for freedom to a business’
efforts for mere success is to deny the importance of freedom and
legitimize proprietary software.
 </p>
 <a name="MP3-Player">
 </a>
 <h3 class="subheading">
  MP3 Player
 </h3>
 <a name="index-_0060_0060MP3-Player_002c_0027_0027-problematic-use-of-term">
 </a>
 <a name="index-MP3-1">
 </a>
 <a name="index-Ogg-Vorbis">
 </a>
 <a name="index-FLAC">
 </a>
 <p>
  In the late 1990s it became feasible to make portable, solid-state
digital audio players. Most support the patented MP3 codec, but not
all. Some support the patent-free audio codecs Ogg Vorbis and FLAC,
and may not even support MP3-encoded files at all, precisely to avoid
these patents. To call such players “MP3 players” is not only
confusing, it also puts MP3 in an undeserved position of privilege
which encourages people to continue using that vulnerable format. We
suggest the terms “digital audio player,” or simply “audio player”
if context permits.
 </p>
 <a name="Open">
 </a>
 <h3 class="subheading">
  Open
 </h3>
 <a name="index-_0060_0060open_002c_0027_0027-misleading-use-of-term-1">
 </a>
 <p>
  Please avoid using the term “open” or “open source” as a
substitute for “free software.” Those terms refer to a different
position based on different values. Free software is a political
movement; open source is a development model.
 </p>
 <p>
  When referring to the open source position, using its name is
appropriate; but please do not use it to label us or our work—that
leads people to think we share those views.
 </p>
 <a name="PC">
 </a>
 <h3 class="subheading">
  PC
 </h3>
 <a name="index-_0060_0060PC_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  It’s OK to use the abbreviation “PC” to refer to a certain kind of
computer hardware, but please don’t use it with the implication that
the computer is running Microsoft
  <a name="index-Windows-1">
  </a>
  Windows. If you install GNU/Linux on the same computer, it is still a
PC.
 </p>
 <p>
  The term “WC” has been suggested for a computer running Windows.
 </p>
 <a name="Photoshop">
 </a>
 <h3 class="subheading">
  Photoshop
 </h3>
 <a name="index-_0060_0060photoshop_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  Please avoid using the term “photoshop” as a verb, meaning any kind
of photo manipulation or image editing in general. Photoshop is just
the name of one particular image editing program, which should be
avoided since it is proprietary. There are plenty of free
alternatives, such as
  <a name="index-GIMP">
  </a>
  <a name="index-GNU_002c-GIMP">
  </a>
  GIMP.
 </p>
 <a name="Piracy">
 </a>
 <h3 class="subheading">
  Piracy
 </h3>
 <a name="index-_0060_0060piracy_002c_0027_0027-erroneous-use-of-term-4">
 </a>
 <p>
  Publishers often refer to copying they don’t approve of as “piracy.”
In this way, they imply that it is ethically equivalent to attacking
ships on the high seas, kidnapping and murdering the people on
them. Based on such propaganda, they have procured laws in most of the
world to forbid copying in most (or sometimes all)
circumstances. (They are still pressuring to make these prohibitions
more complete.)
 </p>
 <p>
  If you don’t believe that copying not approved by the publisher is
just like kidnapping and murder, you might prefer not to use the word
“piracy” to describe it. Neutral terms such as “unauthorized
copying” (or “prohibited copying” for the situation where it is
illegal) are available for use instead. Some of us might even prefer
to use a positive term such as “sharing information with your
neighbor.”
 </p>
 <a name="PowerPoint">
 </a>
 <h3 class="subheading">
  PowerPoint
 </h3>
 <a name="index-_0060_0060PowerPoint_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  Please avoid using the term “PowerPoint” to mean any kind of slide
presentation. “PowerPoint” is just the name of one particular
proprietary program to make presentations, and there are plenty of
free alternatives, such as
  <a name="index-TeX-3">
  </a>
  TeX’s
  <a name="index-beamer-class_002c-TeX">
  </a>
  <tt>
   beamer
  </tt>
  class
and
  <a name="index-OpenOffice_002eorg">
  </a>
  OpenOffice.org’s
  <a name="index-Impress_002c-OpenOffice_002eorg">
  </a>
  Impress.
 </p>
 <a name="Protection">
 </a>
 <h3 class="subheading">
  Protection
 </h3>
 <a name="index-copyright_002c-_0060_0060protection_0027_0027">
 </a>
 <a name="index-_0060_0060protection_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  Publishers’ lawyers love to use the term “protection” to describe
copyright. This word carries the implication of preventing destruction
or suffering; therefore, it encourages people to identify with the
owner and publisher who benefit from copyright, rather than with the
users who are restricted by it.
 </p>
 <p>
  It is easy to avoid “protection” and use neutral terms instead. For
example, instead of saying, “Copyright protection lasts a very long
time,” you can say, “Copyright lasts a very long time.”
 </p>
 <p>
  If you want to criticize copyright instead of supporting it, you can
use the term “copyright restrictions.” Thus, you can say,
“Copyright restrictions last a very long time.”
 </p>
 <p>
  The term “protection” is also used to describe malicious features.
For instance, “copy protection” is a feature that interferes with
copying. From the user’s point of view, this is obstruction. So we
could call that malicious feature “copy obstruction.” More often it
is called Digital Restrictions Management (DRM)—see the Defective by
Design campaign, at
  <a href="http://www.defectivebydesign.org">
   http://www.defectivebydesign.org
  </a>
  .
 </p>
 <a name="RAND-_0028Reasonable-and-Non_002dDiscriminatory_0029">
 </a>
 <h3 class="subheading">
  RAND (Reasonable and Non-Discriminatory)
 </h3>
 <a name="index-_0060_0060RAND-_0028Reasonable-and-Non_002dDiscriminatory_0029_002c_0027_0027-avoid-use-of-term-_0028see-also-patents_0029">
 </a>
 <p>
  Standards bodies that promulgate patent-restricted standards that
prohibit free software typically have a policy of obtaining patent
licenses that require a fixed fee per copy of a conforming program.
They often refer to such licenses by the term “RAND,” which stands
for “reasonable and non-discriminatory.”
 </p>
 <p>
  That term whitewashes a class of patent licenses that are normally
neither reasonable nor nondiscriminatory. It is true that these
licenses do not discriminate against any specific person, but they do
discriminate against the free software community, and that makes them
unreasonable. Thus, half of the term “RAND” is deceptive and the
other half is prejudiced.
 </p>
 <p>
  Standards bodies should recognize that these licenses are
discriminatory, and drop the use of the term “reasonable and
non-discriminatory” or “RAND” to describe them. Until they do so,
writers who do not wish to join in the whitewashing would do well to
reject that term. To accept and use it merely because patent-wielding
companies have made it widespread is to let those companies dictate
the views you express.
 </p>
 <a name="index-patents_002c-_0060_0060uniform-fee-only_0027_0027">
 </a>
 <p>
  We suggest the term “uniform fee only,” or “UFO” for short, as a
replacement. It is accurate because the only condition in these
licenses is a uniform royalty fee.
 </p>
 <a name="Sell-Software">
 </a>
 <h3 class="subheading">
  Sell Software
 </h3>
 <a name="index-selling_002c-_0060_0060sell-software_002c_0027_0027-ambiguous-term">
 </a>
 <p>
  The term “sell software” is ambiguous. Strictly speaking, exchanging
a copy of a free program for a sum of money is selling; but people
usually associate the term “sell” with proprietary restrictions on
the subsequent use of the software. You can be more precise, and
prevent confusion, by saying either “distributing copies of a program
for a fee” or “imposing proprietary restrictions on the use of a
program,” depending on what you mean.
 </p>
 <p>
  See “Selling Free Software” (p. @refx{Selling-pg}{) for further
discussion of this issue.
 </p>
 <a name="Software-Industry">
 </a>
 <h3 class="subheading">
  Software Industry
 </h3>
 <a name="index-_0060_0060software-industry_002c_0027_0027-problematic-term">
 </a>
 <p>
  The term “software industry” encourages people to imagine that
software is always developed by a sort of factory and then delivered
to “consumers.” The free software community shows this is not the
case. Software businesses exist, and various businesses develop free
and/or nonfree software, but those that develop free software are not
run like factories.
 </p>
 <p>
  The term “industry” is being used as propaganda by advocates of
software patents. They call software development “industry” and then
try to argue that this means it should be subject to patent
monopolies. The
  <a name="index-European-Parliament">
  </a>
  <a name="index-European-Union_002c-proposed-European-Union-software-patents-directive">
  </a>
  <a name="index-patents_002c-proposed-European-Union-software-patents-directive">
  </a>
  European Parliament, rejecting software patents in
2003,
  <a href="#FOOT35" name="DOCF35">
   (35)
  </a>
  voted to define “industry” as “automated
production of material goods.”
 </p>
 <a name="Theft">
 </a>
 <h3 class="subheading">
  Theft
 </h3>
 <a name="index-_0060_0060theft_002c_0027_0027-erroneous-use-of-term-1">
 </a>
 <p>
  Copyright apologists often use words like “stolen” and “theft” to
describe copyright infringement. At the same time, they ask us to
treat the legal system as an authority on ethics: if copying is
forbidden, it must be wrong.
 </p>
 <p>
  So it is pertinent to mention that the legal system—at least in the
US—rejects the idea that copyright infringement is “theft.”
Copyright apologists are making an appeal to authority…and
misrepresenting what authority says.
 </p>
 <p>
  The idea that laws decide what is right or wrong is mistaken in
general. Laws are, at their best, an attempt to achieve justice; to
say that laws define justice or ethical conduct is turning things
upside down.
 </p>
 <a name="Trusted-Computing">
 </a>
 <h3 class="subheading">
  Trusted Computing
 </h3>
 <a name="index-_0060_0060trusted-computing_002c_0027_0027-avoid-use-of-term-_0028see-also-treacherous-computing_0029">
 </a>
 <p>
  “Trusted computing” is the proponents’ name for a scheme to redesign
computers so that application
  <a name="index-developers_002c-proprietary-software-3">
  </a>
  developers can trust your computer to obey them instead of you. From
their point of view, it is “trusted”; from your point of view, it is
  <a name="index-treacherous-computing">
  </a>
  “treacherous.”
 </p>
 <a name="Vendor">
 </a>
 <h3 class="subheading">
  Vendor
 </h3>
 <a name="index-_0060_0060vendor_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  Please don’t use the term “vendor” to refer generally to anyone that
develops or packages software. Many programs are developed in order to
sell copies, and their
  <a name="index-developers_002c-term-_0060_0060vendor_0027_0027-and">
  </a>
  developers are therefore their vendors; this even includes some free
software packages. However, many programs are developed by volunteers
or organizations which do not intend to sell copies. These developers
are not vendors. Likewise, only some of the packagers of GNU/Linux
distributions are vendors. We recommend the general term “supplier”
instead.
 </p>
 <a name="index-terminology_002c-importance-of-using-correct-8">
 </a>
 <a name="index-call-to-action_002c-use-correct-terminology-_0028see-also-terminology_0029-8">
 </a>
 <div class="footnote">
  <hr>
   <h3>
    Footnotes
   </h3>
   <h3>
    <a href="#DOCF32" name="FOOT32">
     (32)
    </a>
   </h3>
   <p>
    Dan Farber, “Oracle’s Ellison Nails Cloud
Computing,” 26 September 2008,
    <a href="http://news.cnet.com/8301-13953_3-10052188-80.html">
     http://news.cnet.com/8301-13953_3-10052188-80.html
    </a>
    .
@vglue -1pc
   </p>
   <h3>
    <a href="#DOCF33" name="FOOT33">
     (33)
    </a>
   </h3>
   <p>
    An unedited transcript of American rock musician
Courtney Love’s 16 May 2000 speech to the Digital Hollywood
online-entertainment conference, in New York, is available at
    <a href="http://salon.com/technology/feature/2000/06/14/love/print.html">
     http://salon.com/technology/feature/2000/06/14/love/print.html
    </a>
    .
@vglue -1pc
   </p>
   <h3>
    <a href="#DOCF34" name="FOOT34">
     (34)
    </a>
   </h3>
   <p>
    See my
article, “On Hacking,” at
    <a href="http://stallman.org/articles/on-hacking.html">
     http://stallman.org/articles/on-hacking.html
    </a>
    .
@vglue -1pc
   </p>
   <h3>
    <a href="#DOCF35" name="FOOT35">
     (35)
    </a>
   </h3>
   <p>
    “Directive on the patentability of
computer-implemented inventions,” 24 September 2003,
    <a href="http://eupat.ffii.org/papers/europarl0309">
     http://eupat.ffii.org/papers/europarl0309
    </a>
    .
@vglue -1pc
   </p>
  </hr>
 </div>
 <hr size="2"/>

