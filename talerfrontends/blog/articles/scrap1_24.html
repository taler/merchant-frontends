<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Software-Patents-and-Literary-Patents">
 </a>
 <h1 class="chapter">
  24. Software Patents and Literary Patents
 </h1>
 <a name="index-patents_002c-analogy-between-literary-and-software">
 </a>
 <a name="index-Hugo_002c-Victor">
 </a>
 <a name="index-patents_002c-proposed-European-Union-software-patents-directive-1">
 </a>
 <a name="index-European-Union_002c-proposed-European-Union-software-patents-directive-1">
 </a>
 <p>
  When politicians consider the question of software patents, they are
usually voting blind; not being programmers, they don’t understand
what software patents really do. They often think patents are similar
to copyright law (“except for some details”)—which
is not the case. For instance, when I publicly asked
  <a name="index-Devedjian_002c-Minister-Patrick">
  </a>
  Patrick
Devedjian, then Minister for Industry in
  <a name="index-France-1">
  </a>
  France, how France would vote
on the issue of software patents, Devedjian responded with an
impassioned defense of copyright law, praising Victor Hugo for his
role in the adoption of copyright. (The misleading
term “intellectual property” promotes this confusion—one of the reasons it
should never be used.)
 </p>
 <p>
  Those who imagine effects like those of copyright law cannot grasp the
disastrous effects of software patents. We can use Victor Hugo as an
example to illustrate the difference.
 </p>
 <p>
  A novel and a modern complex program have certain points in common:
each one is large, and implements many ideas in combination. So let’s
follow the analogy, and suppose that patent law had been applied to
novels in the 1800s; suppose that states such as France had permitted
the patenting of literary ideas. How would this have affected Victor
Hugo’s writing? How would the effects of literary patents compare
with the effects of literary copyright?
 </p>
 <a name="index-Les-Miserables_002c-Victor-Hugo">
 </a>
 <p>
  Consider Victor Hugo’s novel
  <cite>
   Les Misérables.
  </cite>
  Since he
wrote it, the copyright belonged only to him. He
did not have to fear that some stranger could sue him for copyright
infringement and win. That was impossible, because copyright covers
only the details of a work of authorship, not the ideas embodied in
them, and it only restricts copying. Hugo had not copied
  <cite>
   Les
Misérables,
  </cite>
  so he was not in danger from copyright.
 </p>
 <p>
  Patents work differently. Patents cover ideas; each patent is a
monopoly on practicing some idea, which is described in the patent
itself. Here’s one example of a hypothetical literary patent:
 </p>
 <ul>
  <li>
   Claim 1: a communication process that represents in the mind of a reader the concept of a character who has been in jail for a long time and becomes bitter towards society and humankind.
  </li>
  <li>
   Claim 2: a communication process according to claim 1, wherein said character subsequently finds moral redemption through the kindness of another.
  </li>
  <li>
   Claim 3: a communication process according to claims 1 and 2, wherein said character changes his name during the story.
  </li>
 </ul>
 <a name="index-Valjean_002c-literary-character-Jean-_0028see-also-Les-Miserables_0029">
 </a>
 <p>
  If such a patent had existed in 1862 when
  <cite>
   Les Misérables
  </cite>
  was
published, the novel would have conflicted with all three claims,
since all these things happened to Jean Valjean in the novel. Victor
Hugo could have been sued, and if sued, he would have lost. The novel
could have been prohibited—in effect, censored—by the
patent holder.
 </p>
 <p>
  Now consider this hypothetical literary patent:
 </p>
 <ul>
  <li>
   Claim 1: a communication process that represents in the mind of a reader the concept of a character who has been in jail for a long time and subsequently changes his name.
  </li>
 </ul>
 <p>
  <cite>
   Les Misérables
  </cite>
  would have been prohibited by that patent too,
because this description too fits the life story of Jean Valjean. And
here’s another hypothetical patent:
 </p>
 <ul>
  <li>
   Claim 1: a communication process that represents in the mind of a reader the concept of a character who finds moral redemption and then changes his name.
  </li>
 </ul>
 <p>
  Jean Valjean would have been forbidden by this patent too.
 </p>
 <p>
  All three patents would cover, and prohibit, the life story of this one
character. They overlap, but they do not precisely duplicate each other,
so they could all be valid simultaneously; all three patent holders
could have sued Victor Hugo. Any one of them could have prohibited
publication of
  <cite>
   Les Misérables.
  </cite>
 </p>
 <p>
  This patent also could have been violated:
 </p>
 <ul>
  <li>
   Claim 1: a communication process that presents a character whose given name matches the last syllable of his family name.
  </li>
 </ul>
 <p>
  through the name “Jean Valjean,” but at least this patent
would have been easy to avoid.
 </p>
 <p>
  You might think that these ideas are so simple that no patent office
would have issued them. We programmers are often amazed by the
simplicity of the ideas that real software patents cover—for
instance, the
  <a name="index-European-Patent-Office">
  </a>
  European Patent Office has issued a patent on the
progress bar, and a patent on accepting payment via credit cards.
These patents would be laughable if they were not so dangerous.
 </p>
 <p>
  Other aspects of
  <cite>
   Les Misérables
  </cite>
  could also have
run afoul of
patents. For instance, there could have been a patent on a
fictionalized portrayal of the Battle of Waterloo, or a patent on
using Parisian slang in fiction. Two more lawsuits. In fact, there
is no limit to the number of different patents that might have been
applicable for suing the author of a work such as
  <cite>
   Les
Misérables.
  </cite>
  All the patent holders would say they deserved a
reward for the literary progress that their patented ideas represent,
but these obstacles would not promote progress in literature, they
would only obstruct it.
 </p>
 <p>
  However, a very broad patent could have made all these issues
irrelevant. Imagine a patent with broad claims like these:
 </p>
 <ul>
  <li>
   A communication process structured with narration that continues
through many pages.
  </li>
  <li>
   A narration structure sometimes resembling a fugue or
improvisation.
  </li>
  <li>
   Intrigue articulated around the confrontation of specific
characters, each in turn setting traps for the others.
  </li>
  <li>
   Narration that presents many layers of society.
  </li>
  <li>
   Narration that shows the wheels of hidden conspiracy.
  </li>
 </ul>
 <p>
  Who would the patent holders have been? They could have been
other novelists, perhaps Dumas or Balzac, who had written such
novels—but not necessarily. It isn’t required to write a
program to patent a software idea, so if our hypothetical literary
patents follow the real patent system, these patent holders would not
have had to write novels, or stories, or anything—except patent
applications. Patent parasite companies, businesses that produce
nothing except threats and lawsuits, are booming nowadays.
 </p>
 <p>
  Given these broad patents, Victor Hugo would not have reached
the point of asking what patents might get him sued for using the
character of Jean Valjean, because he could not even have considered
writing a novel of this kind.
  <a name="index-Valjean_002c-literary-character-Jean-_0028see-also-Les-Miserables_0029-1">
  </a>
  <a name="index-Les-Miserables_002c-Victor-Hugo-1">
  </a>
  <a name="index-Hugo_002c-Victor-1">
  </a>
 </p>
 <p>
  This analogy can help nonprogrammers see what software patents
do. Software patents cover features, such as defining abbreviations in
a word processor, or natural order recalculation in a spreadsheet.
Patents cover algorithms that programs need to use. Patents cover
aspects of file formats, such as Microsoft’s
  <a name="index-Microsoft_002c-OOXML-format-_0028see-also-patents_0029">
  </a>
  OOXML format.
  <a name="index-MPEG_002d2">
  </a>
  MPEG 2
video format is covered by 39 different US patents.
 </p>
 <p>
  Just as one novel could run afoul of many different literary patents at
once, one program can be prohibited by many different patents at once.
It is so much work to identify all the patents that appear to apply
to a large program that only one such study has been done. A 2004 study of
Linux, the
  <a name="index-kernel_002c-Linux-2">
  </a>
  <a name="index-Linux-kernel-2">
  </a>
  kernel of the GNU/Linux operating system, found 283
different US software patents that seemed to cover it. That is to
say, each of these 283 different patents forbids some computational
process found somewhere in the thousands of pages of source code of
Linux. At the time, Linux was around 1 percent of the whole
GNU/Linux system. How many patents might there be that a distributor
of the whole system could be sued under?
 </p>
 <a name="index-call-to-action_002c-do-not-authorize-software-patents">
 </a>
 <p>
  The way to prevent software patents from bollixing software
development is simple: don’t authorize them. This ought to be easy,
since most patent laws have provisions against software patents. They
typically say that “software per se” cannot be patented.
But patent offices around the world are trying to twist the words and
issuing patents on the ideas implemented in programs. Unless this is
blocked, the result will be to put all software developers in danger.
  <a name="index-patents_002c-analogy-between-literary-and-software-1">
  </a>
 </p>
 <hr size="2"/>

