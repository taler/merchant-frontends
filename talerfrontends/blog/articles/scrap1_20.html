<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Freedom_002d_002d_002dor-Copyright">
 </a>
 <h1 class="chapter">
  20. Freedom—or Copyright
 </h1>
 <blockquote class="smallquotation">
  <p>
   This essay addresses how the principles of software freedom apply in
some cases to other works of authorship and art.  It’s included here
since it involves the application of the ideas of free software.
  </p>
 </blockquote>
 <br>
  <p>
   Copyright was established in the age of the printing press as an
industrial regulation on the business of writing and publishing. The
aim was to encourage the publication of a diversity of written works.
The means was to require publishers to get the author’s permission to
publish recent writings. This enabled authors to get income from
publishers, which facilitated and encouraged writing. The general
reading public received the benefit of this, while losing little:
copyright restricted only publication, not the things an ordinary
reader could do. That made copyright arguably a beneficial system for
the public, and therefore arguably legitimate.
  </p>
  <p>
   Well and good—back then.
  </p>
  <p>
   Now we have a new way of distributing information: computers and
networks. Their benefit is that they facilitate copying and
manipulating information, including software, musical recordings,
books, and movies. They offer the possibility of unlimited access to
all sorts of data—an information utopia.
  </p>
  <p>
   One obstacle stood in the way: copyright. Readers and listeners who
made use of their new ability to copy and share published information
were technically copyright infringers. The same law which had
formerly acted as a beneficial industrial regulation on publishers had
become a restriction on the public it was meant to serve.
  </p>
  <p>
   In a democracy, a law that prohibits a popular and useful activity is
usually soon relaxed. Not so where corporations have political power.
The publishers’ lobby was determined to prevent the public from taking
advantage of the power of their computers, and found copyright a
handy weapon. Under their influence, rather than relaxing copyright
rules to suit the new circumstances, governments made them stricter than
ever, imposing harsh penalties on the practice of sharing. The latest
fashion in supporting the publishers against the citizens, known as 
“three strikes,” is to cut off people’s Internet connections if
they share.
  </p>
  <p>
   But that wasn’t the worst of it. Computers can be powerful tools of
domination when software suppliers deny users the control of the
software they run. The
publishers realized that by publishing works in encrypted format,
which only specially authorized software could view, they could gain
unprecedented power: they could compel readers to pay, and identify
themselves, every time they read a book, listen to a song, or watch a
video. That is the publishers’ dream: a
   <a name="index-pay_002dper_002dview">
   </a>
   pay-per-view universe.
  </p>
  <a name="index-DMCA_002c-publishers-and-1">
  </a>
  <p>
   The publishers gained US government support for their dream with the
Digital Millennium Copyright Act of 1998. This law gave publishers
power to write their own copyright rules, by implementing them in the
code of the authorized player software. Under this practice, called
Digital Restrictions Management, or
   <a name="index-DRM_002c-call-it-_0060_0060Digital-Restrictions-Management_0027_0027-1">
   </a>
   DRM, even reading or listening
without authorization is forbidden.
  </p>
  <a name="index-e_002dbooks-2">
  </a>
  <p>
   We still have the same old freedoms in using paper books and other
analog media. But if e-books replace printed books, those freedoms
will not transfer. Imagine: no more used book stores; no more lending
a book to your friend; no more borrowing one from the public
   <a name="index-libraries_002c-e_002dbooks-and-1">
   </a>
   library—no more “leaks” that might give someone a
chance to read without paying. No more purchasing a book anonymously with
cash—you can only buy an e-book with a credit card. That is
the world the publishers want to impose on us. If you buy the
   <a name="index-Amazon">
   </a>
   Amazon
   <a name="index-Kindle-_0028see-also-Swindle_0029">
   </a>
   Kindle (we call it the
   <a name="index-Swindle">
   </a>
   Swindle) or the
   <a name="index-Sony-Reader-_0028call-it-the-Shreader_0029">
   </a>
   Sony Reader (we
call it the Shreader for what it threatens to do to books), you pay to
establish that world.
  </p>
  <p>
   The
   <a name="index-Swindle-1">
   </a>
   Swindle even has an Orwellian back door that can be used to erase
books remotely. Amazon demonstrated this capability by erasing
copies, purchased from Amazon, of
   <a name="index-Orwell_002c-George">
   </a>
   Orwell’s book
   <a name="index-1984_002c-George-Orwell">
   </a>
   <cite>
    1984.
   </cite>
   Evidently
Amazon’s name for this product reflects the intention to burn our
books.
  </p>
  <p>
   Public anger against DRM is slowly growing, held back because
propaganda expressions such
as
   <a name="index-_0060_0060protection_002c_0027_0027-erroneous-use-of-term-1">
   </a>
   “protect
authors”
and
   <a name="index-_0060_0060intellectual-property_002c_0027_0027-bias-and-fallacy-of-term-_0028see-also-ownership_0029-7">
   </a>
   “intellectual
property” have convinced readers that their rights do not
count. These terms implicitly assume that publishers deserve special
power in the name of the authors, that we are morally obliged to bow
to them, and that we have wronged someone if we see or hear
anything without paying for permission.
  </p>
  <p>
   The organizations that profit most from copyright legally exercise it
in the name of the authors (most of whom gain little). They would
have you believe that copyright is a natural right of authors, and
that we the public must suffer it no matter how painful it is. They
call sharing
   <a name="index-_0060_0060piracy_002c_0027_0027-erroneous-use-of-term-7">
   </a>
   “piracy,” equating helping your neighbor with
attacking a ship.
  </p>
  <a name="index-War-on-Sharing-_0028see-also-DRM-and-copyright_0029">
  </a>
  <p>
   They also tell us that a War on Sharing is the only way to keep
art alive. Even if true, it would not justify the policy; but it
isn’t true. Public sharing of copies is likely to increase the sales of
most works, and decrease sales only for big hits.
  </p>
  <a name="index-e_002dbooks-3">
  </a>
  <p>
   Bestsellers can still do well without forbidding sharing.
   <a name="index-King_002c-Stephen">
   </a>
   Stephen
King got hundreds of thousands of dollars selling an unencrypted
e-book serial with no obstacle to copying and sharing. (He was
dissatisfied with that amount and called the experiment a failure, but it looks
like a success to me.)
   <a name="index-Radiohead">
   </a>
   Radiohead made millions in 2007 by inviting
fans to copy an album and pay what they wished, while it was also
shared through
   <a name="index-peer_002dto_002dpeer">
   </a>
   peer-to-peer. In
2008,
   <a name="index-Nine-Inch-Nails">
   </a>
   Nine Inch Nails released an album with permission to share copies and
made $750,000 in a few days.
   <a href="#FOOT43" name="DOCF43">
    (43)
   </a>
  </p>
  <p>
   The possibility of success without oppression is not limited to
bestsellers. Many artists of various levels of fame now make an
adequate living through voluntary support:
   <a href="#FOOT44" name="DOCF44">
    (44)
   </a>
   donations and merchandise purchases of their fans.
   <a name="index-Kelly_002c-Kevin">
   </a>
   Kevin Kelly
   <a href="#FOOT45" name="DOCF45">
    (45)
   </a>
   estimates the artist need
only find around 1,000 true fans.
   <a href="#FOOT46" name="DOCF46">
    (46)
   </a>
  </p>
  <p>
   When computer networks provide an easy anonymous method for sending
someone a small amount of money, without a credit card, it will be
easy to set up a much better system to support the arts. When you
view a work, there will be a button you can press saying, “Click
here to send the artist one dollar.” Wouldn’t you press it, at
least once a week?
  </p>
  <p>
   Another good way to support music and the arts is with
tax funds—perhaps a tax on blank media
or on Internet connectivity. The state should
distribute the tax money entirely to the artists, not
waste it on corporate executives. But the state should not distribute
it in linear proportion to popularity, because that would give most of
it to a few superstars, leaving little to support all the other
artists. I therefore recommend using a cube-root function or
something similar. With linear proportion, superstar A with 1,000
times the popularity of a successful artist B will get 1,000 times as
much money as B. With the cube root, A will get 10 times as much as
B. Thus, each superstar gets a larger share than a less popular
artist, but most of the funds go to the artists who really need this
support. This system will use our tax money efficiently to support
the arts.
  </p>
  <a name="index-Global-Patronage-_0028see-also-DRM-and-copyright_0029">
  </a>
  <p>
   The Global Patronage
   <a href="#FOOT47" name="DOCF47">
    (47)
   </a>
   proposal
combines aspects of those two systems, incorporating mandatory
payments with voluntary allocation among artists.
  </p>
  <a name="index-Spain-1">
  </a>
  <p>
   In Spain, this tax system should replace the
   <a name="index-SGAE">
   </a>
   SGAE
   <a href="#FOOT48" name="DOCF48">
    (48)
   </a>
   and its canon,
which could be eliminated.
  </p>
  <a name="index-call-to-action_002c-boycott-products-with-DRM">
  </a>
  <a name="index-call-to-action_002c-legalize-noncommercial-copying-and-sharing-of-all-published-works">
  </a>
  <p>
   To make copyright fit the network age, we should legalize the
noncommercial copying and sharing of all published works, and prohibit
DRM. But until we win this battle, you must protect yourself: don’t
buy any products with DRM unless you personally have the means to
break the DRM. Never use a product designed to attack your freedom
unless you can nullify the attack.
   <a name="index-DRM_002c-call-it-_0060_0060Digital-Restrictions-Management_0027_0027-2">
   </a>
  </p>
  <div class="footnote">
   <hr>
    <h3>
     Footnotes
    </h3>
    <h3>
     <a href="#DOCF43" name="FOOT43">
      (43)
     </a>
    </h3>
    <p>
     “Nine Inch Nails Made at Least $750k from CC Release in Two Days,” posted by Cory Doctorow, 5 March 2008,
     <a href="http://boingboing.net/2008/03/05/nine-inch-nails-made.html">
      http://boingboing.net/2008/03/05/nine-inch-nails-made.html
     </a>
     .
    </p>
    <h3>
     <a href="#DOCF44" name="FOOT44">
      (44)
     </a>
    </h3>
    <p>
     Mike Masnick,
“The Future of Music Business Models (and Those Who Are Already
There),” 25 January 2010,
     <a href="http://techdirt.com/articles/20091119/1634117011.shtml">
      http://techdirt.com/articles/20091119/1634117011.shtml
     </a>
     .
    </p>
    <h3>
     <a href="#DOCF45" name="FOOT45">
      (45)
     </a>
    </h3>
    <p>
     Kevin Kelly is a commentator on digital culture
and the founder of
     <cite>
      Wired
     </cite>
     magazine.
    </p>
    <h3>
     <a href="#DOCF46" name="FOOT46">
      (46)
     </a>
    </h3>
    <p>
     Kevin Kelly, “1,000 True
Fans,” 4 March 2008,
     <a href="http://kk.org/thetechnium/archives/2008/03/1000_true_fans.php">
      http://kk.org/thetechnium/archives/2008/03/1000_true_fans.php
     </a>
     .
    </p>
    <h3>
     <a href="#DOCF47" name="FOOT47">
      (47)
     </a>
    </h3>
    <p>
     See
     <a href="http://mecenatglobal.org/">
      http://mecenatglobal.org/
     </a>
     for more information.
    </p>
    <h3>
     <a href="#DOCF48" name="FOOT48">
      (48)
     </a>
    </h3>
    <p>
     The SGAE is Spain’s main copyright collective for composers, authors,
and publishers.
    </p>
   </hr>
  </div>
  <hr size="2"/>
 </br>

