<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Categories-of-Free-and-Nonfree-Software">
 </a>
 <h1 class="chapter">
  13. Categories of Free and Nonfree Software
 </h1>
 <div style="display:none">
  <p id="teaser">
   Free software is software that comes with permission for
	anyone to use, copy, and/or distribute, either verbatim or with
	modifications, either gratis or for a fee. In particular, this
	means that source code must be available. “If it’s not
	source, it’s not software.”
  </p>
 </div>
 <a name="index-call-to-action_002c-use-correct-terminology-_0028see-also-terminology_0029-3">
 </a>
 <img alt="category" src="/essay/13._Categories_of_Free_and_Nonfree_Software/data/category.jpg">
  <blockquote class="smallquotation">
   <p>
    <em>
     This diagram, originally by Chao-Kuei and updated by several
others since, explains the different categories of software. It’s
available at
     <a href="http://gnu.org/philosophy/categories.html">
      http://gnu.org/philosophy/categories.html
     </a>
     as
a Scalable Vector Graphic and as an XFig document, under the terms of
any of the GNU GPL v2 or later, the GNU FDL v1.2 or later, or the
Creative Commons Attribution-Share Alike v2.0 or later. To view a copy
of the Creative Commons license, visit
     <a href="http://creativecommons.org/licenses/by-sa/2.0">
      http://creativecommons.org/licenses/by-sa/2.0
     </a>
     , or
send a letter to Creative Commons, 171 Second Street,
Suite 300, San Francisco, California 94105, USA.
    </em>
   </p>
  </blockquote>
  <br>
   <a name="Free-Software">
   </a>
   <h3 class="subheading">
    Free Software
   </h3>
   <a name="index-software_002c-free-_0028see-also-free-software_0029">
   </a>
   <a name="index-free-software-_0028see-also-free-software_002c-four-freedoms_002c-citizen-values_002c-selling_002c-and-software_0029-2">
   </a>
   <p>
    Free software is software that comes with permission for
	anyone to use, copy, and/or distribute, either verbatim or with
	modifications, either gratis or for a fee. In particular, this
	means that source code must be available. “If it’s not
	source, it’s not software.” This is a simplified
	description; see also the full definition (p. @refx{Definition-pg}{).
   </p>
   <p>
    If a program is free, then it can potentially be included
	in a free operating system such as GNU, or free versions of
	the GNU/Linux
	system.
   </p>
   <p>
    There are many different ways to make a program free—many
	questions of detail, which could be decided in more than one way
	and still make the program free. Some of the possible variations
	are described below. For information on specific free software
	licenses, see the license list page, at
    <a href="http://gnu.org/licenses/license-list.html">
     http://gnu.org/licenses/license-list.html
    </a>
    .
   </p>
   <p>
    Free software is a matter of freedom, not price. But
	proprietary software companies typically use the term
	“free software” to refer to price. Sometimes they
	mean that you can obtain a binary copy at no charge; sometimes
	they mean that a copy is bundled with a computer that you are
	buying, and the price includes both. Either way, it has
	nothing to do with what we mean by free software in the GNU
	Project.
   </p>
   <p>
    Because of this potential confusion, when a software company
	says its product is free software, always check the actual
	distribution terms to see whether users really have all the
	freedoms that free software implies. Sometimes it really is free
	software; sometimes it isn’t.
   </p>
   <p>
    Many languages have two separate words for
	“free” as in freedom and “free” as in
	zero price. For example, French has “libre” and
	“gratuit.” Not so English; there is a word
	“gratis” that refers unambiguously to price, but
	no common adjective that refers unambiguously to freedom. So
	if you are speaking another language, we suggest you translate
	“free” into your language to make it clearer. See
	our list of
	translations of the term “free software” into
	various other languages (p. @refx{FS Translations-pg}{).
   </p>
   <p>
    Free software is often more reliable than nonfree software.
    <a name="index-software_002c-free-_0028see-also-free-software_0029-1">
    </a>
    <a name="index-free-software-_0028see-also-free-software_002c-four-freedoms_002c-citizen-values_002c-selling_002c-and-software_0029-3">
    </a>
   </p>
   <a name="Open-Source-Software">
   </a>
   <h3 class="subheading">
    Open Source Software
   </h3>
   <a name="index-software_002c-open-source-_0028see-also-open-source-software_0029">
   </a>
   <a name="index-open-source-software-_0028see-also-software_0029">
   </a>
   <p>
    The term “open source” software is used by some
	people to mean more or less the same category as free
	software. It is not exactly the same class of software: they
	accept some licenses that we consider too restrictive, and
	there are free software licenses they have not
	accepted. However, the differences in extension of the
	category are small: nearly all free software is open source,
	and nearly all open source software is free.
    <a name="index-free-software_002c-essential-difference-between-open-source-and-1">
    </a>
    <a name="index-open-source_002c-essential-difference-between-free-software-and-1">
    </a>
   </p>
   <p>
    We prefer the term “free
	software” because it refers to
	freedom—something that the term “open
	source” does not do.
   </p>
   <a name="Public-Domain-Software">
   </a>
   <h3 class="subheading">
    Public Domain Software
   </h3>
   <a name="index-software_002c-public-domain-_0028see-also-public-domain-software_0029">
   </a>
   <a name="index-public-domain-software-_0028see-also-software_0029-2">
   </a>
   <a name="index-copyright_002c-public-domain-software-and-_0028see-also-public-domain-software_0029">
   </a>
   <p>
    Public domain software is software that is not copyrighted. If
	the source code is in the public domain, that is a special case of
	noncopylefted free
	software, which means that some copies or modified versions
	may not be free at all.
   </p>
   <p>
    In some cases, an executable program can be in the public
domain but the source code is not available. This is not free
software, because free software requires accessibility of source code.
Meanwhile, most free software is not in the 

public domain; it is
copyrighted, and the copyright holders have legally given permission
for everyone to use it in freedom, using a free software license.
   </p>
   <p>
    Sometimes people use the term “public domain”
	in a loose fashion to
	mean “free” or
	“available gratis.” However, “public
	domain” is a legal term and means, precisely, “not
	copyrighted.” For clarity, we recommend using
	“public domain” for that meaning only, and using
	other terms to convey the other meanings.
   </p>
   <p>
    Under the
    <a name="index-copyright_002c-Berne-Convention">
    </a>
    <a name="index-Berne-Convention-_0028see-also-copyright_0029">
    </a>
    Berne Convention, which most countries have
	signed, anything written down is automatically
	copyrighted. This includes programs. Therefore, if you want a
	program you have written to be in the public domain, you must
	take some legal steps to disclaim the copyright on it;
	otherwise, the program is copyrighted.
    <a name="index-software_002c-public-domain-_0028see-also-public-domain-software_0029-1">
    </a>
    <a name="index-public-domain-software-_0028see-also-software_0029-3">
    </a>
    <a name="index-copyright_002c-public-domain-software-and-_0028see-also-public-domain-software_0029-1">
    </a>
   </p>
   <a name="Copylefted-Software">
   </a>
   <h3 class="subheading">
    Copylefted Software
   </h3>
   <a name="index-software_002c-copylefted-_0028see-also-copylefted-software_0029">
   </a>
   <a name="index-copylefted-software-_0028see-also-software_0029">
   </a>
   <p>
    Copylefted software is free software whose distribution terms ensure
that all copies of all versions carry more or less the same
distribution terms. This means, for instance, that copyleft licenses
generally disallow others to add additional requirements to the
software (though a limited set of safe added requirements can be
allowed) and require making source code available. This shields the
program, and its modified versions, from some of the common ways of
making a program proprietary.
   </p>
   <p>
    Some copyleft licenses, such as GPL version 3, block other
means of turning software proprietary, such as tivoization.
   </p>
   <p>
    In the GNU Project, we copyleft almost all the software we
write, because our goal is to give
    <em>
     every
    </em>
    user the freedoms
implied by the term “free software.” See the essay “Copyleft”
(p. @refx{Copyleft-pg}{) for more explanation of how copyleft works and
why we use it.
   </p>
   <a name="index-copyleft_002c-and-GPL">
   </a>
   <p>
    Copyleft is a general concept; to copyleft an actual program,
you need to use a specific set of distribution terms. There are many
possible ways to write copyleft distribution terms, so in principle
there can be many copyleft free software licenses. However, in actual
practice nearly all copylefted software uses the GNU General Public
License. Two different copyleft licenses are usually “incompatible,”
which means it is illegal to merge the code using one license with the
code using the other license; therefore, it is good for the community
if people use a single copyleft license.
   </p>
   <a name="Noncopylefted-Free-Software">
   </a>
   <h3 class="subheading">
    Noncopylefted Free Software
   </h3>
   <a name="index-software_002c-noncopylefted-free-_0028see-also-noncopylefted-free-software_0029">
   </a>
   <a name="index-noncopylefted-free-software-_0028see-also-software_0029-1">
   </a>
   <p>
    Noncopylefted free software comes from the author with
	permission to redistribute and modify, and also to add additional
	restrictions to it.
   </p>
   <p>
    If a program is free but not copylefted, then some copies
	or modified versions may not be free at all. A software
	company can compile the program, with or without
	modifications, and distribute the executable file as
	a proprietary software product.
   </p>
   <a name="index-X-Window-System-3">
   </a>
   <a name="index-X-Consortium-_0028see-also-Open-Group_002c-its-successor_0029">
   </a>
   <a name="index-X11-licenses">
   </a>
   <p>
    The X Window System illustrates this. The X Consortium
releases X11 with distribution terms that make it noncopylefted free
software. If you wish, you can get a copy which has those distribution
terms and is free. However, there are nonfree versions as well, and
there are (or at least were) popular workstations and PC graphics
boards for which nonfree versions are the only ones that work. If you
are using this hardware, X11 is not free software for you. The
developers of X11 even made X11 nonfree for a while; they were able to
do this because others had contributed their code under the same
noncopyleft license.
   </p>
   <a name="index-X-Window-System-4">
   </a>
   <a name="index-X-Consortium-_0028see-also-Open-Group_002c-its-successor_0029-1">
   </a>
   <a name="index-X11-licenses-1">
   </a>
   <a name="index-software_002c-noncopylefted-free-_0028see-also-noncopylefted-free-software_0029-1">
   </a>
   <a name="index-noncopylefted-free-software-_0028see-also-software_0029-2">
   </a>
   <a name="Lax-Permissive-Licensed-Software">
   </a>
   <h3 class="subheading">
    Lax Permissive Licensed Software
   </h3>
   <a name="index-lax-permissive-licensed-software">
   </a>
   <a name="index-software_002c-lax-permissive-licensed">
   </a>
   <p>
    Lax permissive licenses include the
    <a name="index-X11-licenses-2">
    </a>
    X11 license and the two
    <a name="index-BSD-licenses-_0028see-also-both-_0060_0060BSD_002dstyle_0027_0027-and-GPL_0029">
    </a>
    BSD
licenses. These licenses permit almost any use of the code, including
distributing proprietary binaries with or without changing the source
code.
   </p>
   <a name="GPL_002dCovered-Software">
   </a>
   <h3 class="subheading">
    GPL-Covered Software
   </h3>
   <a name="index-software_002c-GPL_002dcovered-_0028see-also-GPL_002dcovered-software_0029">
   </a>
   <a name="index-GPL_002c-GPL_002dcovered-software-_0028see-also-software_0029">
   </a>
   <a name="index-GPL_002dcovered-software-_0028see-also-software_0029">
   </a>
   <p>
    The GNU GPL (General Public
	License) is one specific set of distribution terms for
	copylefting a program. The GNU Project uses it as the distribution
	terms for most GNU software.
   </p>
   <p>
    To equate free software with GPL-covered software is therefore
	an error.
   </p>
   <a name="The-GNU-Operating-System">
   </a>
   <h3 class="subheading">
    The GNU Operating System
   </h3>
   <a name="index-software_002c-GNU-operating-system-_0028see-also-GNU_0029">
   </a>
   <a name="index-GNU_002c-GNU-operating-system-_0028see-also-both-software-and-GNU_0029">
   </a>
   <p>
    The GNU operating system is the
	Unix-like operating system, which is entirely free software, that
	we in the GNU Project have developed since 1984.
   </p>
   <a name="index-TeX-2">
   </a>
   <p>
    A Unix-like operating system consists of many programs. The GNU
	system includes all the GNU software, as well as many other
	packages, such as the X Window System and TeX, which are not GNU
	software.
   </p>
   <a name="index-Hurd_002c-GNU-1">
   </a>
   <a name="index-GNU_002c-GNU-Hurd-2">
   </a>
   <a name="index-kernel_002c-GNU-Hurd-1">
   </a>
   <p>
    The first test release of the complete GNU system was in
	1996. This includes the GNU Hurd, our kernel, developed since
	1990. In 2001 the GNU system (including the GNU Hurd) began
	working fairly reliably, but the Hurd still lacks some
	important features, so it is not widely used. Meanwhile,
	the GNU/Linux system,
	an offshoot of the GNU operating system which uses Linux as
	the kernel instead of the GNU Hurd, has been a great success
	since the 90s.
   </p>
   <p>
    Since the purpose of GNU is to be free, every single
	component in the GNU operating system has to be free
	software. They don’t all have to be copylefted, however; any
	kind of free software is legally suitable to include if it
	helps meet technical goals. And it isn’t necessary for all the
	components to be GNU software, individually. GNU can and does
	include noncopylefted free software such as the X Window
	System that were developed by other projects.
   </p>
   <a name="GNU-Programs">
   </a>
   <h3 class="subheading">
    GNU Programs
   </h3>
   <a name="index-software_002c-GNU-programs-_0028see-also-GNU-programs_0029">
   </a>
   <a name="index-GNU_002c-GNU-programs-_0028see-also-software_0029-1">
   </a>
   <p>
    “GNU programs” is equivalent
	to GNU software. A program Foo is a
	GNU program if it is GNU software. We also sometimes say it
	is a “GNU package.”
   </p>
   <a name="GNU-Software">
   </a>
   <h3 class="subheading">
    GNU Software
   </h3>
   <a name="index-software_002c-GNU_0028see-also-GNU-software_0029">
   </a>
   <a name="index-GNU-_0028see-also-both-software-and-GNU_0029-3">
   </a>
   <p>
    GNU software is
	software that is released under the auspices of the GNU Project. If a program is GNU
	software, we also say that it is a GNU program or a GNU
	package. The README or manual of a GNU package should say it
	is one; also,

        the Free Software Directory identifies all GNU packages.
   </p>
   <p>
    Most GNU software is copylefted, but not all; however,
	all GNU software must be free software.
   </p>
   <a name="index-FSF_002c-software-development">
   </a>
   <a name="index-FSF_002c-copyright-on-software">
   </a>
   <p>
    Some GNU software was written by staff of the Free Software
Foundation, but most GNU software comes from many volunteers. (Some of
these volunteers are paid by companies or universities, but they are
volunteers for us.) Some contributed software is copyrighted by the
Free Software Foundation; some is copyrighted by the contributors who
wrote it.
    <a name="index-software_002c-GNU_0028see-also-GNU-software_0029-1">
    </a>
    <a name="index-GNU-_0028see-also-both-software-and-GNU_0029-4">
    </a>
   </p>
   <a name="Nonfree-Software">
   </a>
   <h3 class="subheading">
    Nonfree Software
   </h3>
   <a name="index-software_002c-nonfree-_0028see-also-nonfree-software_0029">
   </a>
   <a name="index-nonfree-software-_0028see-also-software_0029">
   </a>
   <p>
    Nonfree software is any software that is not free.
	Its use, redistribution or modification is prohibited, or
	requires you to ask for permission, or is restricted so much
	that you effectively can’t do it freely.
   </p>
   <a name="Proprietary-Software">
   </a>
   <h3 class="subheading">
    Proprietary Software
   </h3>
   <a name="index-software_002c-proprietary-_0028see-also-proprietary-software_0029">
   </a>
   <a name="index-proprietary-software-_0028see-also-software_0029">
   </a>
   <p>
    Proprietary software is another name for nonfree software.
	In the past we subdivided nonfree software into
	“semifree software,” which could be modified and
	redistributed noncommercially, and “proprietary
	software,” which could not be. But we have dropped that
	distinction and now use “proprietary software” as
	synonymous with nonfree software.
   </p>
   <a name="index-FSF_002c-on-installing-proprietary-software">
   </a>
   <p>
    The Free Software Foundation follows the rule that we cannot
	install any proprietary program on our computers except temporarily
	for the specific purpose of writing a free replacement for that
	very program. Aside from that, we feel there is no possible excuse
	for installing a proprietary program.
   </p>
   <p>
    For example, we felt justified in installing Unix on our
	computer in the 1980s, because we were using it to write a free
	replacement for Unix. Nowadays, since free operating systems are
	available, the excuse is no longer applicable; we do not use any
	nonfree operating systems, and any new computer we install
	must run a completely free operating system.
   </p>
   <p>
    We don’t insist that users of GNU, or contributors to GNU, have
	to live by this rule. It is a rule we made for ourselves. But we
	hope you will follow it too, for your freedom’s sake.
   </p>
   <a name="Freeware">
   </a>
   <h3 class="subheading">
    Freeware
   </h3>
   <a name="index-software_002c-freeware-_0028see-also-freeware_0029">
   </a>
   <a name="index-_0060_0060freeware_002c_0027_0027-erroneous-use-of-term">
   </a>
   <p>
    The term “freeware” has no clear accepted
	definition, but it is commonly used for packages which permit
	redistribution but not modification (and their source code is
	not available). These packages are
    <em>
     not
    </em>
    free software,
	so please don’t use “freeware” to refer to free
	software.
   </p>
   <a name="Shareware">
   </a>
   <h3 class="subheading">
    Shareware
   </h3>
   <a name="index-software_002c-shareware">
   </a>
   <a name="index-shareware-_0028see-also-software_0029">
   </a>
   <p>
    Shareware is software which comes with permission for people to
	redistribute copies, but says that anyone who continues to use a
	copy is
    <em>
     required
    </em>
    to pay a license fee.
   </p>
   <p>
    Shareware is not free software, or even semifree. There are two
	reasons it is not:
   </p>
   <ul>
    <li>
     For most shareware, source code is not available; thus, you cannot modify the program at all.
    </li>
    <li>
     Shareware does not come with permission to make a copy and install it without paying a license fee, not even for individuals engaging in nonprofit activity. (In practice, people often	disregard the distribution terms and do this anyway, but the terms don’t permit it.)
    </li>
   </ul>
   <a name="index-software_002c-shareware-1">
   </a>
   <a name="index-shareware-_0028see-also-software_0029-1">
   </a>
   <a name="Private-Software">
   </a>
   <h3 class="subheading">
    Private Software
   </h3>
   <a name="index-software_002c-private">
   </a>
   <a name="index-private-software-_0028see-also-software_0029">
   </a>
   <a name="index-development_002c-private-software">
   </a>
   <p>
    Private or custom software is software developed for one user
	(typically an organization or company). That user keeps it and uses
	it, and does not release it to the public either as source code or
	as binaries.
   </p>
   <p>
    A private program is free software in a trivial sense if its
	sole user has full rights to it.
   </p>
   <p>
    In general we do not believe it is wrong to develop a program
	and not release it. There are occasions when a program is so useful
	that withholding it from release is treating humanity badly.
	However, most programs are not that important, so not releasing them
	is not particularly harmful. Thus, there is no conflict between the
	development of private or custom software and the principles of the
	free software movement.
   </p>
   <p>
    Nearly all employment for programmers is in development of
	custom software; therefore most programming jobs are, or could be,
	done in a way compatible with the free software movement.
   </p>
   <a name="Commercial-Software">
   </a>
   <h3 class="subheading">
    Commercial Software
   </h3>
   <a name="index-software_002c-commercial-_0028see-also-commercial-software_0029">
   </a>
   <a name="index-commercial-software-_0028see-also-software_0029">
   </a>
   <a name="index-commercial-software_002c-to-be-distinguished-from-proprietary-software">
   </a>
   <a name="index-proprietary-software_002c-to-be-distinguished-from-commercial-software">
   </a>
   <a name="index-development_002c-commercial-software">
   </a>
   <p>
    Commercial software is software being developed by a
	business which aims to make money from the use of the
	software. “Commercial” and
	“proprietary” are not the same thing! Most
	commercial software
	is proprietary, but there
	is commercial free software, and there is noncommercial
	nonfree software.
   </p>
   <a name="index-GNU_002c-GNU-Ada-compiler-1">
   </a>
   <a name="index-Ada-compiler_002c-GNU-1">
   </a>
   <p>
    For example, GNU Ada is developed by a company. It is always
	distributed under the terms of the GNU GPL, and every copy is
	free software; but its developers sell support contracts. When
	their salesmen speak to prospective customers, sometimes the
	customers say, “We would feel safer with a commercial
	compiler.” The salesmen reply, “GNU
	Ada
    <em>
     is
    </em>
    a commercial compiler; it happens to be free
	software.”
   </p>
   <p>
    For the GNU Project, the emphasis is in the other order:
	the important thing is that GNU Ada is free software; whether
	it is commercial is just a detail. However, the additional
	development of GNU Ada that results from its being commercial
	is definitely beneficial.
   </p>
   <p>
    Please help spread the awareness that free commercial
	software is possible. You can do this by making an effort not
	to say “commercial” when you mean
	“proprietary.”
    <a name="index-call-to-action_002c-use-correct-terminology-_0028see-also-terminology_0029-4">
    </a>
   </p>
   <hr size="2"/>
  </br>
 </img>

