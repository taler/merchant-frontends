<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="What-Is-Copyleft_003f">
 </a>
 <h1 class="chapter">
  21. What Is Copyleft?
 </h1>
 <a name="index-GPL-2">
 </a>
 <a name="index-copyleft-_0028see-also-copyright_0029-3">
 </a>
 <a name="index-copylefted-software-_0028see-also-software_0029-1">
 </a>
 <p>
  Copyleft is a general method for making a program (or
other work) free, and requiring all modified and extended versions of the
program to be free as well.
 </p>
 <a name="index-public-domain-software-_0028see-also-software_0029-4">
 </a>
 <p>
  The simplest way to make a program free software is to put it in the
public domain, uncopyrighted. This allows people to
share the program and their improvements, if they are so minded. But
it also allows uncooperative people to convert the program into
proprietary software. They can make changes, many or few,
and distribute the result as a proprietary product. People who
receive the program in that modified form do not have the freedom that
the original author gave them; the middleman has stripped it away.
 </p>
 <p>
  In the GNU Project, our aim is
to give
  <em>
   all
  </em>
  users the freedom to redistribute and change GNU
software. If middlemen could strip off the freedom, we might have
many users, but those users would not have freedom. So instead of
putting GNU software in the public domain, we “copyleft”
it. Copyleft says that anyone who redistributes the software, with or
without changes, must pass along the freedom to further copy and
change it. Copyleft guarantees that every user has freedom.
 </p>
 <p>
  Copyleft also provides an incentive for other programmers to add to
free software. Important free programs such as the
  <a name="index-GNU_002c-GNU-C_002b_002b-compiler">
  </a>
  GNU C++ compiler
exist only because of this.
 </p>
 <p>
  Copyleft also helps programmers who want to contribute improvements to
free software get permission to
do so. These programmers often work for companies or universities
that would do almost anything to get more money. A programmer may
want to contribute her changes to the community, but her employer may
want to turn the changes into a proprietary software product.
 </p>
 <p>
  When we explain to the employer that it is illegal to distribute the
improved version except as free software, the employer usually decides
to release it as free software rather than throw it away.
 </p>
 <p>
  To copyleft a program, we first state that it is copyrighted; then we
add distribution terms, which are a legal instrument that gives
everyone the rights to use, modify, and redistribute the program’s
code,
  <em>
   or any program derived from it,
  </em>
  but only if the
distribution terms are unchanged. Thus, the code and the freedoms
become legally inseparable.
 </p>
 <p>
  Proprietary software developers use copyright to take away the users’
freedom; we use copyright to guarantee their freedom. That’s why we
reverse the name, changing “copyright” into
“copyleft.”
 </p>
 <p>
  Copyleft is a way of using of the copyright on the program. It
doesn’t mean abandoning the copyright; in fact, doing so would make
copyleft impossible. The “left” in
“copyleft” is not a reference to the verb “to
leave”—only to the direction which is the inverse of
“right.”
 </p>
 <p>
  Copyleft is a general concept, and you can’t use a general concept
directly; you can only use a specific implementation of the concept.
In the GNU Project, the specific distribution terms that we use for
most software are contained in the GNU General Public License. The GNU General Public License is often called the GNU GPL for
short. There is also a Frequently Asked Questions page about the GNU
GPL, at
  <a href="http://gnu.org/licenses/gpl-faq.html">
   http://gnu.org/licenses/gpl-faq.html
  </a>
  . You can also
read about why the FSF gets copyright assignments from contributors,
at
  <a href="http://gnu.org/copyleft/why-assign.html">
   http://gnu.org/copyleft/why-assign.html
  </a>
  .
 </p>
 <a name="index-libraries-_0028comp_002e_0029_002c-LGPL-and">
 </a>
 <a name="index-LGPL_002c-and-GNU-libraries">
 </a>
 <a name="index-GNU_002c-GNU-libraries">
 </a>
 <a name="index-libraries-_0028comp_002e_0029_002c-GNU-1">
 </a>
 <p>
  An alternate form of copyleft, the GNU Lesser General Public License
(LGPL), applies to a few (but not all) GNU libraries. To
learn more about properly using the LGPL, please read the article
“Why You Shouldn’t Use the Lesser GPL for Your Next Library,”
available at
  <a href="http://gnu.org/philosophy/why-not-lgpl.html">
   http://gnu.org/philosophy/why-not-lgpl.html
  </a>
  .
 </p>
 <a name="index-manuals_002c-FDL-and">
 </a>
 <a name="index-FDL-_0028see-also-both-manuals-and-documentation_0029-1">
 </a>
 <p>
  The GNU Free Documentation License (FDL) is a form of
copyleft intended for use on a manual, textbook or other document to
assure everyone the effective freedom to copy and redistribute it,
with or without modifications, either commercially or noncommercially.
 </p>
 <p>
  The appropriate license is included in many manuals and in each GNU
source code distribution.
 </p>
 <p>
  All these licenses are designed so that you can easily apply them to
your own works, assuming you are the copyright holder. You don’t have
to modify the license to do this, just include a copy of the license
in the work, and add notices in the source files that refer properly
to the license.
 </p>
 <a name="index-LGPL_002c-altering-distribution-terms-to-GPL">
 </a>
 <p>
  Using the same distribution terms for many different programs makes it
easy to copy code between various different programs. When they all
have the same distribution terms, there is no problem. The Lesser
GPL, version 2, includes a provision that lets you alter the
distribution terms to the ordinary GPL, so that you can copy code into
another program covered by the GPL. Version 3 of the Lesser GPL is
built as an exception added to GPL version 3, making the compatibility
automatic.
 </p>
 <p>
  If you would like to copyleft your program with the GNU GPL or the GNU
LGPL, please see the license instructions page, at
  <a href="http://gnu.org/copyleft/gpl-howto.html">
   http://gnu.org/copyleft/gpl-howto.html
  </a>
  , for advice.
Please note that you must use the entire text of the license you
choose. Each is an integral whole, and partial copies are not
permitted.
 </p>
 <p>
  If you would like to copyleft your manual with the GNU FDL, please see
the instructions at the end of the FDL text, and the GFDL
instructions page, at
  <a href="http://gnu.org/copyleft/fdl-howto.html">
   http://gnu.org/copyleft/fdl-howto.html
  </a>
  . Again, partial
copies are not permitted.
  <a name="index-copylefted-software-_0028see-also-software_0029-2">
  </a>
  <a name="index-copyleft-_0028see-also-copyright_0029-4">
  </a>
  <a name="index-GPL-3">
  </a>
 </p>
 <hr size="2"/>

