<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Why-Free-Software-Needs-Free-Documentation">
 </a>
 <h1 class="chapter">
  9. Why Free Software Needs Free Documentation
 </h1>
 <a name="index-documentation-_0028see-also-both-FDL-and-manuals_0029-2">
 </a>
 <a name="index-manuals-_0028see-also-manuals_002c-FDL_002c-and-documentation_0029-1">
 </a>
 <p>
  The biggest deficiency in free operating systems is not in the
software—it is the lack of good free manuals that we can include
in these systems. Many of our most important programs do not come
with full manuals. Documentation is an essential part of any software
package; when an important free software package does not come with a
free manual, that is a major gap. We have many such gaps today.
 </p>
 <a name="index-Perl">
 </a>
 <p>
  Once upon a time, many years ago, I thought I would learn Perl. I got
a copy of a free manual, but I found it hard to read. When I asked
Perl users about alternatives, they told me that there were better
introductory manuals—but those were not free.
 </p>
 <p>
  Why was this? The authors of the good manuals had written them for
  <a name="index-O_0027Reilly-Associates">
  </a>
  O’Reilly Associates, which published them with restrictive
terms—no copying, no modification, source files not
available—which exclude them from the free software
community.
 </p>
 <p>
  That wasn’t the first time this sort of thing has happened, and (to
our community’s great loss) it was far from the last. Proprietary
manual publishers have enticed a great many authors to restrict their
manuals since then. Many times I have heard a GNU user eagerly tell
me about a manual that he is writing, with which he expects to help
the
  <a name="index-GNU_002c-GNU-Project-3">
  </a>
  GNU Project—and then had my hopes dashed, as he proceeded to
explain that he had signed a contract with a publisher that would
restrict it so that we cannot use it.
 </p>
 <p>
  Given that writing good English is a rare skill among programmers, we
can ill afford to lose manuals this way.
 </p>
 <p>
  Free documentation, like free software, is a matter of freedom, not
price. The problem with these manuals was not that O’Reilly
Associates charged a price for printed copies—that in itself is
fine. (The
  <a name="index-FSF_002c-and-selling-GNU-manuals">
  </a>
  <a name="index-manuals_002c-GNU">
  </a>
  <a name="index-GNU_002c-GNU-manuals">
  </a>
  Free Software Foundation sells printed
copies of free GNU manuals, too.) But
GNU manuals are available in source code form, while these manuals are
available only on paper. GNU manuals come with permission to copy and
modify; the Perl manuals do not. These restrictions are the problems.
 </p>
 <p>
  The criterion for a free manual is pretty much the same as for free
software: it is a matter of giving all users certain freedoms.
Redistribution (including commercial redistribution) must be
permitted, so that the manual can accompany every copy of the program,
on line or on paper. Permission for modification is crucial too.
 </p>
 <p>
  As a general rule, I don’t believe that it is essential for people to
have permission to modify all sorts of articles and books. The issues
for writings are not necessarily the same as those for software. For
example, I don’t think you or I are obliged to give permission to
modify articles like this one, which describe our actions and our
views.
 </p>
 <p>
  But there is a particular reason why the freedom to modify is crucial
for documentation for free software. When people exercise their right
to modify the software, and add or change its features, if they are
conscientious they will change the manual too—so they can provide
accurate and usable documentation with the modified program. A manual
which forbids programmers from being conscientious and finishing the job, or
more precisely requires them to write a new manual from scratch if
they change the program, does not fill our community’s needs.
 </p>
 <p>
  While a blanket prohibition on modification is unacceptable, some
kinds of limits on the method of modification pose no problem. For
example, requirements to preserve the original author’s copyright
notice, the distribution terms, or the list of authors, are OK. It is
also no problem to require modified versions to include notice that
they were modified, even to have entire sections that may not be
deleted or changed, as long as these sections deal with nontechnical
topics. (Some GNU manuals have them.)
 </p>
 <p>
  These kinds of restrictions are not a problem because, as a practical
matter, they don’t stop the conscientious programmer from adapting the
manual to fit the modified program. In other words, they don’t block
the free software community from making full use of the manual.
 </p>
 <p>
  However, it must be possible to modify all the
  <em>
   technical
  </em>
  content of the manual, and then distribute the result through all the usual
media, through all the usual channels; otherwise, the restrictions do
block the community, the manual is not free, and so we need another
manual.
 </p>
 <p>
  Unfortunately, it is often hard to find someone to write another
manual when a proprietary manual exists. The obstacle is that many
users think that a proprietary manual is good enough—so they
don’t see the need to write a free manual. They do not see that the
free operating system has a gap that needs filling.
 </p>
 <p>
  Why do users think that proprietary manuals are good enough? Some
have not considered the issue. I hope this article will do something
to change that.
 </p>
 <a name="index-citizen-values_002c-proprietary-manuals">
 </a>
 <p>
  Other users consider proprietary manuals acceptable for the same
reason so many people consider proprietary software acceptable: they
judge in purely practical terms, not using freedom as a criterion.
These people are entitled to their opinions, but since those opinions
spring from values which do not include freedom, they are no guide for
those of us who do value freedom.
 </p>
 <a name="index-call-to-action_002c-promote-free-documentation">
 </a>
 <p>
  Please spread the word about this issue. We continue to lose manuals
to proprietary publishing. If we spread the word that proprietary
manuals are not sufficient, perhaps the next person who wants to help
GNU by writing documentation will realize, before it is too late, that
he must above all make it free.
 </p>
 <p>
  We can also encourage commercial publishers to sell free, copylefted
manuals instead of proprietary ones. One way you can help this is to
check the distribution terms of a manual before you buy it, and
prefer copylefted manuals to noncopylefted ones.
 </p>
 <p>
  <b>
   Note:
  </b>
  We maintain a page that lists free books available from other publishers.
  <a name="index-documentation-_0028see-also-both-FDL-and-manuals_0029-3">
  </a>
  <a name="index-manuals-_0028see-also-manuals_002c-FDL_002c-and-documentation_0029-2">
  </a>
 </p>
 <hr size="2"/>

