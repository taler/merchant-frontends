<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="The-Right-to-Read_003a-A-Dystopian-Short-Story">
 </a>
 <h1 class="chapter">
  17. The Right to Read: A Dystopian Short Story
 </h1>
 <a name="index-_0060_0060Right-to-Read_003a-A-Dystopian-Short-Story_0027_0027-_0028see-also-DMCA_002c-DRM_002c-fair-use_002c-and-libraries_0029">
 </a>
 <p>
  <em>
   From
   <cite>
    <span class="roman">
     The Road to Tycho,
    </span>
   </cite>
   a collection of articles about the antecedents of the Lunarian Revolution, published in Luna City in 2096.
  </em>
  <br>
   For Dan Halbert, the road to Tycho began in college—when Lissa
Lenz asked to borrow his computer. Hers had broken down, and unless
she could borrow another, she would fail her midterm project. There
was no one she dared ask, except Dan.
  </br>
 </p>
 <p>
  This put Dan in a dilemma. He had to help her—but if he lent
her his computer, she might read his books. Aside from the fact that
you could go to prison for many years for letting someone else read
your books, the very idea shocked him at first. Like everyone, he had
been taught since elementary school that sharing books was nasty and
wrong—something that only pirates would do.
 </p>
 <p>
  And there wasn’t much chance that the SPA—the Software
Protection Authority—would fail to catch him. In his software
class, Dan had learned that each book had a copyright monitor that
reported when and where it was read, and by whom, to Central
Licensing. (They used this information to catch reading pirates, but
also to sell personal interest profiles to retailers.) The next time
his computer was networked, Central Licensing would find out. He, as
computer owner, would receive the harshest punishment—for not
taking pains to prevent the crime.
 </p>
 <p>
  Of course, Lissa did not necessarily intend to read his books. She
might want the computer only to write her midterm. But Dan knew she
came from a middle-class family and could hardly afford the tuition,
let alone her reading fees. Reading his books might be the only way
she could graduate. He understood this situation; he himself had had
to borrow to pay for all the research papers he read. (Ten percent of those
fees went to the researchers who wrote the papers; since Dan aimed for
an academic career, he could hope that his own research papers, if
frequently referenced, would bring in enough to repay this loan.)
 </p>
 <p>
  Later on, Dan would learn there was a time when anyone could go to the
library and read journal articles, and even books, without having to
pay. There were independent scholars who read thousands of pages
without government library grants. But in the 1990s, both commercial
and nonprofit journal publishers had begun charging fees for access.
By 2047, libraries offering free public access to scholarly literature
were a dim memory.
 </p>
 <p>
  There were ways, of course, to get around the SPA and Central
Licensing. They were themselves illegal. Dan had had a classmate in
software, Frank Martucci, who had obtained an illicit debugging tool,
and used it to skip over the copyright monitor code when reading
books. But he had told too many friends about it, and one of them
turned him in to the SPA for a reward (students deep in debt were
easily tempted into betrayal). In 2047, Frank was in prison, not for
pirate reading, but for possessing a debugger.
 </p>
 <p>
  Dan would later learn that there was a time when anyone could have
debugging tools. There were even free debugging tools available on CD
or downloadable over the net. But ordinary users started using them
to bypass copyright monitors, and eventually a judge ruled that this
had become their principal use in actual practice. This meant they
were illegal; the debuggers’ developers were sent to prison.
 </p>
 <p>
  Programmers still needed debugging tools, of course, but debugger
vendors in 2047 distributed numbered copies only, and only to
officially licensed and bonded programmers. The debugger Dan used in
software class was kept behind a special firewall so that it could be
used only for class exercises.
 </p>
 <p>
  It was also possible to bypass the copyright monitors by installing a
modified system kernel. Dan would eventually find out about the free
kernels, even entire free operating systems, that had existed around
the turn of the century. But not only were they illegal, like
debuggers—you could not install one if you had one, without
knowing your computer’s root password. And neither
the FBI nor Microsoft Support would tell you that.
 </p>
 <p>
  Dan concluded that he couldn’t simply lend Lissa his computer. But he
couldn’t refuse to help her, because he loved her. Every chance to
speak with her filled him with delight. And that she chose him to ask
for help, that could mean she loved him too.
 </p>
 <p>
  Dan resolved the dilemma by doing something even more
unthinkable—he lent her the computer, and told her his password.
This way, if Lissa read his books, Central Licensing would think he
was reading them. It was still a crime, but the SPA would not
automatically find out about it. They would only find out if Lissa
reported him.
 </p>
 <p>
  Of course, if the school ever found out that he had given Lissa his
own password, it would be curtains for both of them as students,
regardless of what she had used it for. School policy was that any
interference with their means of monitoring students’ computer use was
grounds for disciplinary action. It didn’t matter whether you did
anything harmful—the offense was making it hard for the
administrators to check on you. They assumed this meant you were
doing something else forbidden, and they did not need to know what it
was.
 </p>
 <p>
  Students were not usually expelled for this—not directly.
Instead they were banned from the school computer systems, and would
inevitably fail all their classes.
 </p>
 <p>
  Later, Dan would learn that this kind of university policy started
only in the 1980s, when university students in large numbers began
using computers. Previously, universities maintained a different
approach to student discipline; they punished activities that were
harmful, not those that merely raised suspicion.
 </p>
 <p>
  Lissa did not report Dan to the SPA. His decision to help her led to
their marriage, and also led them to question what they had been
taught about piracy as children. The couple began reading about the
history of copyright, about the Soviet Union and its restrictions on
copying, and even the original United States Constitution. They moved
to Luna, where they found others who had likewise gravitated away from
the long arm of the SPA. When the Tycho Uprising began in 2062, the
universal right to read soon became one of its central aims.
  <a name="index-_0060_0060Right-to-Read_003a-A-Dystopian-Short-Story_0027_0027-_0028see-also-DMCA_002c-DRM_002c-fair-use_002c-and-libraries_0029-1">
  </a>
 </p>
 <a name="index-DMCA-_0028see-also-_0060_0060Right-to-Read_002c_0027_0027-fair-use_002c-DRM_002c-and-libraries_0029-1">
 </a>
 <a name="index-Digital-Millennium-Copyright-Act-_0028DMCA_0029-_0028see-also-DMCA_002c-_0060_0060Right-to-Read_002c_0027_0027-fair-use_002c-DRM_002c-and-libraries_0029">
 </a>
 <p>
  The right to read is a battle being fought today. Although it may
take 50 years for our present way of life to fade into obscurity, most
of the specific laws and practices described above have already been
proposed; many have been enacted into law in the US and elsewhere. In
the US, the 1998 Digital Millennium Copyright Act (DMCA) established the legal
basis to restrict the reading and lending of computerized books (and
other works as well). The
  <a name="index-European-Union">
  </a>
  European Union imposed similar restrictions
in a 2001 copyright directive. In
  <a name="index-France">
  </a>
  France, under the
  <a name="index-DADVSI-_0028see-also-both-DMCA-and-DRM_0029">
  </a>
  DADVSI law adopted in 2006, mere possession of a copy of
  <a name="index-DeCSS-_0028see-also-both-DMCA-and-DRM_0029">
  </a>
  DeCSS, the free program
to decrypt video on a DVD, is a crime.
 </p>
 <p>
  In 2001,
  <a name="index-Disney">
  </a>
  Disney-funded Senator
  <a name="index-Hollings_002c-Senator-Ernest">
  </a>
  Hollings proposed a bill called the
  <a name="index-Security-Systems-Standards-and-Certification-Act-_0028SSSCA_0029-_0028see-also-Consumer-Broadband-and-Digital-Television-Promotion-Act-_0028CBDTPA_0029_0029">
  </a>
  SSSCA that would require every new computer to have mandatory
copy-restriction facilities that the user cannot bypass. Following
the
  <a name="index-Clipper-chip">
  </a>
  Clipper chip and similar US government key-escrow proposals, this
shows a long-term trend: computer systems are increasingly set up to
give absentees with clout control over the people actually using the
computer system. The SSSCA was later renamed to the unpronounceable
  <a name="index-Consumer-Broadband-and-Digital-Television-Promotion-Act-_0028CBDTPA_0029-1">
  </a>
  CBDTPA, which was glossed as the “Consume But Don’t Try
Programming Act.”
 </p>
 <p>
  The Republicans took control of the US senate shortly thereafter.
They are less tied to
  <a name="index-Hollywood">
  </a>
  Hollywood than the Democrats, so they did not
press these proposals. Now that the Democrats are back in control,
the danger is once again higher.
 </p>
 <p>
  In 2001 the US began attempting to use the proposed
  <a name="index-Free-Trade-Area-of-the-Americas-_0028FTAA_0029">
  </a>
  Free Trade Area of
the Americas (FTAA) treaty to impose the same rules on all the countries in
the Western Hemisphere. The FTAA is one of the so-called free
trade treaties, which are actually designed to give business
increased power over democratic governments; imposing laws like the
DMCA is typical of this spirit. The FTAA was effectively killed by
  <a name="index-Lula-da-Silva_002c-President">
  </a>
  <a name="index-da-Silva_002c-President-Luis-Inacio-Lula">
  </a>
  Lula, President of
  <a name="index-Brazil-1">
  </a>
  Brazil, who rejected the DMCA requirement and
others.
@let@textindent=@gobble
@def@hang{@kern-@defaultparindent}@hangindent=0pt@relax
@def@thisfootno{}
@dofootnote{
  <em>
   ^1
  </em>
  This note has been updated several times since the first publication of the story.}
 </p>
 <p>
  Since then, the US has imposed similar requirements on countries such
as
  <a name="index-Australia">
  </a>
  Australia and
  <a name="index-Mexico">
  </a>
  Mexico through bilateral “free trade”
agreements, and on countries such as
  <a name="index-Costa-Rica">
  </a>
  Costa Rica through another
treaty,
  <a name="index-CAFTA">
  </a>
  CAFTA.
  <a name="index-Ecuador">
  </a>
  Ecuador’s President
  <a name="index-Correa_002c-President-Rafael">
  </a>
  Correa refused to sign a
“free trade” agreement with the US, but I’ve heard Ecuador
had adopted something like the DMCA in 2003.
 </p>
 <a name="index-Microsoft_002c-control-over-users">
 </a>
 <p>
  One of the ideas in the story was not proposed in reality until 2002.
This is the idea that the
  <a name="index-FBI">
  </a>
  FBI and Microsoft will keep the
root passwords for your personal computers, and not let you have
them.
 </p>
 <p>
  The proponents of this scheme have given it names such as
  <a name="index-_0060_0060trusted-computing_002c_0027_0027-avoid-use-of-term-_0028see-also-treacherous-computing_0029-1">
  </a>
  “trusted computing” and
  <a name="index-Palladium">
  </a>
  “Palladium.” We call
it
  <a name="index-treacherous-computing-1">
  </a>
  “treacherous
computing” because the effect is to make your computer obey
companies even to the extent of disobeying and defying you. This was
implemented in 2007 as part of
  <a name="index-Windows_002c-Vista">
  </a>
  <a name="index-Vista_002c-Windows-_0028see-also-both-Windows-and-DRM_0029">
  </a>
  Windows Vista; we expect
  <a name="index-Apple-_0028see-also-DRM_0029">
  </a>
  Apple to do something similar. In this scheme,
it is the manufacturer that keeps the secret code, but
the FBI would have little trouble getting it.
 </p>
 <p>
  What Microsoft keeps is not exactly a password in the traditional
sense; no person ever types it on a terminal. Rather, it is a
signature and encryption key that corresponds to a second key stored
in your computer. This enables Microsoft, and potentially any web
sites that cooperate with Microsoft, the ultimate control over what
the user can do on his own computer.
 </p>
 <a name="index-DRM_002c-Vista_0027s-main-purpose">
 </a>
 <p>
  Vista also gives Microsoft additional powers; for instance, Microsoft
can forcibly install upgrades, and it can order all machines running
Vista to refuse to run a certain device driver. The main purpose of
Vista’s many restrictions is to impose DRM (Digital Restrictions
Management) that users can’t overcome. The threat of DRM is why we
have established the
  <a name="index-Defective-by-Design-_0028see-also-DRM_0029-1">
  </a>
  Defective by Design campaign.
 </p>
 <a name="index-Software-Publishers-Association-_0028SPA_0029-2">
 </a>
 <p>
  When this story was first written, the SPA was threatening small
Internet service providers, demanding they permit the SPA to monitor
all users. Most
  <a name="index-ISP-_0028Internet-Service-Provider_0029">
  </a>
  ISPs surrendered when threatened, because they cannot
afford to fight back in court. One ISP,
  <a name="index-Community-ConneXion">
  </a>
  Community ConneXion in
Oakland, California, refused the demand and was actually sued. The
SPA later dropped the suit, but obtained the DMCA, which gave them the
power they sought.
  <a name="index-DMCA-_0028see-also-_0060_0060Right-to-Read_002c_0027_0027-fair-use_002c-DRM_002c-and-libraries_0029-2">
  </a>
  <a name="index-Digital-Millennium-Copyright-Act-_0028DMCA_0029-_0028see-also-DMCA_002c-_0060_0060Right-to-Read_002c_0027_0027-fair-use_002c-DRM_002c-and-libraries_0029-1">
  </a>
 </p>
 <p>
  The SPA, which actually stands for Software Publishers Association,
has been replaced in its police-like role by the
  <a name="index-Business-Software-Alliance-_0028BSA_0029-_0028see-also-Software-Publishers-Association-_0028SPA_0029_0029">
  </a>
  Business
Software Alliance. The BSA is not, today, an official police force;
unofficially, it acts like one. Using methods reminiscent of the
erstwhile
  <a name="index-Soviet-Union-2">
  </a>
  Soviet Union, it invites people to inform on their coworkers
and friends. A BSA terror campaign in
  <a name="index-Argentina">
  </a>
  Argentina in 2001 made
slightly veiled threats that people sharing software would be raped.
 </p>
 <a name="index-universities-2">
 </a>
 <p>
  The university security policies described above are not imaginary.
For example, a computer at one Chicago-area university displayed this
message upon login:
 </p>
 <blockquote class="smallquotation">
  <p>
   This system is for the use of authorized users only. Individuals using
this computer system without authority or in the excess of their authority
are subject to having all their activities on this system monitored and
recorded by system personnel. In the course of monitoring individuals
improperly using this system or in the course of system maintenance, the
activities of authorized user may also be monitored. Anyone using this
system expressly consents to such monitoring and is advised that if such
monitoring reveals possible evidence of illegal activity or violation of
University regulations system personnel may provide the evidence of such
monitoring to University authorities and/or law enforcement officials.
  </p>
 </blockquote>
 <p>
  This is an interesting approach to the
  <a name="index-Fourth-Amendment">
  </a>
  Fourth Amendment: pressure most
everyone to agree, in advance, to waive their rights under it.
 </p>
 <a name="References">
 </a>
 <h3 class="subheading">
  References
 </h3>
 <ul>
  <li>
   United States Patent and Trademark Office,
   <cite>
    Intellectual Property [
    <em>
     sic
    </em>
    ] and the National Information Infrastructure: The Report of the Working Group on Intellectual Property [
    <em>
     sic
    </em>
    ] Rights,
   </cite>
   Washington, DC: GPO, 1995.
  </li>
  <li>
   Samuelson, Pamela, “The Copyright Grab,”
   <em>
    Wired,
   </em>
   January 1996, n. 4.01.
  </li>
  <li>
   Boyle, James, “Sold Out,”
   <em>
    New York Times,
   </em>
   31 March 1996, sec. 4, p. 15.
  </li>
  <li>
   Editorial,
   <em>
    Washington Post,
   </em>
   “Public Data or Private Data,” 3 November 1996, sec. C, p. 6.
  </li>
  <li>
   Union for the Public Domain—an organization that aims to resist and reverse the overextension of copyright and patent powers.
  </li>
 </ul>
 <hr size="2"/>

