<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="The-GNU-Manifesto">
 </a>
 <h1 class="chapter">
  4. The GNU Manifesto
 </h1>
 <a name="index-GNU_002c-_0060_0060GNU-Manifesto_0027_0027">
 </a>
 <a name="index-_0060_0060GNU-Manifesto_0027_0027">
 </a>
 <a name="index-GNU_002c-GNU-Project-2">
 </a>
 <blockquote class="smallquotation">
  <p>
   The GNU Manifesto was written by Richard Stallman at the beginning of
the GNU Project, to ask for participation and support. For the first
few years, it was updated in minor ways to account for developments,
but now it seems best to leave it unchanged as most people have seen
it.
   <br/>
  </p>
  <p>
   Since that time, we have learned about certain common misunderstandings
that different wording could help avoid. Footnotes added since 1993 help
clarify these points.
   <br/>
  </p>
  <p>
   For up-to-date information about the available GNU software, please
see the information available on our web server, in particular our
list of software. For how to contribute, see
   <a href="http://gnu.org/help">
    http://gnu.org/help
   </a>
   .
  </p>
 </blockquote>
 <a name="What_0027s-GNU_003f-Gnu_0027s-Not-Unix_0021">
 </a>
 <h3 class="subheading">
  What’s GNU? Gnu’s Not Unix!
 </h3>
 <a name="index-GNU_002c-GNU-software-_0028see-also-software_0029-1">
 </a>
 <a name="index-Unix-compatibility_002c-announcement-of-1">
 </a>
 <p>
  GNU, which stands for Gnu’s Not Unix, is the name for the complete
Unix-compatible software system which I am writing so that I can give
it away free to everyone who can use it.
  <a href="#FOOT12" name="DOCF12">
   (12)
  </a>
  Several other volunteers are helping me. Contributions
of time, money, programs and equipment are greatly needed.
 </p>
 <a name="index-GNU_002c-operating-system-parts-4">
 </a>
 <p>
  So far we have an
  <a name="index-Emacs_002c-GNU-4">
  </a>
  <a name="index-GNU_002c-GNU-Emacs-4">
  </a>
  Emacs text editor with Lisp for writing editor
commands, a source level debugger, a
  <a name="index-yacc-1">
  </a>
  yacc-compatible parser generator,
a linker, and around 35 utilities. A shell (command interpreter) is
nearly completed. A new portable optimizing C compiler has compiled
itself and may be released this year. An initial kernel exists but
many more features are needed to emulate Unix. When the kernel and
compiler are finished, it will be possible to distribute a GNU system
suitable for program development. We will use
  <a name="index-TeX-1">
  </a>
  TeX as our text
formatter, but an
  <a name="index-nroff">
  </a>
  nroff is being worked on. We will use the free,
portable X window system as well. After this we will add a portable
  <a name="index-Common-Lisp">
  </a>
  <a name="index-Lisp_002c-Common">
  </a>
  <a name="index-Empire-game-1">
  </a>
  <a name="index-games_002c-Empire-1">
  </a>
  Common Lisp, an Empire game, a spreadsheet, and hundreds of other
things, plus online documentation. We hope to supply, eventually,
everything useful that normally comes with a Unix system, and more.
 </p>
 <p>
  GNU will be able to run Unix programs, but will not be identical to
Unix. We will make all improvements that are convenient, based on our
experience with other operating systems. In particular, we plan to
have longer file names, file version numbers, a crashproof file system,
file name completion perhaps, terminal-independent display support, and
perhaps eventually a Lisp-based window system through which several
Lisp programs and ordinary Unix programs can share a screen. Both C
and Lisp will be available as system programming languages. We will
try to support
  <a name="index-UUCP-1">
  </a>
  UUCP,
  <a name="index-MIT_002c-Chaosnet-2">
  </a>
  MIT Chaosnet, and Internet protocols for
communication.
 </p>
 <p>
  GNU is aimed initially at machines in the
  <a name="index-68000_002dclass-hardware-2">
  </a>
  68000/16000 class with
virtual memory, because they are the easiest machines to make it run
on. The extra effort to make it run on smaller machines will be left
to someone who wants to use it on them.
 </p>
 <p>
  To avoid horrible confusion, please pronounce the
  <em>
   g
  </em>
  in the word “GNU” when it is the name of this project.
  <a name="index-GNU_002c-GNU-software-_0028see-also-software_0029-2">
  </a>
 </p>
 <a name="Why-I-Must-Write-GNU">
 </a>
 <h3 class="subheading">
  Why I Must Write GNU
 </h3>
 <a name="index-GNU_002c-motivation-to-write">
 </a>
 <p>
  I consider that the
  <a name="index-Golden-Rule-1">
  </a>
  Golden Rule requires that if I like a program I
must share it with other people who like it. Software sellers want to
divide the users and conquer them, making each user agree not to share
with others. I refuse to break solidarity with other users in this
way. I cannot in good conscience sign a
  <a name="index-nondisclosure-agreements-4">
  </a>
  nondisclosure agreement or a
software license agreement. For years I worked within the
  <a name="index-AI-_0028Artificial-Intelligence_0029-Lab_002c-MIT-_0028see-also-MIT_0029-1">
  </a>
  Artificial
Intelligence Lab to resist such tendencies and other inhospitalities,
but eventually they had gone too far: I could not remain in an
institution where such things are done for me against my will.
 </p>
 <p>
  So that I can continue to use computers without dishonor, I have
decided to put together a sufficient body of free software so that I
will be able to get along without any software that is not free. I
have resigned from the
  <a name="index-MIT_002c-AI-_0028Artificial-Intelligence_0029-Lab-3">
  </a>
  AI Lab to deny MIT any legal excuse to prevent
me from giving GNU away.
  <a href="#FOOT13" name="DOCF13">
   (13)
  </a>
  ) for more
explanation.
 </p>
 <a name="Why-GNU-Will-Be-Compatible-with-Unix">
 </a>
 <h3 class="subheading">
  Why GNU Will Be Compatible with Unix
 </h3>
 <a name="index-Unix-compatibility_002c-reason-for-1">
 </a>
 <p>
  Unix is not my ideal system, but it is not too bad. The essential
features of Unix seem to be good ones, and I think I can fill in what
Unix lacks without spoiling them. And a system compatible with Unix
would be convenient for many other people to adopt.
 </p>
 <a name="How-GNU-Will-Be-Available">
 </a>
 <h3 class="subheading">
  How GNU Will Be Available
 </h3>
 <a name="index-public-domain-software-_0028see-also-software_0029-1">
 </a>
 <a name="index-GNU_002c-GNU-programs-_0028see-also-software_0029">
 </a>
 <a name="index-GNU_002c-GNU-software-_0028see-also-software_0029-3">
 </a>
 <p>
  GNU is not in the public domain. Everyone will be permitted to
modify and redistribute GNU, but no distributor will be allowed to
restrict its further redistribution. That is to say,
proprietary modifications will not be allowed. I want to make sure that all
versions of GNU remain free.
 </p>
 <a name="Why-Many-Other-Programmers-Want-to-Help">
 </a>
 <h3 class="subheading">
  Why Many Other Programmers Want to Help
 </h3>
 <a name="index-programmers_002c-incentive-for">
 </a>
 <a name="index-programmers_002c-psychosocial-harm-to">
 </a>
 <p>
  I have found many other programmers who are excited about GNU and
want to help.
 Many programmers are unhappy about the commercialization of system
software. It may enable them to make more money, but it requires them
to feel in conflict with other programmers in general rather than feel
as comrades. The fundamental act of friendship among programmers is the
sharing of programs; marketing arrangements now typically used
essentially forbid programmers to treat others as friends. The
purchaser of software must choose between friendship and obeying the
law. Naturally, many decide that friendship is more important. But
those who believe in law often do not feel at ease with either choice.
They become cynical and think that programming is just a way of making
money.
 </p>
 <p>
  By working on and using GNU rather than proprietary programs, we can
be hospitable to everyone and obey the law. In addition, GNU serves as
an example to inspire and a banner to rally others to join us in
sharing. This can give us a feeling of harmony which is impossible if
we use software that is not free. For about half the programmers I
talk to, this is an important happiness that money cannot replace.
 </p>
 <a name="How-You-Can-Contribute">
 </a>
 <h3 class="subheading">
  How You Can Contribute
 </h3>
 <a name="index-call-to-action_002c-contribute-to-GNU-1">
 </a>
 <a name="index-call-to-action_002c-donate">
 </a>
 <p>
  I am asking computer manufacturers for donations of machines and
money. I’m asking individuals for donations of programs and
work.
  <a href="#FOOT14" name="DOCF14">
   (14)
  </a>
 </p>
 <p>
  One consequence you can expect if you donate machines is that GNU
will run on them at an early date. The machines should be complete,
ready to use systems, approved for use in a residential area, and not
in need of sophisticated cooling or power.
 </p>
 <p>
  I have found very many programmers eager to contribute part-time
work for GNU.
  <a name="index-Unix-compatibility_002c-ease-of-contribution-because-of-1">
  </a>
  For most projects, such part-time distributed work would
be very hard to coordinate; the independently written parts would not
work together. But for the particular task of replacing Unix, this
problem is absent. A complete Unix system contains hundreds of utility
programs, each of which is documented separately. Most interface
specifications are fixed by Unix compatibility. If each contributor
can write a compatible replacement for a single Unix utility, and make
it work properly in place of the original on a Unix system, then these
utilities will work right when put together. Even allowing for Murphy
to create a few unexpected problems, assembling these components will
be a feasible task. (The kernel will require closer communication and
will be worked on by a small, tight group.)
 </p>
 <p>
  If I get donations of money, I may be able to hire a few people full
or part time. The salary won’t be high by programmers’ standards, but
I’m looking for people for whom building community spirit is as
important as making money. I view this as a way of enabling dedicated
people to devote their full energies to working on GNU by sparing them
the need to make a living in another way.
  <a name="index-call-to-action_002c-donate-1">
  </a>
  <a name="index-call-to-action_002c-contribute-to-GNU-2">
  </a>
 </p>
 <a name="Why-All-Computer-Users-Will-Benefit">
 </a>
 <h3 class="subheading">
  Why All Computer Users Will Benefit
 </h3>
 <a name="index-users_002c-benefit-to">
 </a>
 <p>
  Once GNU is written, everyone will be able to obtain good system
software free, just like air.
  <a href="#FOOT15" name="DOCF15">
   (15)
  </a>
 </p>
 <p>
  This means much more than just saving everyone the price of a Unix
license. It means that much wasteful duplication of system programming
effort will be avoided. This effort can go instead into advancing the
state of the art.
 </p>
 <p>
  Complete system sources will be available to everyone. As a result,
a user who needs changes in the system will always be free to make them
himself, or hire any available programmer or company to make them for
him. Users will no longer be at the mercy of one programmer or company
which owns the sources and is in sole position to make changes.
 </p>
 <a name="index-education_002c-free-software-in">
 </a>
 <a name="index-schools_002c-free-software-in">
 </a>
 <p>
  Schools will be able to provide a much more educational environment
by encouraging all students to study and improve the system code.
Harvard’s computer lab used to have the policy that no program could be
installed on the system if its sources were not on public display, and
upheld it by actually refusing to install certain programs. I was very
much inspired by this.
 </p>
 <p>
  Finally, the overhead of considering who owns the system software
and what one is or is not entitled to do with it will be lifted.
 </p>
 <p>
  Arrangements to make people pay for using a program, including
licensing of copies, always incur a tremendous cost to society through
the cumbersome mechanisms necessary to figure out how much (that is,
which programs) a person must pay for. And only a police state can
force everyone to obey them. Consider a space station where air must
be manufactured at great cost: charging each breather per liter of air
may be fair, but wearing the metered gas mask all day and all night is
intolerable even if everyone can afford to pay the air bill. And the
TV cameras everywhere to see if you ever take the mask off are
outrageous. It’s better to support the air plant with a head tax and
chuck the masks.
 </p>
 <p>
  Copying all or parts of a program is as natural to a programmer as
breathing, and as productive. It ought to be as free.
 </p>
 <a name="Some-Easily-Rebutted-Objections-to-GNU_0027s-Goals">
 </a>
 <h3 class="subheading">
  Some Easily Rebutted Objections to GNU’s Goals
 </h3>
 <a name="index-GNU_002c-objections-to">
 </a>
 <a name="index-GNU_002c-user-support">
 </a>
 <a name="index-users_002c-technical-support-for-GNU">
 </a>
 <p>
  &amp;bullet;
  <strong>
   “Nobody will use it if it is free, because that means they can’t rely on any support.”
  </strong>
 </p>
 <p>
  &amp;bullet;
  <strong>
   “You have to charge for the program to pay for providing the support.”
  </strong>
 </p>
 <p>
  If people would rather pay for GNU plus service than get GNU free
without service, a company to provide just service to people who have
obtained GNU free ought to be profitable.
  <a href="#FOOT16" name="DOCF16">
   (16)
  </a>
 </p>
 <p>
  We must distinguish between support in the form of real programming
work and mere handholding. The former is something one cannot rely on
from a software vendor. If your problem is not shared by enough
people, the vendor will tell you to get lost.
 </p>
 <p>
  If your business needs to be able to rely on support, the only way
is to have all the necessary sources and tools. Then you can hire any
available person to fix your problem; you are not at the mercy of any
individual. With Unix, the price of sources puts this out of
consideration for most businesses. With GNU this will be easy. It is
still possible for there to be no available competent person, but this
problem cannot be blamed on distribution arrangements. GNU does not
eliminate all the world’s problems, only some of them.
 </p>
 <p>
  Meanwhile, the users who know nothing about computers need
handholding: doing things for them which they could easily do
themselves but don’t know how.
 </p>
 <p>
  Such services could be provided by companies that sell just
handholding and repair service. If it is true that users would rather
spend money and get a product with service, they will also be willing
to buy the service having got the product free. The service companies
will compete in quality and price; users will not be tied to any
particular one. Meanwhile, those of us who don’t need the service
should be able to use the program without paying for the service.
  <br>
   <a name="index-GNU_002c-advertising-for">
   </a>
   &amp;bullet;
   <strong>
    “You cannot reach many people without advertising, and
you must charge for the program to support that.”
   </strong>
   <br>
    &amp;bullet;
    <strong>
     “It’s no use advertising a program people can get
free.”
    </strong>
   </br>
  </br>
 </p>
 <p>
  There are various forms of free or very cheap publicity that can be
used to inform numbers of computer users about something like GNU. But
it may be true that one can reach more microcomputer users with
advertising. If this is really so, a business which advertises the
service of copying and mailing GNU for a fee ought to be successful
enough to pay for its advertising and more. This way, only the users
who benefit from the advertising pay for it.
 </p>
 <p>
  On the other hand, if many people get GNU from their friends, and
such companies don’t succeed, this will show that advertising was not
really necessary to spread GNU. Why is it that free market advocates
don’t want to let the free market decide this?
  <a href="#FOOT17" name="DOCF17">
   (17)
  </a>
  <br>
   <a name="index-competition_002c-impact-on">
   </a>
   &amp;bullet;
   <strong>
    “My company needs a proprietary operating system to get
a competitive edge.”
   </strong>
  </br>
 </p>
 <p>
  GNU will remove operating system software from the realm of
competition. You will not be able to get an edge in this area, but
neither will your competitors be able to get an edge over you. You and
they will compete in other areas, while benefiting mutually in this
one. If your business is selling an operating system, you will not
like GNU, but that’s tough on you. If your business is something else,
GNU can save you from being pushed into the expensive business of
selling operating systems.
 </p>
 <p>
  I would like to see GNU development supported by gifts from many
manufacturers and users, reducing the cost to each.
  <a href="#FOOT18" name="DOCF18">
   (18)
  </a>
  <br>
   <a name="index-programmers_002c-income-for-2">
   </a>
   <a name="index-programmers_002c-and-creativity-and-entitlement">
   </a>
   &amp;bullet;
   <strong>
    “Don’t programmers deserve a reward for their
creativity?”
   </strong>
  </br>
 </p>
 <p>
  If anything deserves a reward, it is social contribution.
Creativity can be a social contribution, but only in so far as society
is free to use the results. If programmers deserve to be rewarded for
creating innovative programs, by the same token they deserve to be
punished if they restrict the use of these programs.
  <br>
   &amp;bullet;
   <strong>
    “Shouldn’t a programmer be able to ask for a reward for
his creativity?”
   </strong>
  </br>
 </p>
 <a name="index-programmers_002c-income-for-3">
 </a>
 <p>
  There is nothing wrong with wanting pay for work, or seeking to
maximize one’s income, as long as one does not use means that are
destructive. But the means customary in the field of software today
are based on destruction.
 </p>
 <p>
  Extracting money from users of a program by restricting their use of
it is destructive because the restrictions reduce the amount and the
ways that the program can be used. This reduces the amount of wealth
that humanity derives from the program. When there is a deliberate
choice to restrict, the harmful consequences are deliberate destruction.
 </p>
 <a name="index-citizen-values_002c-Golden-Rule-1">
 </a>
 <p>
  The reason a good citizen does not use such destructive means to
become wealthier is that, if everyone did so, we would all become
poorer from the mutual destructiveness. This is
  <a name="index-Kantian-ethics">
  </a>
  Kantian ethics; or,
  <a name="index-Golden-Rule-2">
  </a>
  the Golden Rule. Since I do not like the consequences that result if
everyone hoards information, I am required to consider it wrong for one
to do so. Specifically, the desire to be rewarded for one’s creativity
does not justify depriving the world in general of all or part of that
creativity.
  <br>
   &amp;bullet;
   <strong>
    “Won’t programmers starve?”
   </strong>
  </br>
 </p>
 <p>
  I could answer that nobody is forced to be a programmer. Most of us
cannot manage to get any money for standing on the street and making
faces. But we are not, as a result, condemned to spend our lives
standing on the street making faces, and starving. We do something
else.
 </p>
 <p>
  But that is the wrong answer because it accepts the questioner’s
implicit assumption: that without ownership of software, programmers
cannot possibly be paid a cent. Supposedly it is all or nothing.
 </p>
 <p>
  The real reason programmers will not starve is that it will still be
possible for them to get paid for programming; just not paid as much as
now.
 </p>
 <p>
  Restricting copying is not the only basis for business in software.
It is the most common basis
  <a href="#FOOT19" name="DOCF19">
   (19)
  </a>
  because it
brings in the most money. If it were prohibited, or rejected by the
customer, software business would move to other bases of organization
which are now used less often. There are always numerous ways to
organize any kind of business.
 </p>
 <p>
  Probably programming will not be as lucrative on the new basis as it
is now. But that is not an argument against the change. It is not
considered an injustice that sales clerks make the salaries that they
now do. If programmers made the same, that would not be an injustice
either. (In practice they would still make considerably more than
that.)
  <a name="index-programmers_002c-income-for-4">
  </a>
  <br/>
 </p>
 <p>
  &amp;bullet;
  <strong>
   “Don’t people have a right to control how their creativity is used?”
  </strong>
 </p>
 <a name="index-patents-1">
 </a>
 <a name="index-_0060_0060intellectual-property_002c_0027_0027-bias-and-fallacy-of-term-_0028see-also-ownership_0029">
 </a>
 <p>
  “Control over the use of one’s ideas” really constitutes
control over other people’s lives; and it is usually used to make
their lives more difficult.
 </p>
 <p>
  People who have studied the issue of intellectual property
rights
  <a href="#FOOT20" name="DOCF20">
   (20)
  </a>
  ) for further explanation of how this
term spreads confusion and bias.
 carefully (such as lawyers) say that
there is no intrinsic right to intellectual property. The kinds of
supposed intellectual property rights that the government recognizes
were created by specific acts of legislation for specific purposes.
 </p>
 <p>
  For example, the patent system was established to encourage
inventors to disclose the details of their inventions. Its purpose was
to help society rather than to help inventors. At the time, the life
span of 17 years for a patent was short compared with the rate of
advance of the state of the art. Since patents are an issue only among
manufacturers, for whom the cost and effort of a license agreement are
small compared with setting up production, the patents often do not do
much harm. They do not obstruct most individuals who use patented
products.
 </p>
 <p>
  The idea of copyright did not exist in ancient times, when authors
frequently copied other authors at length in works of nonfiction. This
practice was useful, and is the only way many authors’ works have
survived even in part. The copyright system was created expressly for
the purpose of encouraging authorship. In the domain for which it was
invented—books, which could be copied economically only on a printing
press—it did little harm, and did not obstruct most of the individuals
who read the books.
 </p>
 <p>
  All intellectual property rights are just licenses granted by society
because it was thought, rightly or wrongly, that society as a whole
would benefit by granting them. But in any particular situation, we
have to ask: are we really better off granting such license? What kind
of act are we licensing a person to do?
  <a name="index-_0060_0060intellectual-property_002c_0027_0027-bias-and-fallacy-of-term-_0028see-also-ownership_0029-1">
  </a>
 </p>
 <p>
  The case of programs today is very different from that of books a
hundred years ago. The fact that the easiest way to copy a program is
from one neighbor to another, the fact that a program has both source
code and object code which are distinct, and the fact that a program is
used rather than read and enjoyed, combine to create a situation in
which a person who enforces a copyright is harming society as a whole
both materially and spiritually; in which a person should not do so
regardless of whether the law enables him to.
  <a name="index-programmers_002c-and-creativity-and-entitlement-1">
  </a>
  <br>
   <a name="index-competition_002c-impact-on-1">
   </a>
   &amp;bullet;
   <strong>
    “Competition makes things get done
better.”
   </strong>
  </br>
 </p>
 <p>
  The paradigm of competition is a race: by rewarding the winner, we
encourage everyone to run faster. When capitalism really works this
way, it does a good job; but its defenders are wrong in assuming it
always works this way. If the runners forget why the reward is offered
and become intent on winning, no matter how, they may find other
strategies—such as, attacking other runners. If the runners get into
a fist fight, they will all finish late.
 </p>
 <p>
  Proprietary and secret software is the moral equivalent of runners
in a fist fight. Sad to say, the only referee we’ve got does not seem
to object to fights; he just regulates them (“For every ten
yards you run, you can fire one shot”). He really ought to
break them up, and penalize runners for even trying to fight.
  <br>
   <a name="index-programmers_002c-incentive-for-1">
   </a>
   &amp;bullet;
   <strong>
    “Won’t everyone stop programming without a monetary incentive?”
   </strong>
  </br>
 </p>
 <p>
  Actually, many people will program with absolutely no monetary
incentive. Programming has an irresistible fascination for some
people, usually the people who are best at it. There is no shortage of
professional musicians who keep at it even though they have no hope of
making a living that way.
 </p>
 <p>
  But really this question, though commonly asked, is not appropriate
to the situation. Pay for programmers will not disappear, only become
less. So the right question is, will anyone program with a reduced
monetary incentive? My experience shows that they will.
 </p>
 <a name="index-AI-_0028Artificial-Intelligence_0029-Lab_002c-MIT-_0028see-also-MIT_0029-2">
 </a>
 <p>
  For more than ten years, many of the world’s best programmers worked
at the Artificial Intelligence Lab for far less money than they could
have had anywhere else. They got many kinds of nonmonetary rewards:
fame and appreciation, for example. And creativity is also fun, a
reward in itself.
 </p>
 <p>
  Then most of them left when offered a chance to do the same
interesting work for a lot of money.
 </p>
 <p>
  What the facts show is that people will program for reasons other
than riches; but if given a chance to make a lot of money as well, they
will come to expect and demand it. Low-paying organizations do poorly
in competition with high-paying ones, but they do not have to do badly
if the high-paying ones are banned.
  <a name="index-programmers_002c-incentive-for-2">
  </a>
  <br>
   &amp;bullet;
   <strong>
    “We need the programmers desperately. If they demand that we stop helping our neighbors, we have to obey.”
   </strong>
  </br>
 </p>
 <p>
  You’re never so desperate that you have to obey this sort of demand.
Remember: millions for defense, but not a cent for tribute!
  <br/>
 </p>
 <a name="index-programmers_002c-income-for-5">
 </a>
 <a name="index-development_002c-funding-for-2">
 </a>
 <p>
  &amp;bullet;
  <strong>
   “Programmers need to make a living somehow.”
  </strong>
 </p>
 <p>
  In the short run, this is true. However, there are plenty of ways
that programmers could make a living without selling the right to use a
program. This way is customary now because it brings programmers and
businessmen the most money, not because it is the only way to make a
living. It is easy to find other ways if you want to find them. Here
are a number of examples.
 </p>
 <p>
  A manufacturer introducing a new computer will pay for the porting of
operating systems onto the new hardware.
 </p>
 <p>
  The sale of teaching, handholding and maintenance services could
also employ programmers.
 </p>
 <a name="index-freeware-_0028see-also-software_0029">
 </a>
 <p>
  People with new ideas could distribute programs as
freeware,
  <a href="#FOOT21" name="DOCF21">
   (21)
  </a>
  ) for more explanation.
 asking for donations from satisfied
users, or selling handholding services. I have met people who are
already working this way successfully.
 </p>
 <p>
  Users with related needs can form users’ groups, and pay dues. A
group would contract with programming companies to write programs that
the group’s members would like to use.
 </p>
 <a name="index-software_002c-software-tax">
 </a>
 <p>
  All sorts of development can be funded with a Software Tax:
 </p>
 <p>
  Suppose everyone who buys a computer has to pay
  <em>
   x
  </em>
  percent of the
 price as a software tax. The government gives this to an agency
 like the
  <a name="index-National-Science-Foundation-_0028NSF_0029">
  </a>
  NSF to spend on software development.
 </p>
 <p>
  But if the computer buyer makes a donation to software development
 himself, he can take a credit against the tax. He can donate to
 the project of his own choosing—often, chosen because he hopes to
 use the results when it is done. He can take a credit for any
 amount of donation up to the total tax he had to pay.
 </p>
 <p>
  The total tax rate could be decided by a vote of the payers of the
 tax, weighted according to the amount they will be taxed on.
 </p>
 <p>
  The consequences:
 </p>
 <ul>
  <li>
   The computer-using community supports software development.
  </li>
  <li>
   This community decides what level of support is needed.
  </li>
  <li>
   Users who care which projects their share is spent on can choose this for themselves.
  </li>
 </ul>
 <p>
  In the long run, making programs free is a step toward the
postscarcity world, where nobody will have to work very hard just to
make a living. People will be free to devote themselves to activities
that are fun, such as programming, after spending the necessary ten
hours a week on required tasks such as legislation, family counseling,
robot repair and asteroid prospecting. There will be no need to be
able to make a living from programming.
 </p>
 <a name="index-users_002c-benefit-to-1">
 </a>
 <p>
  We have already greatly reduced the amount of work that the whole
society must do for its actual productivity, but only a little of this
has translated itself into leisure for workers because much
nonproductive activity is required to accompany productive activity.
The main causes of this are bureaucracy and isometric struggles against
competition. Free software will greatly reduce these drains in the
area of software production. We must do this, in order for technical
gains in productivity to translate into less work for us.
  <a name="index-GNU_002c-objections-to-1">
  </a>
  <a name="index-programmers_002c-income-for-6">
  </a>
  <a name="index-development_002c-funding-for-3">
  </a>
  <a name="index-_0060_0060GNU-Manifesto_0027_0027-1">
  </a>
  <a name="index-GNU_002c-_0060_0060GNU-Manifesto_0027_0027-1">
  </a>
 </p>
 <div class="footnote">
  <hr>
   <h3>
    Footnotes
   </h3>
   <h3>
    <a href="#DOCF12" name="FOOT12">
     (12)
    </a>
   </h3>
   <p>
    The wording here was
careless. The intention was that nobody would have to pay for
    <em>
     permission
    </em>
    to use the GNU system. But the words don’t make this
clear, and people often interpret them as saying that copies of GNU
should always be distributed at little or no charge. That was never
the intent; later on, the manifesto mentions the possibility of
companies providing the service of distribution for a
profit. Subsequently I have learned to distinguish carefully between
“free” in the sense of freedom and “free” in the sense of
price. Free software is software that users have the freedom to
distribute and change. Some users may obtain copies at no charge,
while others pay to obtain copies—and if the funds help support
improving the software, so much the better. The important thing is
that everyone who has a copy has the freedom to cooperate with others
in using it.
   </p>
   <h3>
    <a href="#DOCF13" name="FOOT13">
     (13)
    </a>
   </h3>
   <p>
    The expression
    <a name="index-_0060_0060give-away-software_002c_0027_0027-misleading-use-of-term">
    </a>
    “give away” is another indication that I had not yet clearly
separated the issue of price from that of freedom. We now recommend
avoiding this expression when talking about free software. See “Words
to Avoid (or Use with Care)”
   </p>
   <h3>
    <a href="#DOCF14" name="FOOT14">
     (14)
    </a>
   </h3>
   <p>
    Nowadays, for software tasks to work on, see the
    <a name="index-High-Priority-Projects-list">
    </a>
    High Priority Projects list, at
    <a href="http://fsf.org/campaigns/priority-projects/">
     http://fsf.org/campaigns/priority-projects/
    </a>
    , and the
    <a name="index-GNU-Help-Wanted-list">
    </a>
    GNU
Help Wanted list, the general task list for GNU software packages, at
    <a href="http://savannah.gnu.org/people/?type_id=1">
     http://savannah.gnu.org/people/?type_id=1
    </a>
    . For other ways to
help, see
    <a href="http://gnu.org/help/help.html">
     http://gnu.org/help/help.html
    </a>
    .
   </p>
   <h3>
    <a href="#DOCF15" name="FOOT15">
     (15)
    </a>
   </h3>
   <p>
    This is another place I failed
to distinguish carefully between the two different meanings of
“free.” The statement as it stands is not false—you can get copies
of GNU software at no charge, from your friends or over the net. But
it does suggest the wrong idea.
   </p>
   <h3>
    <a href="#DOCF16" name="FOOT16">
     (16)
    </a>
   </h3>
   <p>
    Several such companies now exist.
   </p>
   <h3>
    <a href="#DOCF17" name="FOOT17">
     (17)
    </a>
   </h3>
   <p>
    Although it is
a charity rather than a company, the
    <a name="index-FSF_002c-fundraising-1">
    </a>
    <a name="index-FSF_002c-how-you-can-help">
    </a>
    Free Software Foundation for 10
years raised most of its funds from its distribution service. You can
order things from the FSF to support its work.
   </p>
   <h3>
    <a href="#DOCF18" name="FOOT18">
     (18)
    </a>
   </h3>
   <p>
    A group
of computer companies pooled funds around 1991 to support maintenance
of the
    <a name="index-GNU_002c-GNU-C-compiler-_0028see-also-GNU_002c-GCC_0029-1">
    </a>
    GNU C Compiler.
   </p>
   <h3>
    <a href="#DOCF19" name="FOOT19">
     (19)
    </a>
   </h3>
   <p>
    I think I was mistaken in saying
that proprietary software was the most common basis for making money
in software. It seems that actually the most common business model was
and is development of custom software. That does not offer the
possibility of collecting rents, so the business has to keep doing
real work in order to keep getting income. The custom software
business would continue to exist, more or less unchanged, in a free
software world. Therefore, I no longer expect that most paid
programmers would earn less in a free software world.
   </p>
   <h3>
    <a href="#DOCF20" name="FOOT20">
     (20)
    </a>
   </h3>
   <p>
    In the 1980s I had not yet realized how confusing it
was to speak of “the issue” of “intellectual property.” That term
is obviously biased; more subtle is the fact that it lumps together
various disparate laws which raise very different issues. Nowadays I
urge people to reject the term “intellectual property” entirely,
lest it lead others to suppose that those laws form one coherent
issue. The way to be clear is to discuss patents, copyrights, and
    <a name="index-trademarks-and_002for-trademark-law">
    </a>
    trademarks separately. See “Did You Say ‘Intellectual Property’? It’s
a Seductive Mirage”.
   </p>
   <h3>
    <a href="#DOCF21" name="FOOT21">
     (21)
    </a>
   </h3>
   <p>
    Subsequently we learned to distinguish between
“free software” and “freeware.” The term “freeware” means
software you are free to redistribute, but usually you are not free to
study and change the source code, so most of it is not free
software. See “Words to Avoid (or Use with Care)”.
   </p>
  </hr>
 </div>
 <hr size="2"/>

