<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Why-Software-Should-Not-Have-Owners">
 </a>
 <h1 class="chapter">
  5. Why Software Should Not Have Owners
 </h1>
 <a name="index-competition_002c-impact-on-2">
 </a>
 <a name="index-copyright_002c-digital-technology-and">
 </a>
 <p>
  Digital information technology contributes to the world by making it
easier to copy and modify information. Computers promise to make this
easier for all of us.
 </p>
 <p>
  Not everyone wants it to be easier. The system of copyright gives
software programs “owners,” most of whom aim to withhold
software’s potential benefit from the rest of the public. They would
like to be the only ones who can copy and modify the software that we
use.
 </p>
 <p>
  The copyright system grew up with printing—a technology for
mass-production copying. Copyright fit in well with this technology
because it restricted only the mass producers of copies. It did not
take freedom away from readers of books. An ordinary reader, who did
not own a printing press, could copy books only with pen and ink, and
few readers were sued for that.
 </p>
 <a name="index-ownership_002c-and-Soviet_002dstyle-information-control">
 </a>
 <p>
  Digital technology is more flexible than the printing press: when
information has digital form, you can easily copy it to share it with
others. This very flexibility makes a bad fit with a system like
copyright. That’s the reason for the increasingly nasty and draconian
measures now used to enforce software copyright. Consider these four
practices of the
  <a name="index-Software-Publishers-Association-_0028SPA_0029">
  </a>
  Software Publishers Association (SPA):
 </p>
 <a name="index-copyright_002c-enforcement-measures">
 </a>
 <ul>
  <li>
   Massive propaganda saying it is wrong to disobey the owners
to help your friend.
  </li>
  <li>
   Solicitation for stool pigeons to inform on their coworkers and
colleagues.
  </li>
  <li>
   Raids (with police help) on offices and schools, in which people are
told they must prove they are innocent of illegal copying.
  </li>
  <li>
   Prosecution (by the US government, at the
   <a name="index-Software-Publishers-Association-_0028SPA_0029-1">
   </a>
   SPA’s request)
of people such as MIT’s
   <a name="index-LaMacchia_002c-David">
   </a>
   David LaMacchia, not for copying software (he is not accused of
copying any), but merely for leaving copying facilities unguarded and
failing to censor their use.
   <a href="#FOOT22" name="DOCF22">
    (22)
   </a>
  </li>
 </ul>
 <p>
  All four practices resemble those used in the former
  <a name="index-Soviet-Union">
  </a>
  Soviet Union,
where every copying machine had a guard to prevent forbidden copying,
and where individuals had to copy information secretly and pass it
from hand to hand as samizdat. There is of course a
difference: the motive for information control in the Soviet Union was
political; in the US the motive is profit. But it is the actions that
affect us, not the motive. Any attempt to block the sharing of
information, no matter why, leads to the same methods and the same
harshness.
 </p>
 <p>
  Owners make several kinds of arguments for giving them the power
to control how we use information:
  <a name="index-ownership_002c-and-Soviet_002dstyle-information-control-1">
  </a>
 </p>
 <a name="Name-Calling">
 </a>
 <h3 class="subheading">
  Name Calling
 </h3>
 <a name="index-ownership_002c-arguments-for">
 </a>
 <a name="index-users_002c-arguments-used-to-justify-control-over">
 </a>
 <a name="index-terminology_002c-importance-of-using-correct">
 </a>
 <a name="index-_0060_0060piracy_002c_0027_0027-erroneous-use-of-term-2">
 </a>
 <a name="index-_0060_0060intellectual-property_002c_0027_0027-bias-and-fallacy-of-term-_0028see-also-ownership_0029-2">
 </a>
 <a name="index-_0060_0060theft_002c_0027_0027-erroneous-use-of-term">
 </a>
 <a name="index-_0060_0060damage_002c_0027_0027-erroneous-use-of-term">
 </a>
 <p>
  Owners use smear words such as “piracy” and
“theft,” as well as expert terminology such as
“intellectual property” and “damage,” to
suggest a certain line of thinking to the public—a simplistic
analogy between programs and physical objects.
 </p>
 <p>
  Our ideas and intuitions about property for material objects are about
whether it is right to
  <em>
   take an object away
  </em>
  from someone else. They
don’t directly apply to
  <em>
   making a copy
  </em>
  of something. But the owners
ask us to apply them anyway.
 </p>
 <a name="Exaggeration">
 </a>
 <h3 class="subheading">
  Exaggeration
 </h3>
 <p>
  Owners say that they suffer “harm” or “economic
loss” when users copy programs themselves. But the copying has
no direct effect on the owner, and it harms no one. The owner can
lose only if the person who made the copy would otherwise have paid
for one from the owner.
 </p>
 <p>
  A little thought shows that most such people would not have bought
copies. Yet the owners compute their “losses” as if each
and every one would have bought a copy. That is exaggeration—to
put it kindly.
 </p>
 <a name="The-Law">
 </a>
 <h3 class="subheading">
  The Law
 </h3>
 <p>
  Owners often describe the current state of the law, and the harsh
penalties they can threaten us with. Implicit in this approach is the
suggestion that today’s law reflects an unquestionable view of
morality—yet at the same time, we are urged to regard these
penalties as facts of nature that can’t be blamed on anyone.
 </p>
 <p>
  This line of persuasion isn’t designed to stand up to critical
thinking; it’s intended to reinforce a habitual mental pathway.
 </p>
 <p>
  It’s elementary that laws don’t decide right and wrong. Every American
should know that, in the 1950s, it was against the law in many
states for a black person to sit in the front of a bus; but only
racists would say sitting there was wrong.
 </p>
 <a name="Natural-Rights">
 </a>
 <h3 class="subheading">
  Natural Rights
 </h3>
 <a name="index-programmers_002c-and-creativity-and-entitlement-2">
 </a>
 <a name="index-users_002c-premise-of-author-supremacy-_0028see-also-ownership_0029">
 </a>
 <p>
  Authors often claim a special connection with programs they have
written, and go on to assert that, as a result, their desires and
interests concerning the program simply outweigh those of anyone
else—or even those of the whole rest of the world. (Typically
companies, not authors, hold the copyrights on software, but we are
expected to ignore this discrepancy.)
 </p>
 <p>
  To those who propose this as an ethical axiom—the author is more
important than you—I can only say that I, a notable software
author myself, call it bunk.
 </p>
 <p>
  But people in general are only likely to feel any sympathy with the
natural rights claims for two reasons.
 </p>
 <a name="index-software_002c-overstretched-analogy-with-material-objects">
 </a>
 <p>
  One reason is an overstretched analogy with material objects. When I
cook spaghetti, I do object if someone else eats it, because then I
cannot eat it. His action hurts me exactly as much as it benefits
him; only one of us can eat the spaghetti, so the question is, which one?
The smallest distinction between us is enough to tip the ethical
balance.
 </p>
 <p>
  But whether you run or change a program I wrote affects you directly
and me only indirectly. Whether you give a copy to your friend
affects you and your friend much more than it affects me. I shouldn’t
have the power to tell you not to do these things. No one should.
 </p>
 <p>
  The second reason is that people have been told that natural rights
for authors is the accepted and unquestioned tradition of our society.
 </p>
 <p>
  As a matter of history, the opposite is true. The idea of natural
rights of authors was proposed and decisively rejected when the
  <a name="index-Constitution_002c-authors_0027-natural-rights-and-US">
  </a>
  US
Constitution was drawn up. That’s why the Constitution only
  <em>
   permits
  </em>
  a system of copyright and does not
  <em>
   require
  </em>
  one; that’s why it says that copyright must be temporary. It also
states that the purpose of copyright is to promote progress—not
to reward authors. Copyright does reward authors somewhat, and
publishers more, but that is intended as a means of modifying their
behavior.
 </p>
 <p>
  The real established tradition of our society is that copyright cuts
into the natural rights of the public—and that this can only be
justified for the public’s sake.
  <a name="index-programmers_002c-and-creativity-and-entitlement-3">
  </a>
  <a name="index-users_002c-premise-of-author-supremacy-_0028see-also-ownership_0029-1">
  </a>
 </p>
 <a name="Economics">
 </a>
 <h3 class="subheading">
  Economics
 </h3>
 <p>
  The final argument made for having owners of software is that this
leads to production of more software.
 </p>
 <p>
  Unlike the others, this argument at least takes a legitimate approach
to the subject. It is based on a valid goal—satisfying the
users of software. And it is empirically clear that people will
produce more of something if they are well paid for doing so.
 </p>
 <p>
  But the economic argument has a flaw: it is based on the assumption
that the difference is only a matter of how much money we have to pay.
It assumes that
  <em>
   production of software
  </em>
  is what we want,
whether the software has owners or not.
 </p>
 <a name="index-software_002c-overstretched-analogy-with-material-objects-1">
 </a>
 <p>
  People readily accept this assumption because it accords with our
experiences with material objects. Consider a sandwich, for instance.
You might well be able to get an equivalent sandwich either gratis or
for a price. If so, the amount you pay is the only difference.
Whether or not you have to buy it, the sandwich has the same taste,
the same nutritional value, and in either case you can only eat it
once. Whether you get the sandwich from an owner or not cannot
directly affect anything but the amount of money you have afterwards.
 </p>
 <p>
  This is true for any kind of material object—whether or not it
has an owner does not directly affect what it
  <em>
   is,
  </em>
  or what you
can do with it if you acquire it.
 </p>
 <p>
  But if a program has an owner, this very much affects what it is, and
what you can do with a copy if you buy one. The difference is not
just a matter of money. The system of owners of software encourages
software owners to produce something—but not what society really
needs. And it causes intangible ethical pollution that affects us
all.
  <a name="index-users_002c-arguments-used-to-justify-control-over-1">
  </a>
  <a name="index-ownership_002c-arguments-for-1">
  </a>
 </p>
 <br>
  <p>
   What does society need? It needs information that is truly available
to its citizens—for example, programs that people can read, fix,
adapt, and improve, not just operate. But what software owners
typically deliver is a black box that we can’t study or change.
  </p>
  <p>
   Society also needs freedom. When a program has an owner, the users
lose freedom to control part of their own lives.
  </p>
  <a name="index-citizen-values_002c-cooperation">
  </a>
  <a name="index-_0060_0060piracy_002c_0027_0027-erroneous-use-of-term-3">
  </a>
  <p>
   And, above all, society needs to encourage the spirit of voluntary
cooperation in its citizens. When software owners tell us that
helping our neighbors in a natural way is “piracy,” they
pollute our society’s civic spirit.
  </p>
  <p>
   This is why we say that free software is a matter of freedom, not price.
  </p>
  <p>
   The economic argument for owners is erroneous, but the economic issue
is real. Some people write useful software for the pleasure of
writing it or for admiration and love; but if we want more software
than those people write, we need to raise funds.
  </p>
  <a name="index-developers_002c-funding-for">
  </a>
  <a name="index-programmers_002c-income-for-7">
  </a>
  <p>
   Since the 1980s, free software developers have tried various methods
of finding funds, with some success. There’s no need to make anyone
rich; a typical income is plenty of incentive to do many jobs that are
less satisfying than programming.
  </p>
  <a name="index-Stallman_002c-Richard-2">
  </a>
  <p>
   For years, until a fellowship made it unnecessary, I made a living
from custom enhancements of the free software I had written. Each
enhancement was added to the standard released version and thus
eventually became available to the general public. Clients paid me so
that I would work on the enhancements they wanted, rather than on the
features I would otherwise have considered highest priority.
  </p>
  <p>
   Some free software developers make money by selling support services.
In 1994,
   <a name="index-Cygnus-Support">
   </a>
   Cygnus Support, with around 50 employees, estimated that
about 15 percent of its staff activity was free software
development—a respectable percentage for a software company.
  </p>
  <p>
   In the early 1990s, companies including
   <a name="index-Intel-_0028see-also-_0060_0060trusted-computing_0027_0027_0029">
   </a>
   Intel,
   <a name="index-Motorola-1">
   </a>
   Motorola,
   <a name="index-Texas-Instruments">
   </a>
   <a name="index-Analog-Devices">
   </a>
   Analog Devices
Texas Instruments and Analog Devices combined to fund the continued
development of the
   <a name="index-GNU_002c-GNU-C-compiler-_0028see-also-GNU_002c-GCC_0029-2">
   </a>
   <a name="index-GNU_002c-GCC-2">
   </a>
   GNU C compiler. Most GCC development is still done
by paid developers. The
   <a name="index-GNU_002c-GNU-compiler">
   </a>
   GNU compiler for the
   <a name="index-Ada-language">
   </a>
   Ada language was funded
in the 90s by the
   <a name="index-Air-Force_002c-US">
   </a>
   US Air Force, and continued since then by a company
formed specifically for the purpose.
  </p>
  <p>
   The free software movement is still small, but the example of
listener-supported radio in the US shows it’s possible to support a
large activity without forcing each user to pay.
  </p>
  <a name="index-citizen-values_002c-cooperation-1">
  </a>
  <p>
   As a computer user today, you may find yourself using a
proprietary program. If your friend asks to make a copy, it would be wrong to
refuse. Cooperation is more important than copyright. But
underground, closet cooperation does not make for a good society. A
person should aspire to live an upright life openly with pride, and
this means saying no to proprietary software.
  </p>
  <p>
   You deserve to be able to cooperate openly and freely with other
people who use software. You deserve to be able to learn how the
software works, and to teach your students with it. You deserve to be
able to hire your favorite programmer to fix it when it breaks.
  </p>
  <p>
   You deserve free software.
  </p>
  <div class="footnote">
   <hr>
    <h3>
     Footnotes
    </h3>
    <h3>
     <a href="#DOCF22" name="FOOT22">
      (22)
     </a>
    </h3>
    <p>
     The charges were subsequently
dismissed.
    </p>
   </hr>
  </div>
  <hr size="2"/>
 </br>

