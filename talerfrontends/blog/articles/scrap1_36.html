<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="The-X-Window-System-Trap">
 </a>
 <h1 class="chapter">
  36. The X Window System Trap
 </h1>
 <a name="index-traps_002c-X-Window-System">
 </a>
 <a name="index-X-Window-System-5">
 </a>
 <a name="index-developers_002c-to-copyleft-or-not-to-copyleft_003f">
 </a>
 <p>
  To copyleft or not to copyleft? That is one of the major
controversies in the free software community. The idea of copyleft is
that we should fight fire with fire—that we should use copyright
to make sure our code stays free. The GNU General Public License (GNU
GPL) is one example of a copyleft license.
 </p>
 <p>
  Some free software developers prefer noncopyleft distribution.
Noncopyleft licenses such as the
  <a name="index-XFree86-license">
  </a>
  XFree86 and
  <a name="index-BSD-licenses-_0028see-also-both-_0060_0060BSD_002dstyle_0027_0027-and-GPL_0029-2">
  </a>
  BSD licenses are based on the idea
of never saying no to anyone—not even to someone who seeks to
use your work as the basis for restricting other people. Noncopyleft
licensing does nothing wrong, but it misses the opportunity to
actively protect our freedom to change and redistribute software. For
that, we need copyleft.
 </p>
 <a name="index-copylefted-software-_0028see-also-software_0029-3">
 </a>
 <a name="index-copyleft_002c-X-Consortium-opposition-to">
 </a>
 <a name="index-X-Consortium-_0028see-also-Open-Group_002c-its-successor_0029-3">
 </a>
 <p>
  For many years, the X Consortium was the chief opponent of copyleft.
It exerted both moral suasion and pressure to discourage free software
developers from copylefting their programs. It used moral suasion by
suggesting that it is not nice to say no. It used pressure through
its rule that copylefted software could not be in the X Distribution.
 </p>
 <p>
  Why did the X Consortium adopt this policy? It had to do with their
conception of success. The X Consortium defined success as
popularity—specifically, getting computer companies to use the X
Window System. This definition put the computer companies in the
driver’s seat: whatever they wanted, the X Consortium had to help
them get it.
 </p>
 <p>
  Computer companies normally distribute proprietary software. They
wanted free software developers to donate their work for such use. If
they had asked for this directly, people would have laughed. But the
X Consortium, fronting for them, could present this request as an
unselfish one. “Join us in donating our work to proprietary software
developers,” they said, suggesting that this is a noble form of
self-sacrifice. “Join us in achieving popularity,” they said,
suggesting that it was not even a sacrifice.
 </p>
 <p>
  But self-sacrifice is not the issue: tossing away the defense that
copyleft provides, which protects the freedom of the whole community,
is sacrificing more than yourself. Those who granted the X
Consortium’s request entrusted the community’s future to the goodwill
of the X Consortium.
 </p>
 <a name="index-X11R6_002e4-1">
 </a>
 <p>
  This trust was misplaced. In its last year, the X Consortium made a
plan to restrict the forthcoming X11R6.4 release so that it would not
be free software. They decided to start saying no, not only to
proprietary software developers, but to our community as well.
 </p>
 <p>
  There is an irony here. If you said yes when the X Consortium asked
you not to use copyleft, you put the X Consortium in a position to
license and restrict its version of your program, along with the
code for the core of X.
 </p>
 <p>
  The X Consortium did not carry out this plan. Instead it closed down
and transferred X development to the Open Group, whose staff are now
carrying out a similar plan. To give them credit, when I asked them
to release X11R6.4 under the GNU GPL in parallel with their planned
restrictive license, they were willing to consider the idea. (They
were firmly against staying with the old
  <a name="index-X11-licenses-3">
  </a>
  X11 distribution terms.)
Before they said yes or no to this proposal, it had already failed for
another reason: the
  <a name="index-XFree86-1">
  </a>
  XFree86 group followed the X Consortium’s old
policy, and will not accept copylefted software.
  <a name="index-copylefted-software-_0028see-also-software_0029-4">
  </a>
 </p>
 <p>
  In September 1998, several months after X11R6.4 was released with
nonfree distribution terms, the Open Group reversed its decision and
rereleased it under the same noncopyleft free software license that
was used for X11R6.3. Thus, the Open Group therefore eventually did
what was right, but that does not alter the general issue.
  <a name="index-X11R6_002e4-2">
  </a>
 </p>
 <p>
  Even if the X Consortium and the Open Group had never planned to
restrict X, someone else could have done it. Noncopylefted software
is vulnerable from all directions; it lets anyone make a nonfree
version dominant, if he will invest sufficient resources to add
significantly important features using proprietary code. Users who
choose software based on technical characteristics, rather than on
freedom, could easily be lured to the nonfree version for short-term
convenience.
 </p>
 <p>
  The X Consortium and Open Group can no longer exert moral suasion by
saying that it is wrong to say no. This will make it easier to decide
to copyleft your X-related software.
  <a name="index-X-Consortium-_0028see-also-Open-Group_002c-its-successor_0029-4">
  </a>
  <a name="index-copyleft_002c-X-Consortium-opposition-to-1">
  </a>
 </p>
 <p>
  When you work on the core of X, on programs such as the X server,
Xlib, and Xt, there is a practical reason not to use copyleft. The
  <a name="index-X_002eorg">
  </a>
  X.org group does an important job for the community in maintaining
these programs, and the benefit of copylefting our changes would be
less than the harm done by a fork in development. So it is better to
work with them, and not copyleft our changes on these programs.
Likewise for utilities such as
  <a name="index-xset">
  </a>
  <code>
   xset
  </code>
  and
  <a name="index-xrdb">
  </a>
  <code>
   xrdb
  </code>
  , which are close to the
core of X and do not need major improvements. At least we know that
the X.org group has a firm commitment to developing these programs as
free software.
 </p>
 <p>
  The issue is different for programs outside the core of X:
applications, window managers, and additional libraries and widgets.
There is no reason not to copyleft them, and we should copyleft them.
 </p>
 <p>
  In case anyone feels the pressure exerted by the criteria for
inclusion in the X distributions, the
  <a name="index-GNU_002c-GNU-Project-9">
  </a>
  GNU Project will undertake to
publicize copylefted packages that work with X. If you would like to
copyleft something, and you worry that its omission from the X
distribution will impede its popularity, please ask us to help.
 </p>
 <a name="index-call-to-action_002c-resist-illusory-temptations-of-proprietary-software">
 </a>
 <p>
  At the same time, it is better if we do not feel too much need for
popularity. When a businessman tempts you with “more
popularity,” he may try to convince you that his use of your
program is crucial to its success. Don’t believe it! If your program
is good, it will find many users anyway; you don’t need to feel
desperate for any particular users, and you will be stronger if you do
not. You can get an indescribable sense of joy and freedom by
responding, “Take it or leave it—that’s no skin off my
back.” Often the businessman will turn around and accept the
program with copyleft, once you call the bluff.
 </p>
 <a name="index-call-to-action_002c-copyleft-your-software">
 </a>
 <p>
  Friends, free software developers, don’t repeat old mistakes! If we
do not copyleft our software, we put its future at the mercy of anyone
equipped with more resources than scruples. With copyleft, we can
defend freedom, not just for ourselves, but for our whole
community.
  <a name="index-developers_002c-to-copyleft-or-not-to-copyleft_003f-1">
  </a>
  <a name="index-X-Window-System-6">
  </a>
  <a name="index-traps_002c-X-Window-System-1">
  </a>
 </p>
 <hr size="2"/>

