<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Misinterpreting-Copyright_002d_002d_002dA-Series-of-Errors">
 </a>
 <h1 class="chapter">
  18. Misinterpreting Copyright—A Series of Errors
 </h1>
 <a name="index-Constitution_002c-copyright-and-US">
 </a>
 <p>
  Something strange and dangerous is happening in copyright law. Under
the US Constitution, copyright exists to benefit users—those
who read books, listen to music, watch movies, or run software—not
for the sake of publishers or authors. Yet even as people tend
increasingly to reject and disobey the copyright restrictions imposed
on them “for their own benefit,” the US government is
adding more restrictions, and trying to frighten the public into
obedience with harsh new penalties.
 </p>
 <p>
  How did copyright policies come to be diametrically opposed to their
stated purpose? And how can we bring them back into alignment with that
purpose? To understand, we should start by looking at the root of
United States copyright law: the US Constitution.
 </p>
 <a name="Copyright-in-the-US-Constitution">
 </a>
 <h3 class="subheading">
  Copyright in the US Constitution
 </h3>
 <p>
  When the US Constitution was drafted, the idea that authors were
entitled to a copyright monopoly was proposed—and rejected. The
founders of our country adopted a different premise, that copyright is
not a natural right of authors, but an artificial concession made to
them for the sake of progress. The Constitution gives permission for a
copyright system with this clause (Article I, Section 8,
Clause 8):
 </p>
 <blockquote class="smallquotation">
  <p>
   [Congress shall have the power] to promote the Progress of Science and
the useful Arts, by securing for limited Times to Authors and Inventors
the exclusive Right to their respective Writings and Discoveries.
  </p>
 </blockquote>
 <a name="index-Supreme-Court_002c-US-1">
 </a>
 <p>
  The Supreme Court has repeatedly affirmed that promoting progress means
benefit for the users of copyrighted works. For example, in
  <em>
   Fox Film
v. Doyal,
  </em>
  <a href="#FOOT36" name="DOCF36">
   (36)
  </a>
  the court said,
 </p>
 <blockquote class="smallquotation">
  <p>
   The sole interest of the United States and the primary object in
conferring the [copyright] monopoly lie in the general benefits
derived by the public from the labors of authors.
  </p>
 </blockquote>
 <p>
  This fundamental decision explains why copyright is
not
  <em>
   required
  </em>
  by the Constitution, only
  <em>
   permitted
  </em>
  as an
option—and why it is supposed to last for “limited
times.” If copyright were a natural right, something that
authors have because they deserve it, nothing could justify
terminating this right after a certain period of time, any more than
everyone’s house should become public property after a certain lapse
of time from its construction.
 </p>
 <a name="The-_0060_0060Copyright-Bargain_0027_0027">
 </a>
 <h3 class="subheading">
  The “Copyright Bargain”
 </h3>
 <a name="index-copyright_002c-_0060_0060copyright-bargain_0027_0027">
 </a>
 <p>
  The copyright system works by providing privileges and thus benefits
to publishers and authors; but it does not do this for their sake.
Rather, it does this to modify their behavior: to provide an incentive
for authors to write more and publish more. In effect, the government
spends the public’s natural rights, on the public’s behalf, as part of
a deal to bring the public more published works. Legal scholars call
this concept the “copyright bargain.” It is like a
government purchase of a highway or an airplane using taxpayers’
money, except that the government spends our freedom instead of our
money.
 </p>
 <p>
  But is the bargain as it exists actually a good deal for the public?
Many alternative bargains are possible; which one is best? Every
issue of copyright policy is part of this question. If we
misunderstand the nature of the question, we will tend to decide the
issues badly.
 </p>
 <p>
  The Constitution authorizes granting copyright powers to authors. In
practice, authors typically cede them to publishers; it is usually the
publishers, not the authors, who exercise these powers and get most of
the benefits, though authors may get a small portion. Thus it is
usually the publishers that lobby to increase copyright powers. To
better reflect the reality of copyright rather than the myth, this
article refers to publishers rather than authors as the holders of
copyright powers. It also refers to the users of copyrighted works as
“readers,” even though using them does not always mean
reading, because “the users” is remote and abstract.
 </p>
 <a name="The-First-Error_003a-_0060_0060Striking-a-Balance_0027_0027">
 </a>
 <h3 class="subheading">
  The First Error: “Striking a Balance”
 </h3>
 <a name="index-copyright_002c-erroneous-concept-of-_0060_0060striking-a-balance_0027_0027">
 </a>
 <p>
  The copyright bargain places the public first: benefit for the reading
public is an end in itself; benefits (if any) for publishers are just
a means toward that end. Readers’ interests and publishers’ interests
are thus qualitatively unequal in priority. The first step in
misinterpreting the purpose of copyright is the elevation of the
publishers to the same level of importance as the readers.
 </p>
 <p>
  It is often said that US copyright law is meant to “strike a
balance” between the interests of publishers and readers. Those
who cite this interpretation present it as a restatement of the basic
position stated in the Constitution; in other words, it is supposed to
be equivalent to the copyright bargain.
 </p>
 <p>
  But the two interpretations are far from equivalent; they are
different conceptually, and different in their implications. The
balance concept assumes that the readers’ and publishers’ interests
differ in importance only quantitatively, in
  <em>
   how much
weight
  </em>
  we should give them, and in what actions they apply to.
The term “stakeholders” is often used to frame the issue
in this way; it assumes that all kinds of interest in a policy
decision are equally important. This view rejects the qualitative
distinction between the readers’ and publishers’ interests which is at
the root of the government’s participation in the copyright
bargain.
 </p>
 <p>
  The consequences of this alteration are far-reaching, because the
great protection for the public in the copyright bargain—the
idea that copyright privileges can be justified only in the name of
the readers, never in the name of the publishers—is discarded
by the “balance” interpretation. Since the interest of
the publishers is regarded as an end in itself, it can justify
copyright privileges; in other words, the “balance”
concept says that privileges can be justified in the name of someone
other than the public.
 </p>
 <p>
  As a practical matter, the consequence of the “balance”
concept is to reverse the burden of justification for changes in
copyright law. The copyright bargain places the burden on the
publishers to convince the readers to cede certain freedoms. The
concept of balance reverses this burden, practically speaking, because
there is generally no doubt that publishers will benefit from
additional privilege. Unless harm to the readers can be proved,
sufficient to “outweigh” this benefit, we are led to
conclude that the publishers are entitled to almost any privilege they
request.
 </p>
 <p>
  Since the idea of “striking a balance” between publishers and
readers denies the readers the primacy they are entitled to, we must
reject it.
  <a name="index-copyright_002c-erroneous-concept-of-_0060_0060striking-a-balance_0027_0027-1">
  </a>
 </p>
 <a name="Balancing-against-What_003f">
 </a>
 <h3 class="subheading">
  Balancing against What?
 </h3>
 <p>
  When the government buys something for the public, it acts on behalf
of the public; its responsibility is to obtain the best possible
deal—best for the public, not for the other party in the
agreement.
 </p>
 <p>
  For example, when signing contracts with construction companies to build
highways, the government aims to spend as little as possible of the
public’s money. Government agencies use competitive bidding to push the
price down.
 </p>
 <p>
  As a practical matter, the price cannot be zero, because contractors
will not bid that low. Although not entitled to special
consideration, they have the usual rights of citizens in a free
society, including the right to refuse disadvantageous contracts; even
the lowest bid will be high enough for some contractor to make money.
So there is indeed a balance, of a kind. But it is not a deliberate
balancing of two interests each with claim to special consideration.
It is a balance between a public goal and market forces. The
government tries to obtain for the taxpaying motorists the best deal
they can get in the context of a free society and a free market.
 </p>
 <p>
  In the copyright bargain, the government spends our freedom instead of
our money. Freedom is more precious than money, so government’s
responsibility to spend our freedom wisely and frugally is even
greater than its responsibility to spend our money thus. Governments
must never put the publishers’ interests on a par with the public’s
freedom.
 </p>
 <a name="Not-_0060_0060Balance_0027_0027-but-_0060_0060Trade_002dOff_0027_0027">
 </a>
 <h3 class="subheading">
  Not “Balance” but “Trade-Off”
 </h3>
 <p>
  The idea of balancing the readers’ interests against the publishers’
is the wrong way to judge copyright policy, but there are indeed two
interests to be weighed: two interests
  <em>
   of the readers.
  </em>
  Readers
have an interest in their own freedom in using published works;
depending on circumstances, they may also have an interest in
encouraging publication through some kind of incentive system.
 </p>
 <p>
  The word “balance,” in discussions of copyright, has come
to stand as shorthand for the idea of “striking a balance”
between the readers and the publishers. Therefore, to use the word
“balance” in regard to the readers’ two interests would be
confusing. We need another term.
 </p>
 <p>
  In general, when one party has two goals that partly conflict, and
cannot completely achieve both of them, we call this a
“trade-off.” Therefore, rather than speaking of
“striking the right balance” between parties, we should
speak of “finding the right trade-off between spending our
freedom and keeping it.”
 </p>
 <a name="The-Second-Error_003a-Maximizing-One-Output">
 </a>
 <h3 class="subheading">
  The Second Error: Maximizing One Output
 </h3>
 <a name="index-copyright_002c-erroneous-concept-of-maximizing-one-output">
 </a>
 <p>
  The second mistake in copyright policy consists of adopting the goal
of maximizing—not just increasing—the number of
published works. The erroneous concept of “striking a
balance” elevated the publishers to parity with the readers;
this second error places them far above the readers.
 </p>
 <p>
  When we purchase something, we do not generally buy the whole quantity
in stock or the most expensive model. Instead we conserve funds for
other purchases, by buying only what we need of any particular good, and
choosing a model of sufficient rather than highest quality. The
principle of diminishing returns suggests that spending all our money on
one particular good is likely to be an inefficient allocation of resources;
we generally choose to keep some money for another use.
 </p>
 <p>
  Diminishing returns applies to copyright just as to any other
purchase. The first freedoms we should trade away are those we miss
the least, and whose sacrifice gives the largest encouragement to
publication. As we trade additional freedoms that cut closer to home,
we find that each trade is a bigger sacrifice than the last, while
bringing a smaller increment in literary activity. Well before the
increment becomes zero, we may well say it is not worth its
incremental price; we would then settle on a bargain whose overall
result is to increase the amount of publication, but not to the utmost
possible extent.
 </p>
 <p>
  Accepting the goal of maximizing publication rejects all these wiser,
more advantageous bargains in advance—it dictates that the
public must cede nearly all of its freedom to use published works, for
just a little more publication.
  <a name="index-copyright_002c-_0060_0060copyright-bargain_0027_0027-1">
  </a>
 </p>
 <a name="The-Rhetoric-of-Maximization">
 </a>
 <h3 class="subheading">
  The Rhetoric of Maximization
 </h3>
 <a name="index-_0060_0060piracy_002c_0027_0027-erroneous-use-of-term-5">
 </a>
 <p>
  In practice, the goal of maximizing publication regardless of the cost
to freedom is supported by widespread rhetoric which asserts that
public copying is illegitimate, unfair, and intrinsically wrong. For
instance, the publishers call people who copy “pirates,” a
smear term designed to equate sharing information with your neighbor
with attacking a ship. (This smear term was formerly used by authors
to describe publishers who found lawful ways to publish unauthorized
editions; its modern use by the publishers is almost the reverse.)
This rhetoric directly rejects the constitutional basis for copyright,
but presents itself as representing the unquestioned tradition of the
American legal system.
 </p>
 <p>
  The “pirate” rhetoric is typically accepted because it
so pervades the media that few people realize how radical it is. It
is effective because if copying by the public is fundamentally
illegitimate, we can never object to the publishers’ demand that we
surrender our freedom to do so. In other words, when the public is
challenged to show why publishers should not receive some additional
power, the most important reason of all—“We want to
copy”—is disqualified in advance.
 </p>
 <p>
  This leaves no way to argue against increasing copyright power except
using side issues. Hence, opposition to stronger copyright powers today
almost exclusively cites side issues, and never dares cite the freedom
to distribute copies as a legitimate public value.
 </p>
 <p>
  As a practical matter, the goal of maximization enables publishers to
argue that “A certain practice is reducing our sales—or
we think it might—so we presume it diminishes publication by
some unknown amount, and therefore it should be prohibited.” We
are led to the outrageous conclusion that the public good is measured
by publishers’ sales: What’s good for General Media is good for the
USA.
  <a name="index-_0060_0060piracy_002c_0027_0027-erroneous-use-of-term-6">
  </a>
 </p>
 <a name="The-Third-Error_003a-Maximizing-Publishers_0027-Power">
 </a>
 <h3 class="subheading">
  The Third Error: Maximizing Publishers’ Power
 </h3>
 <a name="index-copyright_002c-erroneous-concept-of-maximizing-publishers_0027-power">
 </a>
 <p>
  Once the publishers have obtained assent to the policy goal of
maximizing publication output at any cost, their next step is to infer
that this requires giving them the maximum possible powers—making
copyright cover every imaginable use of a work, or applying
some other legal tool such as “shrink wrap” licenses to
equivalent effect. This goal, which entails the abolition of
  <a name="index-copyright_002c-fair-use">
  </a>
  <a name="index-fair-use-_0028see-also-copyright_0029">
  </a>
  “fair use” and the “right of first sale,” is
being pressed at every available level of government, from states of
the US to international bodies.
 </p>
 <p>
  This step is erroneous because strict copyright rules obstruct the
creation of useful new works. For instance,
  <a name="index-Shakespeare_002c-William">
  </a>
  Shakespeare borrowed the
plots of some of his plays from works others had published a few decades
before, so if today’s copyright law had been in effect, his plays would
have been illegal.
 </p>
 <p>
  Even if we wanted the highest possible rate of publication, regardless
of cost to the public, maximizing publishers’ power is the wrong way to
get it. As a means of promoting progress, it is self-defeating.
 </p>
 <a name="The-Results-of-the-Three-Errors">
 </a>
 <h3 class="subheading">
  The Results of the Three Errors
 </h3>
 <a name="index-copyright_002c-duration-of-term-of">
 </a>
 <p>
  The current trend in copyright legislation is to hand publishers broader
powers for longer periods of time. The conceptual basis of copyright,
as it emerges distorted from the series of errors, rarely offers a basis
for saying no. Legislators give lip service to the idea that copyright
serves the public, while in fact giving publishers whatever they ask
for.
 </p>
 <p>
  For example, here is what Senator
  <a name="index-Hatch_002c-Senator-Orrin">
  </a>
  Hatch said when introducing S. 483,
  <a href="#FOOT37" name="DOCF37">
   (37)
  </a>
  a 1995 bill to increase the term of copyright by 20 years:
 </p>
 <blockquote class="smallquotation">
  <p>
   I believe we are now at such a point with respect to the question of
whether the current term of copyright adequately protects the
interests of authors and the related question of whether the term of
protection continues to provide a sufficient incentive for the
creation of new works of authorship.
   <a href="#FOOT38" name="DOCF38">
    (38)
   </a>
  </p>
 </blockquote>
 <p>
  This bill extended the copyright on already published works written
since the 1920s. This change was a giveaway to publishers with no
possible benefit to the public, since there is no way to retroactively
increase now the number of books published back then. Yet it cost the
public a freedom that is meaningful today—the freedom to
redistribute books from that era.
 </p>
 <p>
  The bill also extended the copyrights of works yet to be written. For
works made for hire, copyright would last 95 years instead of the
present 75 years. Theoretically this would increase the incentive to
write new works; but any publisher that claims to need this extra
incentive should be required to substantiate the claim with projected
balance sheets for 75 years in the future.
 </p>
 <p>
  Needless to say, Congress did not question the publishers’ arguments:
a law extending copyright was enacted in 1998. It was officially
called the
  <a name="index-Sonny-Bono-Copyright-Term-Extension-Act-_0028also-known-as">
  </a>
  the Mickey Mouse Copyright Act
  <em>
   )
  </em>
  Sonny Bono Copyright Term Extension Act, named after one of
its sponsors who died earlier that year. We usually call it the
Mickey Mouse Copyright Act, since we presume its real motive was to
prevent the copyright on the appearance of Mickey Mouse from expiring.
Bono’s widow, who served the rest of his term, made this statement:
 </p>
 <blockquote class="smallquotation">
  <p>
   Actually,
   <a name="index-Bono_002c-Congressman-Sonny">
   </a>
   Sonny wanted the term of copyright protection to last forever.
I am informed by staff that such a change would violate the Constitution. 
I invite all of you to work with me to strengthen our copyright laws in 
all of the ways available to us. As you know, there is also
   <a name="index-Valenti_002c-Jack">
   </a>
   Jack Valenti’s
   <a href="#FOOT39" name="DOCF39">
    (39)
   </a>
   proposal for term to last forever less one
day. Perhaps the Committee may look at
that next Congress.
   <a href="#FOOT40" name="DOCF40">
    (40)
   </a>
  </p>
 </blockquote>
 <a name="index-Supreme-Court_002c-US-2">
 </a>
 <p>
  The Supreme Court later heard a case that sought to overturn the law
on the grounds that the retroactive extension fails to serve the
Constitution’s goal of promoting progress. The court responded by
abdicating its responsibility to judge the question; on copyright, the
Constitution requires only lip service.
  <a name="index-copyright_002c-duration-of-term-of-1">
  </a>
 </p>
 <p>
  Another law, passed in 1997, made it a felony to make sufficiently many
copies of any published work, even if you give them away to friends just
to be nice. Previously this was not a crime in the US at all.
 </p>
 <a name="index-DMCA_002c-publishers-and">
 </a>
 <p>
  An even worse law, the Digital Millennium Copyright Act (DMCA), was
designed to bring back copy protection (which computer users detest)
by making it a crime to break copy protection, or even publish
information about how to break it. This law ought to be called the
“Domination by Media Corporations Act” because it
effectively offers publishers the chance to write their own copyright
law. It says they can impose any restrictions whatsoever on the use
of a work, and these restrictions take the force of law provided the
work contains some sort of encryption or license manager to enforce
them.
 </p>
 <p>
  One of the arguments offered for this bill was that it would implement
a recent treaty to increase copyright powers. The treaty was
promulgated by the
  <a name="index-World-_0060_0060Intellectual-Property_0027_0027-Organization-_0028WIPO_0029-_0028see-also-_0060_0060intellectual-property_0027_0027_0029-3">
  </a>
  World “Intellectual
Property” Organization, an organization dominated by
copyright- and patent-holding interests, with the aid of
pressure from the
  <a name="index-Clinton-administration">
  </a>
  Clinton administration; since the treaty only
increases copyright power, whether it serves the public interest in
any country is doubtful. In any case, the bill went far beyond what
the treaty required.
 </p>
 <a name="index-libraries_002c-DMCA_002c-fair-use_002c-and-_0028see-also-DMCA_0029">
 </a>
 <p>
  Libraries were a key source of opposition to this bill, especially to
the aspects that block the forms of copying that are considered
  <a name="index-copyright_002c-fair-use-and-libraries">
  </a>
  fair use. How did the publishers respond? Former
representative
  <a name="index-Schroeder_002c-Pat">
  </a>
  Pat Schroeder, now a lobbyist for the
  <a name="index-copyright_002c-Association-of-American-Publishers">
  </a>
  <a name="index-Association-of-American-Publishers-_0028see-also-copyright_0029">
  </a>
  Association of
American Publishers, said that the publishers “could not live
with what [the libraries were] asking for.” Since the libraries
were asking only to preserve part of the status quo, one might respond
by wondering how the publishers had survived until the present
day.
 </p>
 <a name="index-copyright_002c-disregard-for-US-Constitution_0027s-view-of">
 </a>
 <p>
  Congressman
  <a name="index-Frank_002c-Congressman-Barney">
  </a>
  Barney Frank, in a meeting with me and others who opposed
this bill, showed how far the US Constitution’s view of copyright
has been disregarded. He said that new powers, backed by criminal
penalties, were needed urgently because the “movie industry is
worried,” as well as the “music industry” and other
“industries.” I asked him, “But is this in the
public interest?” His response was telling: “Why are you
talking about the public interest? These creative people don’t have
to give up their rights for the public interest!” The
“industry” has been identified with the “creative
people” it hires, copyright has been treated as its entitlement,
and the Constitution has been turned upside down.
  <a name="index-Constitution_002c-copyright-and-US-1">
  </a>
 </p>
 <a name="index-DMCA_002c-and-fair-use">
 </a>
 <p>
  The DMCA was enacted in 1998. As enacted, it says that fair use remains
nominally legitimate, but allows publishers to prohibit all software or
hardware that you could practice it with. Effectively, fair use
is prohibited.
 </p>
 <p>
  Based on this law, the movie industry has imposed censorship on free
software for reading and playing DVDs, and even on the information
about how to read them. In April 2001, Professor
  <a name="index-Felten_002c-Edward">
  </a>
  Edward Felten of
Princeton University was intimidated by lawsuit threats from the
  <a name="index-Recording-Industry-Association-of-America-_0028RIAA_0029">
  </a>
  Recording Industry Association of America (RIAA) into withdrawing a
scientific paper stating what he had learned about a proposed
encryption system for restricting access to recorded music.
 </p>
 <a name="index-libraries_002c-e_002dbooks-and">
 </a>
 <a name="index-e_002dbooks">
 </a>
 <p>
  We are also beginning to see e-books that take away many of readers’
traditional freedoms—for instance, the freedom to lend a book
to your friend, to sell it to a used book store, to borrow it from a
library, to buy it without giving your name to a corporate data bank,
even the freedom to read it twice. Encrypted e-books generally
restrict all these activities—you can read them only with
special secret software designed to restrict you.
 </p>
 <p>
  I will never buy one of these encrypted, restricted e-books, and I
hope you will reject them too. If an e-book doesn’t give you the same
freedoms as a traditional paper book, don’t accept it!
 </p>
 <p>
  Anyone independently releasing software that can read restricted
e-books risks prosecution. A Russian programmer,
  <a name="index-Sklyarov_002c-Dmitry">
  </a>
  Dmitry Sklyarov, was
arrested in 2001 while visiting the US to speak at a conference,
because he had written such a program in
  <a name="index-Russia-1">
  </a>
  Russia, where it was lawful
to do so. Now Russia is preparing a law to prohibit it too, and the
  <a name="index-European-Union-1">
  </a>
  European Union recently adopted one.
 </p>
 <p>
  Mass-market e-books have been a commercial failure so far, but not
because readers chose to defend their freedom; they were unattractive
for other reasons, such as that computer display screens are not easy
surfaces to read from. We can’t rely on this happy accident to
protect us in the long term; the next attempt to promote e-books will
use “electronic paper”—book-like objects into
which an encrypted, restricted e-book can be downloaded. If this
paper-like surface proves more appealing than today’s display screens,
we will have to defend our freedom in order to keep it. Meanwhile,
e-books are making inroads in niches:
  <a name="index-NYU-1">
  </a>
  NYU and other dental schools
require students to buy their textbooks in the form of restricted
e-books.
  <a name="index-e_002dbooks-1">
  </a>
 </p>
 <p>
  The media companies are not satisfied yet. In 2001,
  <a name="index-Disney-1">
  </a>
  Disney-funded
Senator
  <a name="index-Hollings_002c-Senator-Ernest-1">
  </a>
  Hollings proposed a bill called the
  <a name="index-Security-Systems-Standards-and-Certification-Act-_0028SSSCA_0029-_0028see-also-Consumer-Broadband-and-Digital-Television-Promotion-Act-_0028CBDTPA_0029_0029-1">
  </a>
  “Security Systems
Standards and Certification Act”
(SSSCA),
  <a href="#FOOT41" name="DOCF41">
   (41)
  </a>
  which would require all computers
(and other digital recording and playback devices) to have
government-mandated copy-restriction systems. That is their ultimate
goal, but the first item on their agenda is to prohibit any equipment
that can tune digital
  <a name="index-HDTV">
  </a>
  HDTV unless it is designed to be impossible for
the public to “tamper with” (i.e., modify for their own
purposes). Since free software is software that users can modify, we
face here for the first time a proposed law that explicitly prohibits
free software for a certain job. Prohibition of other jobs will
surely follow. If the
  <a name="index-FCC">
  </a>
  FCC adopts this rule, existing free software
such as
  <a name="index-GNU_002c-GNU-Radio">
  </a>
  GNU Radio would be censored.
 </p>
 <p>
  To block these bills and rules requires political
action.
  <a href="#FOOT42" name="DOCF42">
   (42)
  </a>
 </p>
 <a name="Finding-the-Right-Bargain">
 </a>
 <h3 class="subheading">
  Finding the Right Bargain
 </h3>
 <a name="index-copyright_002c-_0060_0060copyright-bargain_0027_0027-2">
 </a>
 <p>
  What is the proper way to decide copyright policy? If copyright is a
bargain made on behalf of the public, it should serve the public
interest above all. The government’s duty when selling the public’s
freedom is to sell only what it must, and sell it as dearly as possible.
At the very least, we should pare back the extent of copyright as much
as possible while maintaining a comparable level of publication.
 </p>
 <p>
  Since we cannot find this minimum price in freedom through competitive
bidding, as we do for construction projects, how can we find it?
 </p>
 <p>
  One possible method is to reduce copyright privileges in stages, and
observe the results. By seeing if and when measurable diminutions in
publication occur, we will learn how much copyright power is really
necessary to achieve the public’s purposes. We must judge this by
actual observation, not by what publishers say will happen, because
they have every incentive to make exaggerated predictions of doom if
their powers are reduced in any way.
 </p>
 <p>
  Copyright policy includes several independent dimensions, which can be
adjusted separately. After we find the necessary minimum for one policy
dimension, it may still be possible to reduce other dimensions of
copyright while maintaining the desired publication level.
 </p>
 <p>
  One important dimension of copyright is its duration, which is now
typically on the order of a century. Reducing the monopoly on copying
to ten years, starting from the date when a work is published, would be
a good first step. Another aspect of copyright, which covers the
making of derivative works, could continue for a longer period.
 </p>
 <a name="index-copyright_002c-duration-of-term-of-2">
 </a>
 <p>
  Why count from the date of publication? Because copyright on
unpublished works does not directly limit readers’ freedom; whether we
are free to copy a work is moot when we do not have copies. So giving
authors a longer time to get a work published does no harm. Authors
(who generally do own the copyright prior to publication) will rarely
choose to delay publication just to push back the end of the copyright
term.
 </p>
 <p>
  Why ten years? Because that is a safe proposal; we can be confident on
practical grounds that this reduction would have little impact on the
overall viability of publishing today. In most media and genres,
successful works are very profitable in just a few years, and even
successful works are usually out of print well before ten. Even for
reference works, whose useful life may be many decades, ten-year
copyright should suffice: updated editions are issued regularly, and
many readers will buy the copyrighted current edition rather than copy a
ten-year-old public domain version.
 </p>
 <p>
  Ten years may still be longer than necessary; once things settle down,
we could try a further reduction to tune the system. At a panel on
copyright at a literary convention, where I proposed the ten-year term,
a noted fantasy author sitting beside me objected vehemently, saying
that anything beyond five years was intolerable.
 </p>
 <p>
  But we don’t have to apply the same time span to all kinds of works.
Maintaining the utmost uniformity of copyright policy is not crucial
to the public interest, and copyright law already has many exceptions
for specific uses and media. It would be foolish to pay for every
highway project at the rates necessary for the most difficult projects
in the most expensive regions of the country; it is equally foolish to
“pay” for all kinds of art with the greatest price in
freedom that we find necessary for any one kind.
 </p>
 <p>
  So perhaps novels, dictionaries, computer programs, songs, symphonies,
and movies should have different durations of copyright, so that we can
reduce the duration for each kind of work to what is necessary for many
such works to be published. Perhaps movies over one hour long could
have a 20-year copyright, because of the expense of producing them.
In my own field, computer programming, three years should suffice,
because product cycles are even shorter than that.
 </p>
 <p>
  Another dimension of copyright policy is the extent of
  <a name="index-copyright_002c-fair-use-1">
  </a>
  <a name="index-fair-use-_0028see-also-copyright_0029-1">
  </a>
  fair use: some
ways of reproducing all or part of a published work that are legally
permitted even though it is copyrighted. The natural first step in
reducing this dimension of copyright power is to permit occasional
private small-quantity noncommercial copying and distribution among
individuals. This would eliminate the intrusion of the copyright
police into people’s private lives, but would probably have little
effect on the sales of published works. (It may be necessary to take
other legal steps to ensure that shrink-wrap licenses cannot be used
to substitute for copyright in restricting such copying.) The
experience of
  <a name="index-Napster">
  </a>
  Napster shows that we should also permit noncommercial
verbatim redistribution to the general public—when so many of
the public want to copy and share, and find it so useful, only
draconian measures will stop them, and the public deserves to get what
it wants.
 </p>
 <p>
  For novels, and in general for works that are used for entertainment,
noncommercial verbatim redistribution may be sufficient freedom for
the readers. Computer programs, being used for functional purposes
(to get jobs done), call for additional freedoms beyond that,
including the freedom to publish an improved version. See “The Free
Software Definition,” in this book, for an explanation of the
freedoms that software users should have. But it may be an acceptable
compromise for these freedoms to be universally available only after a
delay of two or three years from the program’s publication.
 </p>
 <p>
  Changes like these could bring copyright into line with the public’s
wish to use digital technology to copy. Publishers will no doubt find
these proposals “unbalanced”; they may threaten to take
their marbles and go home, but they won’t really do it, because the
game will remain profitable and it will be the only game in town.
 </p>
 <p>
  As we consider reductions in copyright power, we must make sure media
companies do not simply replace it with end-user license agreements.
It would be necessary to prohibit the use of contracts to apply
restrictions on copying that go beyond those of copyright. Such
limitations on what mass-market nonnegotiated contracts can require
are a standard part of the US legal system.
  <a name="index-copyright_002c-_0060_0060copyright-bargain_0027_0027-3">
  </a>
 </p>
 <a name="A-Personal-Note">
 </a>
 <h3 class="subheading">
  A Personal Note
 </h3>
 <a name="index-call-to-action_002c-do-not-surrender-freedom-in-author_0027s-name">
 </a>
 <a name="index-users_002c-premise-of-author-supremacy-_0028see-also-ownership_0029-4">
 </a>
 <p>
  I am a software designer, not a legal scholar. I’ve become concerned
with copyright issues because there’s no avoiding them in the world of
computer networks, such as the Internet. As a user of computers and networks for 30 years, I value the freedoms that we have lost, and the ones we
may lose next. As an author, I can reject the romantic mystique of the
author as semidivine creator, often cited by publishers to justify
increased copyright powers for authors—powers which these authors
will then sign away to publishers.
 </p>
 <p>
  Most of this article consists of facts and reasoning that you can
check, and proposals on which you can form your own opinions. But I ask
you to accept one thing on my word alone: that authors like me don’t
deserve special power over you. If you wish to reward me further for
the software or books I have written, I would gratefully accept a
check—but please don’t surrender your freedom in my name.
 </p>
 <div class="footnote">
  <hr>
   <h3>
    Footnotes
   </h3>
   <h3>
    <a href="#DOCF36" name="FOOT36">
     (36)
    </a>
   </h3>
   <a name="index-Fox-Film-Corp_002e-v_002e-Doyal-1">
   </a>
   <p>
    <cite>
     Fox Film Corp. v. Doyal,
    </cite>
    286 US 123, 1932.
   </p>
   <h3>
    <a href="#DOCF37" name="FOOT37">
     (37)
    </a>
   </h3>
   <p>
    <cite>
     Congressional Record,
    </cite>
    S. 483, “The Copyright Term Extension Act of 1995,” 2 March 1995, pp. S3390–4.
   </p>
   <h3>
    <a href="#DOCF38" name="FOOT38">
     (38)
    </a>
   </h3>
   <p>
    <cite>
     Congressional
Record,
    </cite>
    “Statement on Introduced Bills and Joint Resolutions,”
2 March 1995, p. S3390,
    <a href="http://gpo.gov/fdsys/pkg/CREC-1995-03-02/pdf/CREC-1995-03-02-pt1-PgS3390-2.pdf">
     http://gpo.gov/fdsys/pkg/CREC-1995-03-02/pdf/CREC-1995-03-02-pt1-PgS3390-2.pdf
    </a>
    .
   </p>
   <h3>
    <a href="#DOCF39" name="FOOT39">
     (39)
    </a>
   </h3>
   <p>
    Jack Valenti was a longtime president of the Motion
Picture Association of America.
   </p>
   <h3>
    <a href="#DOCF40" name="FOOT40">
     (40)
    </a>
   </h3>
   <p>
    <cite>
     Congressional Record,
    </cite>
    remarks of
Rep.
    <a name="index-Bono_002c-Congresswoman-Mary">
    </a>
    Bono, 7 October 1998, p. H9952,
    <a href="http://gpo.gov/fdsys/pkg/CREC-1998-10-07/pdf/CREC-1998-10-07-pt1-PgH9946.pdf">
     http://gpo.gov/fdsys/pkg/CREC-1998-10-07/pdf/CREC-1998-10-07-pt1-PgH9946.pdf
    </a>
    .
   </p>
   <h3>
    <a href="#DOCF41" name="FOOT41">
     (41)
    </a>
   </h3>
   <p>
    Since renamed to the unpronounceable
    <a name="index-Consumer-Broadband-and-Digital-Television-Promotion-Act-_0028CBDTPA_0029-2">
    </a>
    CBDTPA,
for which a good mnemonic is “Consume, But Don’t Try
Programming Anything,” but it really stands for the
“Consumer Broadband and Digital Television Promotion
Act.”
   </p>
   <h3>
    <a href="#DOCF42" name="FOOT42">
     (42)
    </a>
   </h3>
   <p>
    If you would like to help, I recommend the web
sites
    <a name="index-Defective-by-Design-_0028see-also-DRM_0029-2">
    </a>
    <a href="http://defectivebydesign.org">
     http://defectivebydesign.org
    </a>
    ,
    <a href="http://publicknowledge.org">
     http://publicknowledge.org
    </a>
    , and
    <a href="http://eff.org">
     http://eff.org
    </a>
    .
   </p>
  </hr>
 </div>
 <hr size="2"/>

