<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="The-Problem-Is-Software-Controlled-by-Its-Developer">
 </a>
 <h1 class="chapter">
  37. The Problem Is Software Controlled by Its Developer
 </h1>
 <a name="index-Zittrain_002c-Jonathan">
 </a>
 <p>
  I fully agree with Jonathan Zittrain’s conclusion that we should not
abandon general-purpose computers. Alas, I disagree completely with
the path that led him to it. He presents serious security problems as
an intolerable crisis, but I’m not convinced. Then he forecasts that
users will panic in response and stampede toward restricted computers
(which he calls “appliances”), but there is no sign of this happening.
 </p>
 <a name="index-zombie-machines">
 </a>
 <a name="index-phishing">
 </a>
 <p>
  Zombie machines are a problem, but not a catastrophe. Moreover, far
from panicking, most users ignore the issue. Today, people are indeed
concerned about the danger of phishing (mail and web pages that
solicit personal information for fraud), but using a browsing-only
device instead of a general computer won’t protect you from that.
 </p>
 <a name="index-Apple_002c-iPhone-_0028see-also-cell-phones_0029">
 </a>
 <p>
  Meanwhile, Apple has reported that 25 percent of iPhones have been
unlocked. Surely at least as many users would have preferred an
unlocked iPhone but were afraid to try a forbidden recipe to obtain
it. This refutes the idea that users generally prefer that their
devices be locked.
 </p>
 <a name="index-RealPlayer-_0028see-also-DRM_0029-1">
 </a>
 <a name="index-Adobe-Flash">
 </a>
 <a name="index-proprietary-software_002c-spying-on-users-1">
 </a>
 <p>
  It is true that a general computer lets you run programs designed to
spy on you, restrict you, or even let the developer attack you. Such
programs include
  <a name="index-KaZaA-_0028see-also-both-DRM-and-treacherous-computing_0029-1">
  </a>
  KaZaA, RealPlayer, Adobe Flash,
  <a name="index-Windows-Media-Player-_0028see-also-both-DRM-and-treacherous-computing_0029-2">
  </a>
  Windows Media Player,
Microsoft
  <a name="index-Windows-4">
  </a>
  Windows, and MacOS.
  <a name="index-Windows_002c-Vista-1">
  </a>
  <a name="index-Vista_002c-Windows-_0028see-also-both-Windows-and-DRM_0029-1">
  </a>
  Windows Vista does all three of those
things; it also lets Microsoft change the software without asking, or
command it to permanently cease normal functioning.
 </p>
 <p>
  But restricted computers are no help, because they present the
same problem for the same reason.
 </p>
 <p>
  The iPhone is designed for remote attack by Apple. When Apple remotely
destroys iPhones that users have unlocked to enable other uses, that
is no better than when Microsoft remotely sabotages
  <a name="index-Vista_002c-Windows-_0028see-also-both-Windows-and-DRM_0029-2">
  </a>
  Vista. The
  <a name="index-TiVo-_0028see-also-tivoization_0029">
  </a>
  <a name="index-tivoization-5">
  </a>
  TiVo is
designed to enforce restrictions on access to the recordings you make,
and reports what you watch.
  <a name="index-e_002dbooks-4">
  </a>
  E-book readers such as the
  <a name="index-Amazon-2">
  </a>
  Amazon
  <a name="index-Swindle-2">
  </a>
  “Swindle” are designed to stop you from sharing and lending your
books. Features that artificially obstruct use of your data are known
  <a name="index-DRM_002c-call-it-_0060_0060Digital-Restrictions-Management_0027_0027-5">
  </a>
  <a name="index-_0060_0060Digital-Rights-Management_002c_0027_0027-avoid-use-of-term-_0028see-also-DRM_0029-1">
  </a>
  as Digital Restrictions Management (DRM); our protest campaign against
DRM is hosted at
  <a name="index-Defective-by-Design-_0028see-also-DRM_0029-4">
  </a>
  <a href="http://defectivebydesign.org">
   http://defectivebydesign.org
  </a>
  . (Our adversaries call DRM
“Digital Rights Management” based on their idea that restricting you
is their right. When you choose a term, you choose your side.)
 </p>
 <p>
  The nastiest of the common restricted devices are
  <a name="index-cell-phones-_0028see-also-both-OpenMoko-and-Apple_0029">
  </a>
  cell phones. They
transmit signals for tracking your whereabouts even when switched
“off”; the only way to stop this is to take out all the
batteries. Many can also be turned on remotely, for listening,
unbeknownst to you. (The
  <a name="index-FBI-1">
  </a>
  FBI is already taking advantage of this
feature, and the
  <a name="index-Commerce-Department_002c-US">
  </a>
  US Commerce Department lists this danger in its
Security Guide.) Cellular phone network companies regularly install
software in users phones, without asking, to impose new usage
restrictions.
 </p>
 <p>
  With a general computer you can escape by rejecting such programs. You
don’t have to have KaZaA, RealPlayer, Adobe Flash,
  <a name="index-Windows-Media-Player-_0028see-also-both-DRM-and-treacherous-computing_0029-3">
  </a>
  Windows Media
Player, Microsoft Windows or
  <a name="index-MacOS-_0028see-also-DRM_0029">
  </a>
  MacOS on your computer (I don’t). By
contrast, a restricted computer gives you no escape from the software
built into it.
  <a name="index-KaZaA-_0028see-also-both-DRM-and-treacherous-computing_0029-2">
  </a>
  <a name="index-Adobe-Flash-1">
  </a>
  <a name="index-RealPlayer-_0028see-also-DRM_0029-2">
  </a>
 </p>
 <a name="index-development_002c-developer-control">
 </a>
 <p>
  The root of this problem, both in general PCs and restricted
computers, is software controlled by its developer. The developer
(typically a corporation) controls what the program does, and prevents
everyone else from changing it. If the developer decides to put in
malicious features, even a master programmer cannot easily remove
them.
 </p>
 <a name="index-users_002c-benefit-to-4">
 </a>
 <a name="index-call-to-action_002c-insist-on-free-software">
 </a>
 <p>
  The remedy is to give the users more control, not less. We must insist
on free/libre software, software that the users are free to change and
redistribute. Free/libre software develops under the control of its
users: if they don’t like its features, for whatever reason, they can
change them. If you’re not a programmer, you still get the benefit of
control by the users. A programmer can make the improvements you would
like, and publish the changed version. Then you can use it too.
 </p>
 <a name="index-malware">
 </a>
 <p>
  With free/libre software, no one has the power to make a malicious
feature stick. Since the source code is available to the users,
millions of programmers are in a position to spot and remove the
malicious feature and release an improved version; surely someone
will do it. Others can then compare the two versions
to verify independently which version treats users right. As a practical
fact, free software is generally free of designed-in malware.
 </p>
 <a name="index-call-to-action_002c-price-deception">
 </a>
 <p>
  Many people do acquire restricted devices, but not for motives of
security. Why do people choose them?
 </p>
 <p>
  Sometimes it is because the restricted devices are physically
smaller. I edit text all day (literally) and I find the keyboard and
screen of a laptop well worth the size and weight. However, people who
use computers differently may prefer something that fits in a
pocket. In the past, these devices have typically been restricted, but
they weren’t chosen for that reason.
 </p>
 <p>
  Now they are becoming less restricted. In fact, the
  <a name="index-OpenMoko-_0028see-also-cell-phones_0029">
  </a>
  OpenMoko cell
phone features a main computer running entirely free/libre software,
including the GNU/Linux operating system normally used on PCs and
servers.
 </p>
 <a name="index-games_002c-price-deception-and">
 </a>
 <p>
  A major cause for the purchase of some restricted computers is
financial sleight of hand. Game consoles, and the iPhone, are sold for an
unsustainably low price, and the manufacturers subsequently charge when you use
them. Thus, game developers must pay the game console manufacturer to
distribute a game, and they pass this cost on to the
user. Likewise,
  <a name="index-AT_0026T">
  </a>
  AT&amp;T pays Apple when an iPhone is used as a
telephone. The low up-front price misleads customers into thinking
they will save money.
  <a name="index-Apple_002c-iPhone-_0028see-also-cell-phones_0029-1">
  </a>
 </p>
 <p>
  If we are concerned about the spread of restricted computers, we
should tackle the issue of the price deception that sells them.
If we are concerned about malware, we should insist on free
software that gives the users control.
  <a name="index-call-to-action_002c-price-deception-1">
  </a>
  <a name="index-malware-1">
  </a>
 </p>
 <a name="Postnote">
 </a>
 <h3 class="subheading">
  Postnote
 </h3>
 <a name="index-development_002c-patents-2">
 </a>
 <p>
  Zittrain’s suggestion to reduce the statute of limitations
on software patent lawsuits is a tiny step in the right direction, but
it is much easier to solve the whole problem. Software patents are an
unnecessary, artificial danger imposed on all software developers and
users in the US. Every program is a combination of many methods and
techniques—thousands of them in a large program. If patenting these
methods is allowed, then hundreds of those used in a given program are
probably patented. (Avoiding them is not feasible; there may be no
alternatives, or the alternatives may be patented too.) So the
developers of the program face hundreds of potential lawsuits from
parties unknown, and the users can be sued as well.
 </p>
 <p>
  The complete, simple solution is to eliminate patents from the field
of software. Since the patent system is created by statute, eliminating
patents from software will be easy given sufficient political
will. (See
  <a href="http://www.endsoftpatents.org">
   http://www.endsoftpatents.org
  </a>
  .)
  <a name="index-Zittrain_002c-Jonathan-1">
  </a>
 </p>
 <hr size="2"/>

