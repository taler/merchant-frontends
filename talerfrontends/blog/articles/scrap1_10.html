<!-- This is the second edition of Free Software, Free Society: Selected Essays of Richard M. Stallman.

Free Software Foundation

51 Franklin Street, Fifth Floor

Boston, MA 02110-1335
Copyright C 2002, 2010 Free Software Foundation, Inc.
Verbatim copying and distribution of this entire book are permitted
worldwide, without royalty, in any medium, provided this notice is
preserved. Permission is granted to copy and distribute translations
of this book from the original English into another language provided
the translation has been approved by the Free Software Foundation and
the copyright notice and this permission notice are preserved on all
copies.

ISBN 978-0-9831592-0-9
Cover design by Rob Myers.

Cover photograph by Peter Hinely.
 -->


 <a name="Selling-Free-Software">
 </a>
 <h1 class="chapter">
  10. Selling Free Software
 </h1>
 <a name="index-selling_002c-free-software-2">
 </a>
 <p>
  Many people believe that the spirit of the
  <a name="index-GNU_002c-GNU-Project-4">
  </a>
  GNU Project is that you
should not charge money for distributing copies of software, or that
you should charge as little as possible—just enough to cover
the cost. This is a misunderstanding.
 </p>
 <p>
  Actually, we encourage people who redistribute free software
to charge as much as they wish or can. If this seems surprising to
you, please read on.
 </p>
 <p>
  The word “free” has two legitimate general meanings; it can refer
either to freedom or to price. When we speak of “free software,”
we’re talking about freedom, not price. (Think of “free speech,”
not “free beer.”) Specifically, it means that a user is free to run
the program, change the program, and redistribute the program with or
without changes.
 </p>
 <p>
  Free programs are sometimes distributed gratis, and sometimes for a
substantial price. Often the same program is available in both ways
from different places. The program is free regardless of the price,
because users have freedom in using it.
 </p>
 <p>
  Nonfree programs are usually sold for a high price, but sometimes a store will give you a copy at no charge. That doesn’t make it free software, though. Price or no price, the program is nonfree because users don’t have freedom.
 </p>
 <p>
  Since free software is not a matter of price, a low price doesn’t make
the software
free, or even closer to free. So if you are redistributing copies of free
software, you might as well charge a substantial fee and
  <em>
   make
some money.
  </em>
  Redistributing free software is a good and
legitimate activity; if you do it, you might as well make a profit
from it.
 </p>
 <a name="index-call-to-action_002c-raise-funds">
 </a>
 <a name="index-development_002c-fundraising-1">
 </a>
 <p>
  Free software is a community project, and everyone who depends on it
ought to look for ways to contribute to building the community. For a
distributor, the way to do this is to give a part of the profit to free software development projects or to the
  <a name="index-FSF_002c-how-you-can-help-3">
  </a>
  <a name="index-development_002c-fundraising-2">
  </a>
  <a name="index-development_002c-contributions-and-donations-2">
  </a>
  Free Software Foundation. This way you can
advance the world of free software.
 </p>
 <p>
  <em>
   Distributing free software is an opportunity to raise funds for development. Don’t waste it!
  </em>
 </p>
 <p>
  In order to contribute funds, you need to have some extra. If you
charge too low a fee, you won’t have anything to spare to support
development.
 </p>
 <a name="Will-a-Higher-Distribution-Price-Hurt-Some-Users_003f">
 </a>
 <h3 class="subheading">
  Will a Higher Distribution Price Hurt Some Users?
 </h3>
 <p>
  People sometimes worry that a high distribution fee will put free
software out of range for users who don’t have a lot of money. With
proprietary software, a high price does exactly that—but free software
is different.
 </p>
 <p>
  The difference is that free software naturally tends to spread around,
and there are many ways to get it.
 </p>
 <p>
  Software hoarders try their damnedest to stop you from running a
proprietary program without paying the standard price. If this price
is high, that does make it hard for some users to use the program.
 </p>
 <p>
  With free software, users don’t
  <em>
   have
  </em>
  to pay the
distribution fee in order to use the software. They can copy the
program from a friend who has a copy, or with the help of a friend who
has network access. Or several users can join together, split the
price of one CD-ROM, then each in turn can install the software. A high
CD-ROM price is not a major obstacle when the software is free.
 </p>
 <a name="Will-a-Higher-Distribution-Price-Discourage-Use-of-Free-Software_003f">
 </a>
 <h3 class="subheading">
  Will a Higher Distribution Price Discourage Use of Free Software?
 </h3>
 <a name="index-selling_002c-and-distribution-fees">
 </a>
 <p>
  Another common concern is for the popularity of free software. People
think that a high price for distribution would reduce the number of
users, or that a low price is likely to encourage users.
 </p>
 <p>
  This is true for proprietary software—but free software is
different. With so many ways to get copies, the price of distribution
service has less effect on popularity.
 </p>
 <a name="index-call-to-action_002c-develop-more-free-software">
 </a>
 <p>
  In the long run, how many people use free software is determined
mainly by
  <em>
   how much free software can do,
  </em>
  and how easy it
is to use. Many users do not make freedom their priority; they
may continue to use proprietary software if
free software can’t do all the jobs they want done. Thus, if we want
to increase the number of users in the long run, we should above all
  <em>
   develop more free software.
  </em>
 </p>
 <a name="index-manuals_002c-need-for-2">
 </a>
 <a name="index-call-to-action_002c-write-free-documentation">
 </a>
 <p>
  The most direct way to do this is by writing needed
free software or manuals yourself. But if you do
distribution rather than writing, the best way you can help is by
  <a name="index-call-to-action_002c-raise-funds-1">
  </a>
  raising funds for others to write them.
 </p>
 <a name="The-Term-_0060_0060Selling-Software_0027_0027-Can-Be-Confusing-Too">
 </a>
 <h3 class="subheading">
  The Term “Selling Software” Can Be Confusing Too
 </h3>
 <p>
  Strictly speaking, “selling” means trading goods for
money. Selling a copy of a free program is legitimate, and we
encourage it.
 </p>
 <p>
  However, when people think of “selling software,”
they usually imagine doing it the way most companies do it: making the
software proprietary rather than free.
 </p>
 <p>
  So unless you’re going to draw distinctions carefully, the way this
article does, we suggest it is better to avoid using the term
“selling software” and choose some other wording instead.
For example, you could say “distributing free software for a
fee”—that is unambiguous.
 </p>
 <a name="High-or-Low-Fees_002c-and-the-GNU-GPL">
 </a>
 <h3 class="subheading">
  High or Low Fees, and the GNU GPL
 </h3>
 <a name="index-GPL_002c-high-or-low-fees-and">
 </a>
 <p>
  Except for one special situation, the GNU General Public License (GNU GPL)
has no requirements about how much you can charge for distributing a
copy of free software. You can charge nothing, a penny, a dollar, or
a billion dollars. It’s up to you, and the marketplace, so don’t
complain to us if nobody wants to pay a billion dollars for a
copy.
 </p>
 <p>
  The one exception is in the case where binaries are distributed
without the corresponding complete source code. Those who do this are
required by the GNU GPL to provide source code on subsequent request.
Without a limit on the fee for the source code, they would be able set
a fee too large for anyone to pay—such as a billion
dollars—and thus pretend to release source code while in truth
concealing it. So in this case we have to limit the fee for source in order
to ensure the user’s freedom. In ordinary situations, however, there
is no such justification for limiting distribution fees, so we do not
limit them.
 </p>
 <p>
  Sometimes companies whose activities cross the line stated in the GNU
GPL plead for permission, saying that they “won’t charge
money for the GNU software” or such like. That won’t get them anywhere
with us. Free software is about freedom, and enforcing the GPL is
defending freedom. When we defend users’ freedom, we are not
distracted by side issues such as how much of a distribution fee is
charged. Freedom is the issue, the whole issue, and the only issue.
  <a name="index-selling_002c-free-software-3">
  </a>
  <a name="index-selling_002c-and-distribution-fees-1">
  </a>
 </p>
 <hr size="2"/>

