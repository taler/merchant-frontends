#  This file is part of TALER
#  (C) 2016 INRIA
#
#  TALER is free software; you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free Software
#  Foundation; either version 3, or (at your option) any later version.
#
#  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
#
#  @author Florian Dold
#  @author Marcello Stanisci

from flask import request, jsonify, make_response, current_app, render_template
import flask
from urllib.parse import urljoin, urlencode
import logging
import requests
import re
import datetime
import json
from .talerconfig import TalerConfig

logger = logging.getLogger(__name__)

tc = TalerConfig.from_env()
BACKEND_URL = tc["frontends"]["backend"].value_string(required=True)
NDIGITS = tc["frontends"]["NDIGITS"].value_int()
CURRENCY = tc["taler"]["CURRENCY"].value_string()

FRACTION_BASE = 1e8

if not NDIGITS:
    NDIGITS = 2

class MissingParameterException(Exception):
    def __init__(self, param):
        self.param = param

def amount_to_float(amount):
    return amount['value'] + (float(amount['fraction']) / float(FRACTION_BASE))


def amount_from_float(x):
    value = int(x)
    fraction = int((x - value) * FRACTION_BASE)
    return dict(currency=CURRENCY, value=value, fraction=fraction)


def join_urlparts(*parts):
    s = ""
    i = 0
    while i < len(parts):
        n = parts[i]
        i += 1
        if s.endswith("/"):
            n = n.lstrip("/")
        elif s and  not n.startswith("/"):
            n = "/" + n
        s += n
    return s


def make_url(page, *query_params):
    """
    Return a URL to a page in the current Flask application with the given
    query parameters (sequence of key/value pairs).
    """
    query = urlencode(query_params)
    if page.startswith("/"):
        root = request.url_root
        page = page.lstrip("/")
    else:
        root = request.base_url
    url = urljoin(root, "%s?%s" % (page, query))
    # urlencode is overly eager with quoting, the wallet right now
    # needs some characters unquoted.
    return url.replace("%24", "$").replace("%7B", "{").replace("%7D", "}")


def expect_parameter(name, alt=None):
    value = request.args.get(name, None)
    if value is None and alt is None:
        logger.error("Missing parameter '%s' from '%s'." % (name, request.args))
        raise MissingParameterException(name)
    return value if value else alt


def get_query_string():
    return request.query_string

def backend_error(requests_response):
    logger.error("Backend error: status code: "
                 + str(requests_response.status_code))
    try:
        return flask.jsonify(requests_response.json()), requests_response.status_code
    except json.decoder.JSONDecodeError:
        logger.error("Backend error (NO JSON returned): status code: "
                     + str(requests_response.status_code))
        return flask.jsonify(dict(error="Backend died, no JSON got from it")), 502
