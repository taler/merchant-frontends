/*
  @licstart  The following is the entire license notice for the
  JavaScript code in this page.

  Copyright (C) 2015, 2016 INRIA

  The JavaScript code in this page is free software: you can
  redistribute it and/or modify it under the terms of the GNU
  Lesser General Public License (GNU LGPL) as published by the Free Software
  Foundation, either version 2.1 of the License, or (at your option)
  any later version.  The code is distributed WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU LGPL for more details.

  As additional permission under GNU LGPL version 2.1 section 7, you
  may distribute non-source (e.g., minimized or compacted) forms of
  that code without the copy of the GNU LGPL normally required by
  section 4, provided you include this license notice and a URL
  through which recipients can access the Corresponding Source.

  @licend  The above is the entire license notice
  for the JavaScript code in this page.
 
  @brief Contains functions that regulate the focus to input fields
         and others that request /history to the frontend
  @author Marcello Stanisci
*/
"use strict";

var FRACTION = 100000000;
var TIMESTAMP = 0;
var START = 0;
var INSTANCES = ["GNUnet", "Tor", "Taler"];

function amount_to_string(amount){
  var number = Number(amount.value) + (Number(amount.fraction)/FRACTION);
  return `${number} ${amount.currency}`;
}

function parse_date(date){

  var split = date.match(/Date\((.*)\)/);
  var seconds;
  if(isNaN(seconds = Number(split[1]))){
    console.error("Malformed date gotten from backend");
    return;
  }
  var d = new Date(seconds * 1000);
  var months = ["Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"];
  return `${d.getUTCDate()} ${months[d.getUTCMonth()]} ${d.getUTCFullYear()}, ${d.getUTCHours()}:${d.getUTCMinutes()}`;
}

function toggle_visible(overlay){
  overlay.style.visibility = "visible";
  overlay.style.opacity = 1;
}

function track_transfer(wtid){
  // Extract Exchange and WTID from 'wtid'
  console.log("Tracking", wtid);
}

function xpath_get(xpath, ctx){
  var ret = document.evaluate(xpath,
                              ctx,
                              null,
                              XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
                              null);
  return ret.snapshotItem(0);
}

function track_order(order_id, instance){
  var req = new XMLHttpRequest();
  req.open("GET", `/track/order?order_id=${order_id}&instance=${instance}`, true);
  req.onload = function(){
    if(req.readyState == 4 && req.status == 200){
      var tracks = JSON.parse(req.responseText); 
      if(!tracks)
        console.log("Got invalid JSON");
      if(0 == tracks.length){
        console.log("Got no tracks AND status == 200.  Should not be here.");
        return;
      }

      for(var i=0; i<tracks.length; i++){
        var entry = tracks[i];
        var overlay = document.getElementsByClassName("overlay")[0];
        var track_content = document.getElementsByClassName("track-content")[0];
        var table = xpath_get("table/tbody", track_content);
        toggle_visible(overlay);
        for(var j=0; j<entry.coins.length; j++){
          var coin = entry.coins[j];
          var row = document.createElement("tr");
          row.innerHTML = `<td>
                             <a href="#"
                                onclick='track_transfer("${entry.wtid}")'>${entry.wtid.substring(0, 8)}...</a>
                           </td>
            <td>${amount_to_string(coin.amount_with_fee)}</td>
            <td>${coin.coin_pub.substring(0, 8)}...</td>
            <td>${parse_date(entry.execution_time)}</td>`;
          table.appendChild(row);
        }
      }
    }
    if (req.readyState == 4 && req.status == 202){
      console.log("Pending order.");
      var overlay = document.getElementsByClassName("overlay")[0];
      var track_content = document.getElementsByClassName("track-content")[0];
      track_content.innerHTML = "This order is still waiting to be paid back.";
      toggle_visible(overlay);
    }
    // Manage here 202 case (tracks not ready).
  }
  req.send();
}

/**
 * Append results to the table showing results.
 */
function fill_table(history){
  var table = document.getElementById("history");
  var tbody = xpath_get("tbody", table);
  for (var i=0; i<history.length; i++){
    var entry = history[i];
    var row = document.createElement("tr");
    var td_order_id = document.createElement("td");
    var td_summary = document.createElement("td");
    var td_amount = document.createElement("td");
    var td_timestamp = document.createElement("td");
    td_order_id.innerHTML = `<a href="#" onclick='track_order("${entry.order_id}", "${entry.instance}");'>${entry.order_id}</a>`;
    td_summary.innerHTML = "TBD";
    td_amount.innerHTML = amount_to_string(entry.amount);
    td_timestamp.innerHTML = parse_date(entry.timestamp);
    row.appendChild(td_order_id);
    row.appendChild(td_summary);
    row.appendChild(td_amount);
    row.appendChild(td_timestamp);
    tbody.appendChild(row);
    }

  function remove_loader(){
    var loader = document.getElementsByClassName("loader")[0]; 
    loader.style.visibility = "hidden";
    table.style.visibility = "";
  }
  window.setTimeout(remove_loader, 900);
}

function get_history(scroll){
  var loader = document.getElementsByClassName("loader")[0]; 
  loader.style.visibility = "visible";
  if(scroll)
    START +=20;
  var req = new Array(3);
  for(var i=0; i<INSTANCES.length; i++){
    req[i] = new XMLHttpRequest();
    req[i].open("GET", `/history?timestamp=${TIMESTAMP}&instance=${INSTANCES[i]}&start=${START}`, true);
    req[i].onload = function(){
      if(this.readyState == 4 && this.status == 200){
        var history = JSON.parse(this.responseText); 
        if(!history){
          console.log("Got invalid JSON");
          return;
        }
        fill_table(history);
      }
      else{
        console.log("error: status != 200");
      }
    }
    req[i].send();
  }
}

document.addEventListener("DOMContentLoaded", () => get_history(false));
document.addEventListener("scroll", () => get_history(true));
